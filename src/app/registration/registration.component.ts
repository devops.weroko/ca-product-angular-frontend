import { Component, OnInit } from '@angular/core';
import { CaproductService } from '../services/caproduct.service';
import { Router } from '@angular/router';
import { delay } from 'rxjs/operators';
import { ResponseMsg } from '../models/ResponseMsg';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  constructor(private caproductService: CaproductService, private router: Router) { }

    name : string;
    contact_no : string;
    username : string;
    firm_name : string;
    company_type : string;
    user_type : string;
    register_for : string;
    password : string;
    responseMsg : ResponseMsg;


  ngOnInit(): void {
  }

  onSubmit(){

    const registerInfo = {
      name : this.name,
      contact_no : this.contact_no,
      username : this.username,
      firm_name : this.firm_name,
      company_type : this.company_type,
      user_type : this.user_type,
      register_for : this.register_for,
      password : this.password
    }
   
        
    console.log(registerInfo)
        

    this.caproductService.registerUser(registerInfo).subscribe(
      (reponseMsg : ResponseMsg ) => {

        this.responseMsg = reponseMsg;
        console.log(this.responseMsg);
        
        if(this.responseMsg.genId == 1){
        
        setTimeout(()=>{   
          this.router.navigate(['login']);
         }, 3000);
        }

        else  {
          console.log("Something went wrong")
          }
      }
    );

  }



}
