import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorViewCompanySequrityComponent } from './editor-view-company-sequrity.component';

describe('EditorViewCompanySequrityComponent', () => {
  let component: EditorViewCompanySequrityComponent;
  let fixture: ComponentFixture<EditorViewCompanySequrityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorViewCompanySequrityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorViewCompanySequrityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
