import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorViewCompanyInfoComponent } from './editor-view-company-info.component';

describe('EditorViewCompanyInfoComponent', () => {
  let component: EditorViewCompanyInfoComponent;
  let fixture: ComponentFixture<EditorViewCompanyInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorViewCompanyInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorViewCompanyInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
