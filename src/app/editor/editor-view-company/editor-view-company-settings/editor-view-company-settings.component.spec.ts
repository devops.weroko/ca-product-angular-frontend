import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorViewCompanySettingsComponent } from './editor-view-company-settings.component';

describe('EditorViewCompanySettingsComponent', () => {
  let component: EditorViewCompanySettingsComponent;
  let fixture: ComponentFixture<EditorViewCompanySettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorViewCompanySettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorViewCompanySettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
