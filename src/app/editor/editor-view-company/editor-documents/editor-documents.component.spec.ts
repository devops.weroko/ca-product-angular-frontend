import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorDocumentsComponent } from './editor-documents.component';

describe('EditorDocumentsComponent', () => {
  let component: EditorDocumentsComponent;
  let fixture: ComponentFixture<EditorDocumentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorDocumentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorDocumentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
