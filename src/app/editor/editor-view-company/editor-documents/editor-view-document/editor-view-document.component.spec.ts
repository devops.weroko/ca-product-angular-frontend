import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorViewDocumentComponent } from './editor-view-document.component';

describe('EditorViewDocumentComponent', () => {
  let component: EditorViewDocumentComponent;
  let fixture: ComponentFixture<EditorViewDocumentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorViewDocumentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorViewDocumentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
