import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorViewCompanyHoldingComponent } from './editor-view-company-holding.component';

describe('EditorViewCompanyHoldingComponent', () => {
  let component: EditorViewCompanyHoldingComponent;
  let fixture: ComponentFixture<EditorViewCompanyHoldingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorViewCompanyHoldingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorViewCompanyHoldingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
