import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorViewCompanyAuditorsComponent } from './editor-view-company-auditors.component';

describe('EditorViewCompanyAuditorsComponent', () => {
  let component: EditorViewCompanyAuditorsComponent;
  let fixture: ComponentFixture<EditorViewCompanyAuditorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorViewCompanyAuditorsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorViewCompanyAuditorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
