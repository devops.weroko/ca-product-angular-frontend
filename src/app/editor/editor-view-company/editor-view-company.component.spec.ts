import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorViewCompanyComponent } from './editor-view-company.component';

describe('EditorViewCompanyComponent', () => {
  let component: EditorViewCompanyComponent;
  let fixture: ComponentFixture<EditorViewCompanyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorViewCompanyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorViewCompanyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
