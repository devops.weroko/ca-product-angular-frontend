import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorViewWirehousesComponent } from './editor-view-wirehouses.component';

describe('EditorViewWirehousesComponent', () => {
  let component: EditorViewWirehousesComponent;
  let fixture: ComponentFixture<EditorViewWirehousesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorViewWirehousesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorViewWirehousesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
