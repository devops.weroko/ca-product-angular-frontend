import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorViewCompanyAddressComponent } from './editor-view-company-address.component';

describe('EditorViewCompanyAddressComponent', () => {
  let component: EditorViewCompanyAddressComponent;
  let fixture: ComponentFixture<EditorViewCompanyAddressComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorViewCompanyAddressComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorViewCompanyAddressComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
