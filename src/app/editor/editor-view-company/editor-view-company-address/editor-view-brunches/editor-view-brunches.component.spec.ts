import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorViewBrunchesComponent } from './editor-view-brunches.component';

describe('EditorViewBrunchesComponent', () => {
  let component: EditorViewBrunchesComponent;
  let fixture: ComponentFixture<EditorViewBrunchesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorViewBrunchesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorViewBrunchesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
