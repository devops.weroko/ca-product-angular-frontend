import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorViewCompanySignetoriesComponent } from './editor-view-company-signetories.component';

describe('EditorViewCompanySignetoriesComponent', () => {
  let component: EditorViewCompanySignetoriesComponent;
  let fixture: ComponentFixture<EditorViewCompanySignetoriesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorViewCompanySignetoriesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorViewCompanySignetoriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
