import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorFillingComponent } from './editor-filling.component';

describe('EditorFillingComponent', () => {
  let component: EditorFillingComponent;
  let fixture: ComponentFixture<EditorFillingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorFillingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorFillingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
