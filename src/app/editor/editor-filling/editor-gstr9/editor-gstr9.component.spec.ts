import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorGstr9Component } from './editor-gstr9.component';

describe('EditorGstr9Component', () => {
  let component: EditorGstr9Component;
  let fixture: ComponentFixture<EditorGstr9Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorGstr9Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorGstr9Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
