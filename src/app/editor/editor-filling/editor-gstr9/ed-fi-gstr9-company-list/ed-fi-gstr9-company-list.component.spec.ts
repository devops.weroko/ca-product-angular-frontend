import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EdFiGstr9CompanyListComponent } from './ed-fi-gstr9-company-list.component';

describe('EdFiGstr9CompanyListComponent', () => {
  let component: EdFiGstr9CompanyListComponent;
  let fixture: ComponentFixture<EdFiGstr9CompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EdFiGstr9CompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EdFiGstr9CompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
