import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorGspr1Component } from './editor-gspr1.component';

describe('EditorGspr1Component', () => {
  let component: EditorGspr1Component;
  let fixture: ComponentFixture<EditorGspr1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorGspr1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorGspr1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
