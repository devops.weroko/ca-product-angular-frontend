import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EdFiGspr1CompanyListComponent } from './ed-fi-gspr1-company-list.component';

describe('EdFiGspr1CompanyListComponent', () => {
  let component: EdFiGspr1CompanyListComponent;
  let fixture: ComponentFixture<EdFiGspr1CompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EdFiGspr1CompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EdFiGspr1CompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
