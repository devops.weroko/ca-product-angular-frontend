import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EdFiNiReCompanyListComponent } from './ed-fi-ni-re-company-list.component';

describe('EdFiNiReCompanyListComponent', () => {
  let component: EdFiNiReCompanyListComponent;
  let fixture: ComponentFixture<EdFiNiReCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EdFiNiReCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EdFiNiReCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
