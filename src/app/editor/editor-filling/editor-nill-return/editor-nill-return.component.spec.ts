import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorNillReturnComponent } from './editor-nill-return.component';

describe('EditorNillReturnComponent', () => {
  let component: EditorNillReturnComponent;
  let fixture: ComponentFixture<EditorNillReturnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorNillReturnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorNillReturnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
