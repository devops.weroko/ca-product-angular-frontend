import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EdFiGspr3bCompanyListComponent } from './ed-fi-gspr3b-company-list.component';

describe('EdFiGspr3bCompanyListComponent', () => {
  let component: EdFiGspr3bCompanyListComponent;
  let fixture: ComponentFixture<EdFiGspr3bCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EdFiGspr3bCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EdFiGspr3bCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
