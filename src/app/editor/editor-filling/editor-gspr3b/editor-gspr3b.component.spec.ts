import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorGspr3bComponent } from './editor-gspr3b.component';

describe('EditorGspr3bComponent', () => {
  let component: EditorGspr3bComponent;
  let fixture: ComponentFixture<EditorGspr3bComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorGspr3bComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorGspr3bComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
