import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EdLedSupCompanyListComponent } from './ed-led-sup-company-list.component';

describe('EdLedSupCompanyListComponent', () => {
  let component: EdLedSupCompanyListComponent;
  let fixture: ComponentFixture<EdLedSupCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EdLedSupCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EdLedSupCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
