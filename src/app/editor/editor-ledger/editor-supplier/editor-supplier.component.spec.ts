import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorSupplierComponent } from './editor-supplier.component';

describe('EditorSupplierComponent', () => {
  let component: EditorSupplierComponent;
  let fixture: ComponentFixture<EditorSupplierComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorSupplierComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorSupplierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
