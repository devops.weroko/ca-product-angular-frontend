import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorExpenditureComponent } from './editor-expenditure.component';

describe('EditorExpenditureComponent', () => {
  let component: EditorExpenditureComponent;
  let fixture: ComponentFixture<EditorExpenditureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorExpenditureComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorExpenditureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
