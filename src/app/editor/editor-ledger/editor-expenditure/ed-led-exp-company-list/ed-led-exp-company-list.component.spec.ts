import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EdLedExpCompanyListComponent } from './ed-led-exp-company-list.component';

describe('EdLedExpCompanyListComponent', () => {
  let component: EdLedExpCompanyListComponent;
  let fixture: ComponentFixture<EdLedExpCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EdLedExpCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EdLedExpCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
