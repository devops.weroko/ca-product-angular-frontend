import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EdLedInvCompanyListComponent } from './ed-led-inv-company-list.component';

describe('EdLedInvCompanyListComponent', () => {
  let component: EdLedInvCompanyListComponent;
  let fixture: ComponentFixture<EdLedInvCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EdLedInvCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EdLedInvCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
