import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorInvestmentComponent } from './editor-investment.component';

describe('EditorInvestmentComponent', () => {
  let component: EditorInvestmentComponent;
  let fixture: ComponentFixture<EditorInvestmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorInvestmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorInvestmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
