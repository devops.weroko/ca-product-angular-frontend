import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorFixtAssetsComponent } from './editor-fixt-assets.component';

describe('EditorFixtAssetsComponent', () => {
  let component: EditorFixtAssetsComponent;
  let fixture: ComponentFixture<EditorFixtAssetsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorFixtAssetsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorFixtAssetsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
