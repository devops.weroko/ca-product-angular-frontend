import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EdLedFixtAsCompanyListComponent } from './ed-led-fixt-as-company-list.component';

describe('EdLedFixtAsCompanyListComponent', () => {
  let component: EdLedFixtAsCompanyListComponent;
  let fixture: ComponentFixture<EdLedFixtAsCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EdLedFixtAsCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EdLedFixtAsCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
