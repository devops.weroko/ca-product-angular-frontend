import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EdLedMuLedCompanyListComponent } from './ed-led-mu-led-company-list.component';

describe('EdLedMuLedCompanyListComponent', () => {
  let component: EdLedMuLedCompanyListComponent;
  let fixture: ComponentFixture<EdLedMuLedCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EdLedMuLedCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EdLedMuLedCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
