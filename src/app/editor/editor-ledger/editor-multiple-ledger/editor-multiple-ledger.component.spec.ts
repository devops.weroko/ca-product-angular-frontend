import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorMultipleLedgerComponent } from './editor-multiple-ledger.component';

describe('EditorMultipleLedgerComponent', () => {
  let component: EditorMultipleLedgerComponent;
  let fixture: ComponentFixture<EditorMultipleLedgerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorMultipleLedgerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorMultipleLedgerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
