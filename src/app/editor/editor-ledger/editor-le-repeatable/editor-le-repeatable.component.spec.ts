import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorLeRepeatableComponent } from './editor-le-repeatable.component';

describe('EditorLeRepeatableComponent', () => {
  let component: EditorLeRepeatableComponent;
  let fixture: ComponentFixture<EditorLeRepeatableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorLeRepeatableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorLeRepeatableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
