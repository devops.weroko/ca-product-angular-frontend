import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorAditionalInfoComponentComponent } from './editor-aditional-info-component.component';

describe('EditorAditionalInfoComponentComponent', () => {
  let component: EditorAditionalInfoComponentComponent;
  let fixture: ComponentFixture<EditorAditionalInfoComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorAditionalInfoComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorAditionalInfoComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
