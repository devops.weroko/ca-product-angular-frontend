import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorBankInfoComponentComponent } from './editor-bank-info-component.component';

describe('EditorBankInfoComponentComponent', () => {
  let component: EditorBankInfoComponentComponent;
  let fixture: ComponentFixture<EditorBankInfoComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorBankInfoComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorBankInfoComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
