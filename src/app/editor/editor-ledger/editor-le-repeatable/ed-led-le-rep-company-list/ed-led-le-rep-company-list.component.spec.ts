import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EdLedLeRepCompanyListComponent } from './ed-led-le-rep-company-list.component';

describe('EdLedLeRepCompanyListComponent', () => {
  let component: EdLedLeRepCompanyListComponent;
  let fixture: ComponentFixture<EdLedLeRepCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EdLedLeRepCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EdLedLeRepCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
