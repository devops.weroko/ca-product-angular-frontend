import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorImpNoteComponentComponent } from './editor-imp-note-component.component';

describe('EditorImpNoteComponentComponent', () => {
  let component: EditorImpNoteComponentComponent;
  let fixture: ComponentFixture<EditorImpNoteComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorImpNoteComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorImpNoteComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
