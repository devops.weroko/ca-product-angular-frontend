import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorContactPersonComponentComponent } from './editor-contact-person-component.component';

describe('EditorContactPersonComponentComponent', () => {
  let component: EditorContactPersonComponentComponent;
  let fixture: ComponentFixture<EditorContactPersonComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorContactPersonComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorContactPersonComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
