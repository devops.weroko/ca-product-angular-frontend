import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorAttachmentComponentComponent } from './editor-attachment-component.component';

describe('EditorAttachmentComponentComponent', () => {
  let component: EditorAttachmentComponentComponent;
  let fixture: ComponentFixture<EditorAttachmentComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorAttachmentComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorAttachmentComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
