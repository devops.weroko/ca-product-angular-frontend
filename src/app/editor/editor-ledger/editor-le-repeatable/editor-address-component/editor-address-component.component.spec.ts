import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorAddressComponentComponent } from './editor-address-component.component';

describe('EditorAddressComponentComponent', () => {
  let component: EditorAddressComponentComponent;
  let fixture: ComponentFixture<EditorAddressComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorAddressComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorAddressComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
