import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EdLedIncCompanyListComponent } from './ed-led-inc-company-list.component';

describe('EdLedIncCompanyListComponent', () => {
  let component: EdLedIncCompanyListComponent;
  let fixture: ComponentFixture<EdLedIncCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EdLedIncCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EdLedIncCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
