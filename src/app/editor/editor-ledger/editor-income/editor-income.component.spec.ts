import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorIncomeComponent } from './editor-income.component';

describe('EditorIncomeComponent', () => {
  let component: EditorIncomeComponent;
  let fixture: ComponentFixture<EditorIncomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorIncomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorIncomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
