import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorLedgerAddressComponent } from './editor-ledger-address.component';

describe('EditorLedgerAddressComponent', () => {
  let component: EditorLedgerAddressComponent;
  let fixture: ComponentFixture<EditorLedgerAddressComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorLedgerAddressComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorLedgerAddressComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
