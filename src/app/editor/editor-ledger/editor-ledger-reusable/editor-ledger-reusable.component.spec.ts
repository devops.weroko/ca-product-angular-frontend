import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorLedgerReusableComponent } from './editor-ledger-reusable.component';

describe('EditorLedgerReusableComponent', () => {
  let component: EditorLedgerReusableComponent;
  let fixture: ComponentFixture<EditorLedgerReusableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorLedgerReusableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorLedgerReusableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
