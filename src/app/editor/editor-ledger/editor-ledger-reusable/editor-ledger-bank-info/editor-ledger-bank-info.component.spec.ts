import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorLedgerBankInfoComponent } from './editor-ledger-bank-info.component';

describe('EditorLedgerBankInfoComponent', () => {
  let component: EditorLedgerBankInfoComponent;
  let fixture: ComponentFixture<EditorLedgerBankInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorLedgerBankInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorLedgerBankInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
