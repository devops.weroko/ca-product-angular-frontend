import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorLedgerAdditionalNoteComponent } from './editor-ledger-additional-note.component';

describe('EditorLedgerAdditionalNoteComponent', () => {
  let component: EditorLedgerAdditionalNoteComponent;
  let fixture: ComponentFixture<EditorLedgerAdditionalNoteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorLedgerAdditionalNoteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorLedgerAdditionalNoteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
