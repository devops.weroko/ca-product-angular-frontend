import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorLedgerAttachmentComponent } from './editor-ledger-attachment.component';

describe('EditorLedgerAttachmentComponent', () => {
  let component: EditorLedgerAttachmentComponent;
  let fixture: ComponentFixture<EditorLedgerAttachmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorLedgerAttachmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorLedgerAttachmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
