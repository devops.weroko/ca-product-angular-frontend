import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EdLedLedReuCompanyListComponent } from './ed-led-led-reu-company-list.component';

describe('EdLedLedReuCompanyListComponent', () => {
  let component: EdLedLedReuCompanyListComponent;
  let fixture: ComponentFixture<EdLedLedReuCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EdLedLedReuCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EdLedLedReuCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
