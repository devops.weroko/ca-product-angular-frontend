import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorEmployeeComponent } from './editor-employee.component';

describe('EditorEmployeeComponent', () => {
  let component: EditorEmployeeComponent;
  let fixture: ComponentFixture<EditorEmployeeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorEmployeeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorEmployeeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
