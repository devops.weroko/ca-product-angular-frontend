import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EdLedEmpCompanyListComponent } from './ed-led-emp-company-list.component';

describe('EdLedEmpCompanyListComponent', () => {
  let component: EdLedEmpCompanyListComponent;
  let fixture: ComponentFixture<EdLedEmpCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EdLedEmpCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EdLedEmpCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
