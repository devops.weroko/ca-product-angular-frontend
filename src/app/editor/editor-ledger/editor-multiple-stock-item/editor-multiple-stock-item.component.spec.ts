import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorMultipleStockItemComponent } from './editor-multiple-stock-item.component';

describe('EditorMultipleStockItemComponent', () => {
  let component: EditorMultipleStockItemComponent;
  let fixture: ComponentFixture<EditorMultipleStockItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorMultipleStockItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorMultipleStockItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
