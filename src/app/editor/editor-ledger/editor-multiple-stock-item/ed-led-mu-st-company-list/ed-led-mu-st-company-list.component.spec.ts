import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EdLedMuStCompanyListComponent } from './ed-led-mu-st-company-list.component';

describe('EdLedMuStCompanyListComponent', () => {
  let component: EdLedMuStCompanyListComponent;
  let fixture: ComponentFixture<EdLedMuStCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EdLedMuStCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EdLedMuStCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
