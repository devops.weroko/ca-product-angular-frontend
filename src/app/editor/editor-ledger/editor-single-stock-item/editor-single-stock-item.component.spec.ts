import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorSingleStockItemComponent } from './editor-single-stock-item.component';

describe('EditorSingleStockItemComponent', () => {
  let component: EditorSingleStockItemComponent;
  let fixture: ComponentFixture<EditorSingleStockItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorSingleStockItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorSingleStockItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
