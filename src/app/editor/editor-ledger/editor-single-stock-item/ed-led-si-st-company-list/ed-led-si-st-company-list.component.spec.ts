import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EdLedSiStCompanyListComponent } from './ed-led-si-st-company-list.component';

describe('EdLedSiStCompanyListComponent', () => {
  let component: EdLedSiStCompanyListComponent;
  let fixture: ComponentFixture<EdLedSiStCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EdLedSiStCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EdLedSiStCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
