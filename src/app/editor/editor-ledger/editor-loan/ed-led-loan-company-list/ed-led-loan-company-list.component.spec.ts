import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EdLedLoanCompanyListComponent } from './ed-led-loan-company-list.component';

describe('EdLedLoanCompanyListComponent', () => {
  let component: EdLedLoanCompanyListComponent;
  let fixture: ComponentFixture<EdLedLoanCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EdLedLoanCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EdLedLoanCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
