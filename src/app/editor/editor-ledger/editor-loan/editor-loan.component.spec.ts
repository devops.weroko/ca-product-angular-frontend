import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorLoanComponent } from './editor-loan.component';

describe('EditorLoanComponent', () => {
  let component: EditorLoanComponent;
  let fixture: ComponentFixture<EditorLoanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorLoanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorLoanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
