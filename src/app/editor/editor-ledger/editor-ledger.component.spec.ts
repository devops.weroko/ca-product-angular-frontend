import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorLedgerComponent } from './editor-ledger.component';

describe('EditorLedgerComponent', () => {
  let component: EditorLedgerComponent;
  let fixture: ComponentFixture<EditorLedgerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorLedgerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorLedgerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
