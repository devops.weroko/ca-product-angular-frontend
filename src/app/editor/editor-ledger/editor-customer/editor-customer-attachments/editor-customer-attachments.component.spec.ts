import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorCustomerAttachmentsComponent } from './editor-customer-attachments.component';

describe('EditorCustomerAttachmentsComponent', () => {
  let component: EditorCustomerAttachmentsComponent;
  let fixture: ComponentFixture<EditorCustomerAttachmentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorCustomerAttachmentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorCustomerAttachmentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
