import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorCustomerNotesComponent } from './editor-customer-notes.component';

describe('EditorCustomerNotesComponent', () => {
  let component: EditorCustomerNotesComponent;
  let fixture: ComponentFixture<EditorCustomerNotesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorCustomerNotesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorCustomerNotesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
