import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorCustomerContactPersonComponent } from './editor-customer-contact-person.component';

describe('EditorCustomerContactPersonComponent', () => {
  let component: EditorCustomerContactPersonComponent;
  let fixture: ComponentFixture<EditorCustomerContactPersonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorCustomerContactPersonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorCustomerContactPersonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
