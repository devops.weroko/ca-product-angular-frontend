import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorCustomerTaxRegComponent } from './editor-customer-tax-reg.component';

describe('EditorCustomerTaxRegComponent', () => {
  let component: EditorCustomerTaxRegComponent;
  let fixture: ComponentFixture<EditorCustomerTaxRegComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorCustomerTaxRegComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorCustomerTaxRegComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
