import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorCustomerBankComponent } from './editor-customer-bank.component';

describe('EditorCustomerBankComponent', () => {
  let component: EditorCustomerBankComponent;
  let fixture: ComponentFixture<EditorCustomerBankComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorCustomerBankComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorCustomerBankComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
