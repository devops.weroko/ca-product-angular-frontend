import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorCustomerAddressComponent } from './editor-customer-address.component';

describe('EditorCustomerAddressComponent', () => {
  let component: EditorCustomerAddressComponent;
  let fixture: ComponentFixture<EditorCustomerAddressComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorCustomerAddressComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorCustomerAddressComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
