import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EdLedCustomerCompanyListComponent } from './ed-led-customer-company-list.component';

describe('EdLedCustomerCompanyListComponent', () => {
  let component: EdLedCustomerCompanyListComponent;
  let fixture: ComponentFixture<EdLedCustomerCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EdLedCustomerCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EdLedCustomerCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
