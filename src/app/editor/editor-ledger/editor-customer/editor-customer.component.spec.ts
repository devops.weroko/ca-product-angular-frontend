import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorCustomerComponent } from './editor-customer.component';

describe('EditorCustomerComponent', () => {
  let component: EditorCustomerComponent;
  let fixture: ComponentFixture<EditorCustomerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorCustomerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorCustomerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
