import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorCustomerInfoComponent } from './editor-customer-info.component';

describe('EditorCustomerInfoComponent', () => {
  let component: EditorCustomerInfoComponent;
  let fixture: ComponentFixture<EditorCustomerInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorCustomerInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorCustomerInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
