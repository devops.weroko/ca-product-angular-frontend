import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorCustomerAdditionalInfoComponent } from './editor-customer-additional-info.component';

describe('EditorCustomerAdditionalInfoComponent', () => {
  let component: EditorCustomerAdditionalInfoComponent;
  let fixture: ComponentFixture<EditorCustomerAdditionalInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorCustomerAdditionalInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorCustomerAdditionalInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
