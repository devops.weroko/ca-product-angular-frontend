import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorCustomLedgerComponent } from './editor-custom-ledger.component';

describe('EditorCustomLedgerComponent', () => {
  let component: EditorCustomLedgerComponent;
  let fixture: ComponentFixture<EditorCustomLedgerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorCustomLedgerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorCustomLedgerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
