import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EdLedCustomLedCompanyListComponent } from './ed-led-custom-led-company-list.component';

describe('EdLedCustomLedCompanyListComponent', () => {
  let component: EdLedCustomLedCompanyListComponent;
  let fixture: ComponentFixture<EdLedCustomLedCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EdLedCustomLedCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EdLedCustomLedCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
