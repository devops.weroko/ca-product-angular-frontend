import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorCashComponent } from './editor-cash.component';

describe('EditorCashComponent', () => {
  let component: EditorCashComponent;
  let fixture: ComponentFixture<EditorCashComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorCashComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorCashComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
