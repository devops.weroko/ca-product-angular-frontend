import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EdLedCashCompanyListComponent } from './ed-led-cash-company-list.component';

describe('EdLedCashCompanyListComponent', () => {
  let component: EdLedCashCompanyListComponent;
  let fixture: ComponentFixture<EdLedCashCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EdLedCashCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EdLedCashCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
