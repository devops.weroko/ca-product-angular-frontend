import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorBillOfMaterialComponent } from './editor-bill-of-material.component';

describe('EditorBillOfMaterialComponent', () => {
  let component: EditorBillOfMaterialComponent;
  let fixture: ComponentFixture<EditorBillOfMaterialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorBillOfMaterialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorBillOfMaterialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
