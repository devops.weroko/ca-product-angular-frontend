import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EdLedBillCompanyListComponent } from './ed-led-bill-company-list.component';

describe('EdLedBillCompanyListComponent', () => {
  let component: EdLedBillCompanyListComponent;
  let fixture: ComponentFixture<EdLedBillCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EdLedBillCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EdLedBillCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
