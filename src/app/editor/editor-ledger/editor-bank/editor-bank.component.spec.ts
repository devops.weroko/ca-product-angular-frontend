import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorBankComponent } from './editor-bank.component';

describe('EditorBankComponent', () => {
  let component: EditorBankComponent;
  let fixture: ComponentFixture<EditorBankComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorBankComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorBankComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
