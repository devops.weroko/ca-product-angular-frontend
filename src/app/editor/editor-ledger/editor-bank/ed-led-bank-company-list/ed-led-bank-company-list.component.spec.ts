import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EdLedBankCompanyListComponent } from './ed-led-bank-company-list.component';

describe('EdLedBankCompanyListComponent', () => {
  let component: EdLedBankCompanyListComponent;
  let fixture: ComponentFixture<EdLedBankCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EdLedBankCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EdLedBankCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
