import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorProfitLossStatementsComponent } from './editor-profit-loss-statements.component';

describe('EditorProfitLossStatementsComponent', () => {
  let component: EditorProfitLossStatementsComponent;
  let fixture: ComponentFixture<EditorProfitLossStatementsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorProfitLossStatementsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorProfitLossStatementsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
