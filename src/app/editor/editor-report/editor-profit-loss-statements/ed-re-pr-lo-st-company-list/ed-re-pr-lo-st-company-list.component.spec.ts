import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EdRePrLoStCompanyListComponent } from './ed-re-pr-lo-st-company-list.component';

describe('EdRePrLoStCompanyListComponent', () => {
  let component: EdRePrLoStCompanyListComponent;
  let fixture: ComponentFixture<EdRePrLoStCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EdRePrLoStCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EdRePrLoStCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
