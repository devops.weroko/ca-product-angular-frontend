import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EdReAllCompanyListComponent } from './ed-re-all-company-list.component';

describe('EdReAllCompanyListComponent', () => {
  let component: EdReAllCompanyListComponent;
  let fixture: ComponentFixture<EdReAllCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EdReAllCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EdReAllCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
