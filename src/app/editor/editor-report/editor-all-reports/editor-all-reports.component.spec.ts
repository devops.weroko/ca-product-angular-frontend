import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorAllReportsComponent } from './editor-all-reports.component';

describe('EditorAllReportsComponent', () => {
  let component: EditorAllReportsComponent;
  let fixture: ComponentFixture<EditorAllReportsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorAllReportsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorAllReportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
