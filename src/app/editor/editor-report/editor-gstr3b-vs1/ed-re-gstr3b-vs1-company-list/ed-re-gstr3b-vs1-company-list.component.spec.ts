import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EdReGstr3bVs1CompanyListComponent } from './ed-re-gstr3b-vs1-company-list.component';

describe('EdReGstr3bVs1CompanyListComponent', () => {
  let component: EdReGstr3bVs1CompanyListComponent;
  let fixture: ComponentFixture<EdReGstr3bVs1CompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EdReGstr3bVs1CompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EdReGstr3bVs1CompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
