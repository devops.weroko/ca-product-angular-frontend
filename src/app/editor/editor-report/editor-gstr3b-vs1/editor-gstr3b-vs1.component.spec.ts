import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorGstr3bVs1Component } from './editor-gstr3b-vs1.component';

describe('EditorGstr3bVs1Component', () => {
  let component: EditorGstr3bVs1Component;
  let fixture: ComponentFixture<EditorGstr3bVs1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorGstr3bVs1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorGstr3bVs1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
