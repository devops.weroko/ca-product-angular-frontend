import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorGstr3bVs2aComponent } from './editor-gstr3b-vs2a.component';

describe('EditorGstr3bVs2aComponent', () => {
  let component: EditorGstr3bVs2aComponent;
  let fixture: ComponentFixture<EditorGstr3bVs2aComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorGstr3bVs2aComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorGstr3bVs2aComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
