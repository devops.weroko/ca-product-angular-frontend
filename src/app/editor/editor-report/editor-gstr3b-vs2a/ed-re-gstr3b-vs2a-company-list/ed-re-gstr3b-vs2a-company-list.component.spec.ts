import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EdReGstr3bVs2aCompanyListComponent } from './ed-re-gstr3b-vs2a-company-list.component';

describe('EdReGstr3bVs2aCompanyListComponent', () => {
  let component: EdReGstr3bVs2aCompanyListComponent;
  let fixture: ComponentFixture<EdReGstr3bVs2aCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EdReGstr3bVs2aCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EdReGstr3bVs2aCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
