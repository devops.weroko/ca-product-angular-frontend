import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorRatioAnalysisComponent } from './editor-ratio-analysis.component';

describe('EditorRatioAnalysisComponent', () => {
  let component: EditorRatioAnalysisComponent;
  let fixture: ComponentFixture<EditorRatioAnalysisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorRatioAnalysisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorRatioAnalysisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
