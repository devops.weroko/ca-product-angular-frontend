import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EdReRatioAnCompanyListComponent } from './ed-re-ratio-an-company-list.component';

describe('EdReRatioAnCompanyListComponent', () => {
  let component: EdReRatioAnCompanyListComponent;
  let fixture: ComponentFixture<EdReRatioAnCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EdReRatioAnCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EdReRatioAnCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
