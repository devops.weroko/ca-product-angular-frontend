import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EdReMuMoReCompanyListComponent } from './ed-re-mu-mo-re-company-list.component';

describe('EdReMuMoReCompanyListComponent', () => {
  let component: EdReMuMoReCompanyListComponent;
  let fixture: ComponentFixture<EdReMuMoReCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EdReMuMoReCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EdReMuMoReCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
