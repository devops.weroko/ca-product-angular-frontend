import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorMultiMonthReportComponent } from './editor-multi-month-report.component';

describe('EditorMultiMonthReportComponent', () => {
  let component: EditorMultiMonthReportComponent;
  let fixture: ComponentFixture<EditorMultiMonthReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorMultiMonthReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorMultiMonthReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
