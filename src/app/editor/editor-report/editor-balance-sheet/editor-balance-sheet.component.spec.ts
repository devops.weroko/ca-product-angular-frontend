import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorBalanceSheetComponent } from './editor-balance-sheet.component';

describe('EditorBalanceSheetComponent', () => {
  let component: EditorBalanceSheetComponent;
  let fixture: ComponentFixture<EditorBalanceSheetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorBalanceSheetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorBalanceSheetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
