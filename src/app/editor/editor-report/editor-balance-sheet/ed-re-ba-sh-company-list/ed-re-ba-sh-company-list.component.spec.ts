import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EdReBaShCompanyListComponent } from './ed-re-ba-sh-company-list.component';

describe('EdReBaShCompanyListComponent', () => {
  let component: EdReBaShCompanyListComponent;
  let fixture: ComponentFixture<EdReBaShCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EdReBaShCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EdReBaShCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
