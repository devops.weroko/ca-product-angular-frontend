import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorBalanceSheetChartComponent } from './editor-balance-sheet-chart.component';

describe('EditorBalanceSheetChartComponent', () => {
  let component: EditorBalanceSheetChartComponent;
  let fixture: ComponentFixture<EditorBalanceSheetChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorBalanceSheetChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorBalanceSheetChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
