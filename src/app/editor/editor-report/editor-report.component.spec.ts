import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorReportComponent } from './editor-report.component';

describe('EditorReportComponent', () => {
  let component: EditorReportComponent;
  let fixture: ComponentFixture<EditorReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
