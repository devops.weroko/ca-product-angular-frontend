import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorSupplierComplianceComponent } from './editor-supplier-compliance.component';

describe('EditorSupplierComplianceComponent', () => {
  let component: EditorSupplierComplianceComponent;
  let fixture: ComponentFixture<EditorSupplierComplianceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorSupplierComplianceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorSupplierComplianceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
