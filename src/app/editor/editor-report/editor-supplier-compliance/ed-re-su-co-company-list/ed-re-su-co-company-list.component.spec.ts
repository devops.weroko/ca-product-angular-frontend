import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EdReSuCoCompanyListComponent } from './ed-re-su-co-company-list.component';

describe('EdReSuCoCompanyListComponent', () => {
  let component: EdReSuCoCompanyListComponent;
  let fixture: ComponentFixture<EdReSuCoCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EdReSuCoCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EdReSuCoCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
