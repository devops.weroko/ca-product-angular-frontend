import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorClientLogComponent } from './editor-client-log.component';

describe('EditorClientLogComponent', () => {
  let component: EditorClientLogComponent;
  let fixture: ComponentFixture<EditorClientLogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorClientLogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorClientLogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
