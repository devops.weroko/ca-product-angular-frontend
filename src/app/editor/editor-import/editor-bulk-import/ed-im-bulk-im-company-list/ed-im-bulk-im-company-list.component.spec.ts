import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EdImBulkImCompanyListComponent } from './ed-im-bulk-im-company-list.component';

describe('EdImBulkImCompanyListComponent', () => {
  let component: EdImBulkImCompanyListComponent;
  let fixture: ComponentFixture<EdImBulkImCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EdImBulkImCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EdImBulkImCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
