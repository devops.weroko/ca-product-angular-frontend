import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorBulkImportComponent } from './editor-bulk-import.component';

describe('EditorBulkImportComponent', () => {
  let component: EditorBulkImportComponent;
  let fixture: ComponentFixture<EditorBulkImportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorBulkImportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorBulkImportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
