import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EdImTallyCoCompanyListComponent } from './ed-im-tally-co-company-list.component';

describe('EdImTallyCoCompanyListComponent', () => {
  let component: EdImTallyCoCompanyListComponent;
  let fixture: ComponentFixture<EdImTallyCoCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EdImTallyCoCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EdImTallyCoCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
