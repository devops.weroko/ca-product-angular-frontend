import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorTallyConnectorComponent } from './editor-tally-connector.component';

describe('EditorTallyConnectorComponent', () => {
  let component: EditorTallyConnectorComponent;
  let fixture: ComponentFixture<EditorTallyConnectorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorTallyConnectorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorTallyConnectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
