import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorSignatoriesDetailsComponent } from './editor-signatories-details.component';

describe('EditorSignatoriesDetailsComponent', () => {
  let component: EditorSignatoriesDetailsComponent;
  let fixture: ComponentFixture<EditorSignatoriesDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorSignatoriesDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorSignatoriesDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
