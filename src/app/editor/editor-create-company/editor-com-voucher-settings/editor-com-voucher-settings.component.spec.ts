import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorComVoucherSettingsComponent } from './editor-com-voucher-settings.component';

describe('EditorComVoucherSettingsComponent', () => {
  let component: EditorComVoucherSettingsComponent;
  let fixture: ComponentFixture<EditorComVoucherSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorComVoucherSettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorComVoucherSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
