import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorComVoucherSettingsInputComponent } from './editor-com-voucher-settings-input.component';

describe('EditorComVoucherSettingsInputComponent', () => {
  let component: EditorComVoucherSettingsInputComponent;
  let fixture: ComponentFixture<EditorComVoucherSettingsInputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorComVoucherSettingsInputComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorComVoucherSettingsInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
