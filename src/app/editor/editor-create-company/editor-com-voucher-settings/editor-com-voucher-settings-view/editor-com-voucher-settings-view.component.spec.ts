import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorComVoucherSettingsViewComponent } from './editor-com-voucher-settings-view.component';

describe('EditorComVoucherSettingsViewComponent', () => {
  let component: EditorComVoucherSettingsViewComponent;
  let fixture: ComponentFixture<EditorComVoucherSettingsViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorComVoucherSettingsViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorComVoucherSettingsViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
