import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorCompanySettingsComponent } from './editor-company-settings.component';

describe('EditorCompanySettingsComponent', () => {
  let component: EditorCompanySettingsComponent;
  let fixture: ComponentFixture<EditorCompanySettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorCompanySettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorCompanySettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
