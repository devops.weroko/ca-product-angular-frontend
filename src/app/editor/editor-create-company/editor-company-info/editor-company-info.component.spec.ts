import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorCompanyInfoComponent } from './editor-company-info.component';

describe('EditorCompanyInfoComponent', () => {
  let component: EditorCompanyInfoComponent;
  let fixture: ComponentFixture<EditorCompanyInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorCompanyInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorCompanyInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
