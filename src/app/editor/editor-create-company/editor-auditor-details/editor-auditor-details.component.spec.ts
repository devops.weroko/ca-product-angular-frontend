import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorAuditorDetailsComponent } from './editor-auditor-details.component';

describe('EditorAuditorDetailsComponent', () => {
  let component: EditorAuditorDetailsComponent;
  let fixture: ComponentFixture<EditorAuditorDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorAuditorDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorAuditorDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
