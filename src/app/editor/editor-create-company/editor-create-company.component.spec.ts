import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorCreateCompanyComponent } from './editor-create-company.component';

describe('EditorCreateCompanyComponent', () => {
  let component: EditorCreateCompanyComponent;
  let fixture: ComponentFixture<EditorCreateCompanyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorCreateCompanyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorCreateCompanyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
