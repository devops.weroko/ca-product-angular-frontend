import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorComBranchDetailsComponent } from './editor-com-branch-details.component';

describe('EditorComBranchDetailsComponent', () => {
  let component: EditorComBranchDetailsComponent;
  let fixture: ComponentFixture<EditorComBranchDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorComBranchDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorComBranchDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
