import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorComWarehouseDetailsComponent } from './editor-com-warehouse-details.component';

describe('EditorComWarehouseDetailsComponent', () => {
  let component: EditorComWarehouseDetailsComponent;
  let fixture: ComponentFixture<EditorComWarehouseDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorComWarehouseDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorComWarehouseDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
