import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorCompanyAddressComponent } from './editor-company-address.component';

describe('EditorCompanyAddressComponent', () => {
  let component: EditorCompanyAddressComponent;
  let fixture: ComponentFixture<EditorCompanyAddressComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorCompanyAddressComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorCompanyAddressComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
