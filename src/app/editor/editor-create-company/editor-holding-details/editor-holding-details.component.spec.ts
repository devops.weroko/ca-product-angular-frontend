import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorHoldingDetailsComponent } from './editor-holding-details.component';

describe('EditorHoldingDetailsComponent', () => {
  let component: EditorHoldingDetailsComponent;
  let fixture: ComponentFixture<EditorHoldingDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorHoldingDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorHoldingDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
