import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorComSecuritySettingsComponent } from './editor-com-security-settings.component';

describe('EditorComSecuritySettingsComponent', () => {
  let component: EditorComSecuritySettingsComponent;
  let fixture: ComponentFixture<EditorComSecuritySettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorComSecuritySettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorComSecuritySettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
