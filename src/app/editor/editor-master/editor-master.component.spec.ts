import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorMasterComponent } from './editor-master.component';

describe('EditorMasterComponent', () => {
  let component: EditorMasterComponent;
  let fixture: ComponentFixture<EditorMasterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorMasterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorMasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
