import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EdMasCompanyListComponent } from './ed-mas-company-list.component';

describe('EdMasCompanyListComponent', () => {
  let component: EdMasCompanyListComponent;
  let fixture: ComponentFixture<EdMasCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EdMasCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EdMasCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
