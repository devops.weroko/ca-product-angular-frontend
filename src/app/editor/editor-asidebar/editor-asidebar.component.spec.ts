import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorAsidebarComponent } from './editor-asidebar.component';

describe('EditorAsidebarComponent', () => {
  let component: EditorAsidebarComponent;
  let fixture: ComponentFixture<EditorAsidebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorAsidebarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorAsidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
