import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorCompanyShortInfoComponent } from './editor-company-short-info.component';

describe('EditorCompanyShortInfoComponent', () => {
  let component: EditorCompanyShortInfoComponent;
  let fixture: ComponentFixture<EditorCompanyShortInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorCompanyShortInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorCompanyShortInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
