import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorCompanyNameListingComponent } from './editor-company-name-listing.component';

describe('EditorCompanyNameListingComponent', () => {
  let component: EditorCompanyNameListingComponent;
  let fixture: ComponentFixture<EditorCompanyNameListingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorCompanyNameListingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorCompanyNameListingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
