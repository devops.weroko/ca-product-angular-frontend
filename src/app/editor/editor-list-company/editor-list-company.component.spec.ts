import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorListCompanyComponent } from './editor-list-company.component';

describe('EditorListCompanyComponent', () => {
  let component: EditorListCompanyComponent;
  let fixture: ComponentFixture<EditorListCompanyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorListCompanyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorListCompanyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
