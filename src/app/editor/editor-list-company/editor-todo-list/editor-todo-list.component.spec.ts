import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorTodoListComponent } from './editor-todo-list.component';

describe('EditorTodoListComponent', () => {
  let component: EditorTodoListComponent;
  let fixture: ComponentFixture<EditorTodoListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorTodoListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorTodoListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
