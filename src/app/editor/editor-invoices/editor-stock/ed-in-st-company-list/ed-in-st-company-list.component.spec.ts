import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EdInStCompanyListComponent } from './ed-in-st-company-list.component';

describe('EdInStCompanyListComponent', () => {
  let component: EdInStCompanyListComponent;
  let fixture: ComponentFixture<EdInStCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EdInStCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EdInStCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
