import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorStockComponent } from './editor-stock.component';

describe('EditorStockComponent', () => {
  let component: EditorStockComponent;
  let fixture: ComponentFixture<EditorStockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorStockComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorStockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
