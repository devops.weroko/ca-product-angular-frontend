import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorInvoicesComponent } from './editor-invoices.component';

describe('EditorInvoicesComponent', () => {
  let component: EditorInvoicesComponent;
  let fixture: ComponentFixture<EditorInvoicesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorInvoicesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorInvoicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
