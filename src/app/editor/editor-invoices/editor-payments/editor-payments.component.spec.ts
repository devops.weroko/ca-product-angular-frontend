import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorPaymentsComponent } from './editor-payments.component';

describe('EditorPaymentsComponent', () => {
  let component: EditorPaymentsComponent;
  let fixture: ComponentFixture<EditorPaymentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorPaymentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorPaymentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
