import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EdInPayCompanyListComponent } from './ed-in-pay-company-list.component';

describe('EdInPayCompanyListComponent', () => {
  let component: EdInPayCompanyListComponent;
  let fixture: ComponentFixture<EdInPayCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EdInPayCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EdInPayCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
