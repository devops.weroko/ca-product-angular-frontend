import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EdInSaCompanyListComponent } from './ed-in-sa-company-list.component';

describe('EdInSaCompanyListComponent', () => {
  let component: EdInSaCompanyListComponent;
  let fixture: ComponentFixture<EdInSaCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EdInSaCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EdInSaCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
