import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorSalesComponent } from './editor-sales.component';

describe('EditorSalesComponent', () => {
  let component: EditorSalesComponent;
  let fixture: ComponentFixture<EditorSalesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorSalesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorSalesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
