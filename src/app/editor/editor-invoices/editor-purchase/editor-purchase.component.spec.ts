import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorPurchaseComponent } from './editor-purchase.component';

describe('EditorPurchaseComponent', () => {
  let component: EditorPurchaseComponent;
  let fixture: ComponentFixture<EditorPurchaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorPurchaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorPurchaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
