import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EdInPurCompanyListComponent } from './ed-in-pur-company-list.component';

describe('EdInPurCompanyListComponent', () => {
  let component: EdInPurCompanyListComponent;
  let fixture: ComponentFixture<EdInPurCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EdInPurCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EdInPurCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
