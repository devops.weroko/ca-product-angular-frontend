import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminInvestmentComponent } from './admin-investment.component';

describe('AdminInvestmentComponent', () => {
  let component: AdminInvestmentComponent;
  let fixture: ComponentFixture<AdminInvestmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminInvestmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminInvestmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
