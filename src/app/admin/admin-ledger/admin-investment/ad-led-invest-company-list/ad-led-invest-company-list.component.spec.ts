import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdLedInvestCompanyListComponent } from './ad-led-invest-company-list.component';

describe('AdLedInvestCompanyListComponent', () => {
  let component: AdLedInvestCompanyListComponent;
  let fixture: ComponentFixture<AdLedInvestCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdLedInvestCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdLedInvestCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
