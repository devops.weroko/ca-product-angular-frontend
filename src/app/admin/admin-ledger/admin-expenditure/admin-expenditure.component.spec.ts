import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminExpenditureComponent } from './admin-expenditure.component';

describe('AdminExpenditureComponent', () => {
  let component: AdminExpenditureComponent;
  let fixture: ComponentFixture<AdminExpenditureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminExpenditureComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminExpenditureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
