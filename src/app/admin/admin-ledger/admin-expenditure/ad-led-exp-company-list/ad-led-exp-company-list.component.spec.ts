import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdLedExpCompanyListComponent } from './ad-led-exp-company-list.component';

describe('AdLedExpCompanyListComponent', () => {
  let component: AdLedExpCompanyListComponent;
  let fixture: ComponentFixture<AdLedExpCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdLedExpCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdLedExpCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
