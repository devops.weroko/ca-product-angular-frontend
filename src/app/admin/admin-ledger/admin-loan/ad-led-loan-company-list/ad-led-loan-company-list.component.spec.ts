import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdLedLoanCompanyListComponent } from './ad-led-loan-company-list.component';

describe('AdLedLoanCompanyListComponent', () => {
  let component: AdLedLoanCompanyListComponent;
  let fixture: ComponentFixture<AdLedLoanCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdLedLoanCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdLedLoanCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
