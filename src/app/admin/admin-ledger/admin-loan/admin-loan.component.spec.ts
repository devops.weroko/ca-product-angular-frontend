import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminLoanComponent } from './admin-loan.component';

describe('AdminLoanComponent', () => {
  let component: AdminLoanComponent;
  let fixture: ComponentFixture<AdminLoanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminLoanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminLoanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
