import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdLedEmpCompanyListComponent } from './ad-led-emp-company-list.component';

describe('AdLedEmpCompanyListComponent', () => {
  let component: AdLedEmpCompanyListComponent;
  let fixture: ComponentFixture<AdLedEmpCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdLedEmpCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdLedEmpCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
