import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdLedBillCompanyListComponent } from './ad-led-bill-company-list.component';

describe('AdLedBillCompanyListComponent', () => {
  let component: AdLedBillCompanyListComponent;
  let fixture: ComponentFixture<AdLedBillCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdLedBillCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdLedBillCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
