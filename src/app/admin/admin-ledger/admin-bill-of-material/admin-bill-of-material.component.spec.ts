import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminBillOfMaterialComponent } from './admin-bill-of-material.component';

describe('AdminBillOfMaterialComponent', () => {
  let component: AdminBillOfMaterialComponent;
  let fixture: ComponentFixture<AdminBillOfMaterialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminBillOfMaterialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminBillOfMaterialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
