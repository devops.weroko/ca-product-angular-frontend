import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminCustomerContactPersonComponent } from './admin-customer-contact-person.component';

describe('AdminCustomerContactPersonComponent', () => {
  let component: AdminCustomerContactPersonComponent;
  let fixture: ComponentFixture<AdminCustomerContactPersonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminCustomerContactPersonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminCustomerContactPersonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
