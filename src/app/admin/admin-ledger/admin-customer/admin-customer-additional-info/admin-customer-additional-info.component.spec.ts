import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminCustomerAdditionalInfoComponent } from './admin-customer-additional-info.component';

describe('AdminCustomerAdditionalInfoComponent', () => {
  let component: AdminCustomerAdditionalInfoComponent;
  let fixture: ComponentFixture<AdminCustomerAdditionalInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminCustomerAdditionalInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminCustomerAdditionalInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
