import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminCustomerTaxRegComponent } from './admin-customer-tax-reg.component';

describe('AdminCustomerTaxRegComponent', () => {
  let component: AdminCustomerTaxRegComponent;
  let fixture: ComponentFixture<AdminCustomerTaxRegComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminCustomerTaxRegComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminCustomerTaxRegComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
