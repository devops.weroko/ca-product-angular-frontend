import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminCustomerNotesComponent } from './admin-customer-notes.component';

describe('AdminCustomerNotesComponent', () => {
  let component: AdminCustomerNotesComponent;
  let fixture: ComponentFixture<AdminCustomerNotesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminCustomerNotesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminCustomerNotesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
