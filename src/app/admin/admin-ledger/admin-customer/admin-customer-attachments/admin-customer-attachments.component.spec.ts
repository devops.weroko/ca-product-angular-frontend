import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminCustomerAttachmentsComponent } from './admin-customer-attachments.component';

describe('AdminCustomerAttachmentsComponent', () => {
  let component: AdminCustomerAttachmentsComponent;
  let fixture: ComponentFixture<AdminCustomerAttachmentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminCustomerAttachmentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminCustomerAttachmentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
