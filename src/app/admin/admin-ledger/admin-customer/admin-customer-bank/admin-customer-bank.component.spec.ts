import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminCustomerBankComponent } from './admin-customer-bank.component';

describe('AdminCustomerBankComponent', () => {
  let component: AdminCustomerBankComponent;
  let fixture: ComponentFixture<AdminCustomerBankComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminCustomerBankComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminCustomerBankComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
