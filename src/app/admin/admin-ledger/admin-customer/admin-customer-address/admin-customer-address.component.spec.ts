import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminCustomerAddressComponent } from './admin-customer-address.component';

describe('AdminCustomerAddressComponent', () => {
  let component: AdminCustomerAddressComponent;
  let fixture: ComponentFixture<AdminCustomerAddressComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminCustomerAddressComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminCustomerAddressComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
