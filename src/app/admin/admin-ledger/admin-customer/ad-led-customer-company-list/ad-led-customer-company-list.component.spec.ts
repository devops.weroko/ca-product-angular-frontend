import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdLedCustomerCompanyListComponent } from './ad-led-customer-company-list.component';

describe('AdLedCustomerCompanyListComponent', () => {
  let component: AdLedCustomerCompanyListComponent;
  let fixture: ComponentFixture<AdLedCustomerCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdLedCustomerCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdLedCustomerCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
