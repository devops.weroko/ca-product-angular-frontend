import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminCustomerInfoComponent } from './admin-customer-info.component';

describe('AdminCustomerInfoComponent', () => {
  let component: AdminCustomerInfoComponent;
  let fixture: ComponentFixture<AdminCustomerInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminCustomerInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminCustomerInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
