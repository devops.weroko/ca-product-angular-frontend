import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdLedSupCompanyListComponent } from './ad-led-sup-company-list.component';

describe('AdLedSupCompanyListComponent', () => {
  let component: AdLedSupCompanyListComponent;
  let fixture: ComponentFixture<AdLedSupCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdLedSupCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdLedSupCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
