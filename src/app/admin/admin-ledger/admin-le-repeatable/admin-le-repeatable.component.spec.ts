import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminLeRepeatableComponent } from './admin-le-repeatable.component';

describe('AdminLeRepeatableComponent', () => {
  let component: AdminLeRepeatableComponent;
  let fixture: ComponentFixture<AdminLeRepeatableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminLeRepeatableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminLeRepeatableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
