import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdLedLeRepCompanyListComponent } from './ad-led-le-rep-company-list.component';

describe('AdLedLeRepCompanyListComponent', () => {
  let component: AdLedLeRepCompanyListComponent;
  let fixture: ComponentFixture<AdLedLeRepCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdLedLeRepCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdLedLeRepCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
