import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminBankInfoComponentComponent } from './admin-bank-info-component.component';

describe('AdminBankInfoComponentComponent', () => {
  let component: AdminBankInfoComponentComponent;
  let fixture: ComponentFixture<AdminBankInfoComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminBankInfoComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminBankInfoComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
