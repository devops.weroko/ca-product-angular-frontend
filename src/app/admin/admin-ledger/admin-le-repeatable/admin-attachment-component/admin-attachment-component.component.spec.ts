import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminAttachmentComponentComponent } from './admin-attachment-component.component';

describe('AdminAttachmentComponentComponent', () => {
  let component: AdminAttachmentComponentComponent;
  let fixture: ComponentFixture<AdminAttachmentComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminAttachmentComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminAttachmentComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
