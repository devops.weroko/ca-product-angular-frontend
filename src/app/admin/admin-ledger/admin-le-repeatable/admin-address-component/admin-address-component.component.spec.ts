import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminAddressComponentComponent } from './admin-address-component.component';

describe('AdminAddressComponentComponent', () => {
  let component: AdminAddressComponentComponent;
  let fixture: ComponentFixture<AdminAddressComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminAddressComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminAddressComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
