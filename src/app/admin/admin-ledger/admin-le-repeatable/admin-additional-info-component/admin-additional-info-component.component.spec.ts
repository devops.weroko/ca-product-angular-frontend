import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminAdditionalInfoComponentComponent } from './admin-additional-info-component.component';

describe('AdminAdditionalInfoComponentComponent', () => {
  let component: AdminAdditionalInfoComponentComponent;
  let fixture: ComponentFixture<AdminAdditionalInfoComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminAdditionalInfoComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminAdditionalInfoComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
