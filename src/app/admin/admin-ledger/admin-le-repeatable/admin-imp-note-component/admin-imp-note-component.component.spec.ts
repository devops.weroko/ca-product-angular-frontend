import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminImpNoteComponentComponent } from './admin-imp-note-component.component';

describe('AdminImpNoteComponentComponent', () => {
  let component: AdminImpNoteComponentComponent;
  let fixture: ComponentFixture<AdminImpNoteComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminImpNoteComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminImpNoteComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
