import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminContactPersonComponentComponent } from './admin-contact-person-component.component';

describe('AdminContactPersonComponentComponent', () => {
  let component: AdminContactPersonComponentComponent;
  let fixture: ComponentFixture<AdminContactPersonComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminContactPersonComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminContactPersonComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
