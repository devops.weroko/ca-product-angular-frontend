import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdLedBankCompanyListComponent } from './ad-led-bank-company-list.component';

describe('AdLedBankCompanyListComponent', () => {
  let component: AdLedBankCompanyListComponent;
  let fixture: ComponentFixture<AdLedBankCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdLedBankCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdLedBankCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
