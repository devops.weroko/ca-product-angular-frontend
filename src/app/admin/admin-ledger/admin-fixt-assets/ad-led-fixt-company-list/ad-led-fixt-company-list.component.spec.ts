import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdLedFixtCompanyListComponent } from './ad-led-fixt-company-list.component';

describe('AdLedFixtCompanyListComponent', () => {
  let component: AdLedFixtCompanyListComponent;
  let fixture: ComponentFixture<AdLedFixtCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdLedFixtCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdLedFixtCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
