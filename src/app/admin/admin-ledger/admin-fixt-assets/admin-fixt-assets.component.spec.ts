import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminFixtAssetsComponent } from './admin-fixt-assets.component';

describe('AdminFixtAssetsComponent', () => {
  let component: AdminFixtAssetsComponent;
  let fixture: ComponentFixture<AdminFixtAssetsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminFixtAssetsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminFixtAssetsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
