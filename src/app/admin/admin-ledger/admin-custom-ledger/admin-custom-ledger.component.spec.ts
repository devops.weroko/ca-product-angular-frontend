import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminCustomLedgerComponent } from './admin-custom-ledger.component';

describe('AdminCustomLedgerComponent', () => {
  let component: AdminCustomLedgerComponent;
  let fixture: ComponentFixture<AdminCustomLedgerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminCustomLedgerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminCustomLedgerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
