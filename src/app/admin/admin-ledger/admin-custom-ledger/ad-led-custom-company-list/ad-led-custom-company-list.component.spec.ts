import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdLedCustomCompanyListComponent } from './ad-led-custom-company-list.component';

describe('AdLedCustomCompanyListComponent', () => {
  let component: AdLedCustomCompanyListComponent;
  let fixture: ComponentFixture<AdLedCustomCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdLedCustomCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdLedCustomCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
