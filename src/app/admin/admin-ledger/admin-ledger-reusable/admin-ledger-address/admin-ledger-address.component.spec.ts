import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminLedgerAddressComponent } from './admin-ledger-address.component';

describe('AdminLedgerAddressComponent', () => {
  let component: AdminLedgerAddressComponent;
  let fixture: ComponentFixture<AdminLedgerAddressComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminLedgerAddressComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminLedgerAddressComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
