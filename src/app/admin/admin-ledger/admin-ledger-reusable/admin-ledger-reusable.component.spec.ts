import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminLedgerReusableComponent } from './admin-ledger-reusable.component';

describe('AdminLedgerReusableComponent', () => {
  let component: AdminLedgerReusableComponent;
  let fixture: ComponentFixture<AdminLedgerReusableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminLedgerReusableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminLedgerReusableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
