import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdLedLedReuCompanyListComponent } from './ad-led-led-reu-company-list.component';

describe('AdLedLedReuCompanyListComponent', () => {
  let component: AdLedLedReuCompanyListComponent;
  let fixture: ComponentFixture<AdLedLedReuCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdLedLedReuCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdLedLedReuCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
