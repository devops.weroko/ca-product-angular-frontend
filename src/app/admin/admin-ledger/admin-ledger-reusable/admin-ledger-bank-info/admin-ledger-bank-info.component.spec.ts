import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminLedgerBankInfoComponent } from './admin-ledger-bank-info.component';

describe('AdminLedgerBankInfoComponent', () => {
  let component: AdminLedgerBankInfoComponent;
  let fixture: ComponentFixture<AdminLedgerBankInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminLedgerBankInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminLedgerBankInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
