import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminLedgerAttachmentComponent } from './admin-ledger-attachment.component';

describe('AdminLedgerAttachmentComponent', () => {
  let component: AdminLedgerAttachmentComponent;
  let fixture: ComponentFixture<AdminLedgerAttachmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminLedgerAttachmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminLedgerAttachmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
