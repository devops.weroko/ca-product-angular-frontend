import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminLedgerAdditionalNoteComponent } from './admin-ledger-additional-note.component';

describe('AdminLedgerAdditionalNoteComponent', () => {
  let component: AdminLedgerAdditionalNoteComponent;
  let fixture: ComponentFixture<AdminLedgerAdditionalNoteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminLedgerAdditionalNoteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminLedgerAdditionalNoteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
