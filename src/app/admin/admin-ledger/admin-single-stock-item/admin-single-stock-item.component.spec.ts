import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminSingleStockItemComponent } from './admin-single-stock-item.component';

describe('AdminSingleStockItemComponent', () => {
  let component: AdminSingleStockItemComponent;
  let fixture: ComponentFixture<AdminSingleStockItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminSingleStockItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminSingleStockItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
