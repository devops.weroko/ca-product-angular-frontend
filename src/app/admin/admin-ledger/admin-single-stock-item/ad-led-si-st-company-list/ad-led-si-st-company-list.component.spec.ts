import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdLedSiStCompanyListComponent } from './ad-led-si-st-company-list.component';

describe('AdLedSiStCompanyListComponent', () => {
  let component: AdLedSiStCompanyListComponent;
  let fixture: ComponentFixture<AdLedSiStCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdLedSiStCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdLedSiStCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
