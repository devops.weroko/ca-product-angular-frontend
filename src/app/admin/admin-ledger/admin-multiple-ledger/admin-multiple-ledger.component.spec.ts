import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminMultipleLedgerComponent } from './admin-multiple-ledger.component';

describe('AdminMultipleLedgerComponent', () => {
  let component: AdminMultipleLedgerComponent;
  let fixture: ComponentFixture<AdminMultipleLedgerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminMultipleLedgerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminMultipleLedgerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
