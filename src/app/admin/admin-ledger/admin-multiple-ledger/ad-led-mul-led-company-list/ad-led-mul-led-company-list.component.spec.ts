import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdLedMulLedCompanyListComponent } from './ad-led-mul-led-company-list.component';

describe('AdLedMulLedCompanyListComponent', () => {
  let component: AdLedMulLedCompanyListComponent;
  let fixture: ComponentFixture<AdLedMulLedCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdLedMulLedCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdLedMulLedCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
