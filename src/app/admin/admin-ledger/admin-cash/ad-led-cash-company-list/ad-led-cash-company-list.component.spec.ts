import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdLedCashCompanyListComponent } from './ad-led-cash-company-list.component';

describe('AdLedCashCompanyListComponent', () => {
  let component: AdLedCashCompanyListComponent;
  let fixture: ComponentFixture<AdLedCashCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdLedCashCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdLedCashCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
