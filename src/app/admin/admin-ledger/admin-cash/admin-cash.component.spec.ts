import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminCashComponent } from './admin-cash.component';

describe('AdminCashComponent', () => {
  let component: AdminCashComponent;
  let fixture: ComponentFixture<AdminCashComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminCashComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminCashComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
