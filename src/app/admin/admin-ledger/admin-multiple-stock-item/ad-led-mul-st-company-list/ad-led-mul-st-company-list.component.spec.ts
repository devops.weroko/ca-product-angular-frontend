import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdLedMulStCompanyListComponent } from './ad-led-mul-st-company-list.component';

describe('AdLedMulStCompanyListComponent', () => {
  let component: AdLedMulStCompanyListComponent;
  let fixture: ComponentFixture<AdLedMulStCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdLedMulStCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdLedMulStCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
