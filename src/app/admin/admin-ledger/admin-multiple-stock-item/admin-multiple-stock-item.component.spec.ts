import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminMultipleStockItemComponent } from './admin-multiple-stock-item.component';

describe('AdminMultipleStockItemComponent', () => {
  let component: AdminMultipleStockItemComponent;
  let fixture: ComponentFixture<AdminMultipleStockItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminMultipleStockItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminMultipleStockItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
