import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdLedIncomeCompanyListComponent } from './ad-led-income-company-list.component';

describe('AdLedIncomeCompanyListComponent', () => {
  let component: AdLedIncomeCompanyListComponent;
  let fixture: ComponentFixture<AdLedIncomeCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdLedIncomeCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdLedIncomeCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
