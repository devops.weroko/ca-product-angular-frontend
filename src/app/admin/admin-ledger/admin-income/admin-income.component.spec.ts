import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminIncomeComponent } from './admin-income.component';

describe('AdminIncomeComponent', () => {
  let component: AdminIncomeComponent;
  let fixture: ComponentFixture<AdminIncomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminIncomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminIncomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
