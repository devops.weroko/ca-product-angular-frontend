import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdImBulkCompanyListComponent } from './ad-im-bulk-company-list.component';

describe('AdImBulkCompanyListComponent', () => {
  let component: AdImBulkCompanyListComponent;
  let fixture: ComponentFixture<AdImBulkCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdImBulkCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdImBulkCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
