import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminBulkImportComponent } from './admin-bulk-import.component';

describe('AdminBulkImportComponent', () => {
  let component: AdminBulkImportComponent;
  let fixture: ComponentFixture<AdminBulkImportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminBulkImportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminBulkImportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
