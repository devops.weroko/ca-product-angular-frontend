import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminTallyConnectorComponent } from './admin-tally-connector.component';

describe('AdminTallyConnectorComponent', () => {
  let component: AdminTallyConnectorComponent;
  let fixture: ComponentFixture<AdminTallyConnectorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminTallyConnectorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminTallyConnectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
