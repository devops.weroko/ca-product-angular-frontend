import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdImTallyCompanyListComponent } from './ad-im-tally-company-list.component';

describe('AdImTallyCompanyListComponent', () => {
  let component: AdImTallyCompanyListComponent;
  let fixture: ComponentFixture<AdImTallyCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdImTallyCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdImTallyCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
