import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminCompanyNameListingComponent } from './admin-company-name-listing.component';

describe('AdminCompanyNameListingComponent', () => {
  let component: AdminCompanyNameListingComponent;
  let fixture: ComponentFixture<AdminCompanyNameListingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminCompanyNameListingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminCompanyNameListingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
