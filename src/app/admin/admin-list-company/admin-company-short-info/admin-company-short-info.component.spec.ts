import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminCompanyShortInfoComponent } from './admin-company-short-info.component';

describe('AdminCompanyShortInfoComponent', () => {
  let component: AdminCompanyShortInfoComponent;
  let fixture: ComponentFixture<AdminCompanyShortInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminCompanyShortInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminCompanyShortInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
