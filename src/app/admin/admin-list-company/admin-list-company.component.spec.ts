import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminListCompanyComponent } from './admin-list-company.component';

describe('AdminListCompanyComponent', () => {
  let component: AdminListCompanyComponent;
  let fixture: ComponentFixture<AdminListCompanyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminListCompanyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminListCompanyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
