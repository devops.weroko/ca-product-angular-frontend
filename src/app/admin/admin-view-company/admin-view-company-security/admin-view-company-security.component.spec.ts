import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminViewCompanySecurityComponent } from './admin-view-company-security.component';

describe('AdminViewCompanySecurityComponent', () => {
  let component: AdminViewCompanySecurityComponent;
  let fixture: ComponentFixture<AdminViewCompanySecurityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminViewCompanySecurityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminViewCompanySecurityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
