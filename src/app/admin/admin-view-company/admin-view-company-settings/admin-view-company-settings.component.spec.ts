import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminViewCompanySettingsComponent } from './admin-view-company-settings.component';

describe('AdminViewCompanySettingsComponent', () => {
  let component: AdminViewCompanySettingsComponent;
  let fixture: ComponentFixture<AdminViewCompanySettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminViewCompanySettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminViewCompanySettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
