import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminViewDocumentComponent } from './admin-view-document.component';

describe('AdminViewDocumentComponent', () => {
  let component: AdminViewDocumentComponent;
  let fixture: ComponentFixture<AdminViewDocumentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminViewDocumentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminViewDocumentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
