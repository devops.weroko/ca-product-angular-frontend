import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminViewCompanyAuditorsComponent } from './admin-view-company-auditors.component';

describe('AdminViewCompanyAuditorsComponent', () => {
  let component: AdminViewCompanyAuditorsComponent;
  let fixture: ComponentFixture<AdminViewCompanyAuditorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminViewCompanyAuditorsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminViewCompanyAuditorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
