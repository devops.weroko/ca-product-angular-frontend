import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminViewWarehousesComponent } from './admin-view-warehouses.component';

describe('AdminViewWarehousesComponent', () => {
  let component: AdminViewWarehousesComponent;
  let fixture: ComponentFixture<AdminViewWarehousesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminViewWarehousesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminViewWarehousesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
