import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminViewCompanyAddressComponent } from './admin-view-company-address.component';

describe('AdminViewCompanyAddressComponent', () => {
  let component: AdminViewCompanyAddressComponent;
  let fixture: ComponentFixture<AdminViewCompanyAddressComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminViewCompanyAddressComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminViewCompanyAddressComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
