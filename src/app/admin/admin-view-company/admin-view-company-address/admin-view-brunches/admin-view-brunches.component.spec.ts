import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminViewBrunchesComponent } from './admin-view-brunches.component';

describe('AdminViewBrunchesComponent', () => {
  let component: AdminViewBrunchesComponent;
  let fixture: ComponentFixture<AdminViewBrunchesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminViewBrunchesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminViewBrunchesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
