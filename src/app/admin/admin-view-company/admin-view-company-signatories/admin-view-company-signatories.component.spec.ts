import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminViewCompanySignatoriesComponent } from './admin-view-company-signatories.component';

describe('AdminViewCompanySignatoriesComponent', () => {
  let component: AdminViewCompanySignatoriesComponent;
  let fixture: ComponentFixture<AdminViewCompanySignatoriesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminViewCompanySignatoriesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminViewCompanySignatoriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
