import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminViewCompanyHoldingComponent } from './admin-view-company-holding.component';

describe('AdminViewCompanyHoldingComponent', () => {
  let component: AdminViewCompanyHoldingComponent;
  let fixture: ComponentFixture<AdminViewCompanyHoldingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminViewCompanyHoldingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminViewCompanyHoldingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
