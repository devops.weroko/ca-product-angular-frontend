import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminViewCompanyInfoComponent } from './admin-view-company-info.component';

describe('AdminViewCompanyInfoComponent', () => {
  let component: AdminViewCompanyInfoComponent;
  let fixture: ComponentFixture<AdminViewCompanyInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminViewCompanyInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminViewCompanyInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
