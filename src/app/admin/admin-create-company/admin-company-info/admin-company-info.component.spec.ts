import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminCompanyInfoComponent } from './admin-company-info.component';

describe('AdminCompanyInfoComponent', () => {
  let component: AdminCompanyInfoComponent;
  let fixture: ComponentFixture<AdminCompanyInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminCompanyInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminCompanyInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
