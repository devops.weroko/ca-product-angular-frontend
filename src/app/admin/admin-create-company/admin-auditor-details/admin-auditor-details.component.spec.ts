import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminAuditorDetailsComponent } from './admin-auditor-details.component';

describe('AdminAuditorDetailsComponent', () => {
  let component: AdminAuditorDetailsComponent;
  let fixture: ComponentFixture<AdminAuditorDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminAuditorDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminAuditorDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
