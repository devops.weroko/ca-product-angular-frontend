import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminComWarehouseDetailsComponent } from './admin-com-warehouse-details.component';

describe('AdminComWarehouseDetailsComponent', () => {
  let component: AdminComWarehouseDetailsComponent;
  let fixture: ComponentFixture<AdminComWarehouseDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminComWarehouseDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminComWarehouseDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
