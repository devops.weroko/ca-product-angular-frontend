import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminCompanyAddressComponent } from './admin-company-address.component';

describe('AdminCompanyAddressComponent', () => {
  let component: AdminCompanyAddressComponent;
  let fixture: ComponentFixture<AdminCompanyAddressComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminCompanyAddressComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminCompanyAddressComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
