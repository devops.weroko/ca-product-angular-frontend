import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminComBranchDetailsComponent } from './admin-com-branch-details.component';

describe('AdminComBranchDetailsComponent', () => {
  let component: AdminComBranchDetailsComponent;
  let fixture: ComponentFixture<AdminComBranchDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminComBranchDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminComBranchDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
