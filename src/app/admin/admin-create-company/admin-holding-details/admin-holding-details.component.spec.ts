import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminHoldingDetailsComponent } from './admin-holding-details.component';

describe('AdminHoldingDetailsComponent', () => {
  let component: AdminHoldingDetailsComponent;
  let fixture: ComponentFixture<AdminHoldingDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminHoldingDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminHoldingDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
