import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminComSecuritySettingsComponent } from './admin-com-security-settings.component';

describe('AdminComSecuritySettingsComponent', () => {
  let component: AdminComSecuritySettingsComponent;
  let fixture: ComponentFixture<AdminComSecuritySettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminComSecuritySettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminComSecuritySettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
