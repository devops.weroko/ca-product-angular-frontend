import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminComVoucherSettingsInputComponent } from './admin-com-voucher-settings-input.component';

describe('AdminComVoucherSettingsInputComponent', () => {
  let component: AdminComVoucherSettingsInputComponent;
  let fixture: ComponentFixture<AdminComVoucherSettingsInputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminComVoucherSettingsInputComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminComVoucherSettingsInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
