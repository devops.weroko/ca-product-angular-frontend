import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminComVoucherSettingsComponent } from './admin-com-voucher-settings.component';

describe('AdminComVoucherSettingsComponent', () => {
  let component: AdminComVoucherSettingsComponent;
  let fixture: ComponentFixture<AdminComVoucherSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminComVoucherSettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminComVoucherSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
