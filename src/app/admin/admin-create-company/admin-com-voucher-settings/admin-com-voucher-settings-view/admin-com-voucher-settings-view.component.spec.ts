import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminComVoucherSettingsViewComponent } from './admin-com-voucher-settings-view.component';

describe('AdminComVoucherSettingsViewComponent', () => {
  let component: AdminComVoucherSettingsViewComponent;
  let fixture: ComponentFixture<AdminComVoucherSettingsViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminComVoucherSettingsViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminComVoucherSettingsViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
