import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminCompanySettingsComponent } from './admin-company-settings.component';

describe('AdminCompanySettingsComponent', () => {
  let component: AdminCompanySettingsComponent;
  let fixture: ComponentFixture<AdminCompanySettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminCompanySettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminCompanySettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
