import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminSignatoriesDetailsComponent } from './admin-signatories-details.component';

describe('AdminSignatoriesDetailsComponent', () => {
  let component: AdminSignatoriesDetailsComponent;
  let fixture: ComponentFixture<AdminSignatoriesDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminSignatoriesDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminSignatoriesDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
