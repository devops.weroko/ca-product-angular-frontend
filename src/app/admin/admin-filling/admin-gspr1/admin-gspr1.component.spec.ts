import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminGspr1Component } from './admin-gspr1.component';

describe('AdminGspr1Component', () => {
  let component: AdminGspr1Component;
  let fixture: ComponentFixture<AdminGspr1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminGspr1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminGspr1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
