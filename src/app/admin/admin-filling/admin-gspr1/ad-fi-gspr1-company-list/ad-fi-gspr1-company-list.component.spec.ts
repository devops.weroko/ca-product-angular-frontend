import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdFiGspr1CompanyListComponent } from './ad-fi-gspr1-company-list.component';

describe('AdFiGspr1CompanyListComponent', () => {
  let component: AdFiGspr1CompanyListComponent;
  let fixture: ComponentFixture<AdFiGspr1CompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdFiGspr1CompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdFiGspr1CompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
