import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminFillingComponent } from './admin-filling.component';

describe('AdminFillingComponent', () => {
  let component: AdminFillingComponent;
  let fixture: ComponentFixture<AdminFillingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminFillingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminFillingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
