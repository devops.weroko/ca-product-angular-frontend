import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminGstr9Component } from './admin-gstr9.component';

describe('AdminGstr9Component', () => {
  let component: AdminGstr9Component;
  let fixture: ComponentFixture<AdminGstr9Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminGstr9Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminGstr9Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
