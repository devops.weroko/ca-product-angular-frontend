import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdFiGstr9CompanyListComponent } from './ad-fi-gstr9-company-list.component';

describe('AdFiGstr9CompanyListComponent', () => {
  let component: AdFiGstr9CompanyListComponent;
  let fixture: ComponentFixture<AdFiGstr9CompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdFiGstr9CompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdFiGstr9CompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
