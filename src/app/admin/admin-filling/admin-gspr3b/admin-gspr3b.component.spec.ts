import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminGspr3bComponent } from './admin-gspr3b.component';

describe('AdminGspr3bComponent', () => {
  let component: AdminGspr3bComponent;
  let fixture: ComponentFixture<AdminGspr3bComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminGspr3bComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminGspr3bComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
