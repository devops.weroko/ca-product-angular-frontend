import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdFiGspr3bCompanyListComponent } from './ad-fi-gspr3b-company-list.component';

describe('AdFiGspr3bCompanyListComponent', () => {
  let component: AdFiGspr3bCompanyListComponent;
  let fixture: ComponentFixture<AdFiGspr3bCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdFiGspr3bCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdFiGspr3bCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
