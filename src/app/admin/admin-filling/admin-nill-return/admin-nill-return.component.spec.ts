import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminNillReturnComponent } from './admin-nill-return.component';

describe('AdminNillReturnComponent', () => {
  let component: AdminNillReturnComponent;
  let fixture: ComponentFixture<AdminNillReturnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminNillReturnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminNillReturnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
