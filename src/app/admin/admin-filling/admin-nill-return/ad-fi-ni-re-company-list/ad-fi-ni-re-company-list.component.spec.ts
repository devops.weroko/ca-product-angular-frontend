import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdFiNiReCompanyListComponent } from './ad-fi-ni-re-company-list.component';

describe('AdFiNiReCompanyListComponent', () => {
  let component: AdFiNiReCompanyListComponent;
  let fixture: ComponentFixture<AdFiNiReCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdFiNiReCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdFiNiReCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
