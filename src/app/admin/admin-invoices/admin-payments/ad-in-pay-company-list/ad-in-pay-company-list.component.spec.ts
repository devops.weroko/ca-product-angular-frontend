import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdInPayCompanyListComponent } from './ad-in-pay-company-list.component';

describe('AdInPayCompanyListComponent', () => {
  let component: AdInPayCompanyListComponent;
  let fixture: ComponentFixture<AdInPayCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdInPayCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdInPayCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
