import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdInStCompanyListComponent } from './ad-in-st-company-list.component';

describe('AdInStCompanyListComponent', () => {
  let component: AdInStCompanyListComponent;
  let fixture: ComponentFixture<AdInStCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdInStCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdInStCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
