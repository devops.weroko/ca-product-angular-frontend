import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdInSaCompanyListComponent } from './ad-in-sa-company-list.component';

describe('AdInSaCompanyListComponent', () => {
  let component: AdInSaCompanyListComponent;
  let fixture: ComponentFixture<AdInSaCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdInSaCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdInSaCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
