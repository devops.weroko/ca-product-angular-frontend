import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdInPurCompanyListComponent } from './ad-in-pur-company-list.component';

describe('AdInPurCompanyListComponent', () => {
  let component: AdInPurCompanyListComponent;
  let fixture: ComponentFixture<AdInPurCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdInPurCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdInPurCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
