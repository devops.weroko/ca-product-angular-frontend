import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdMaCompanyListComponent } from './ad-ma-company-list.component';

describe('AdMaCompanyListComponent', () => {
  let component: AdMaCompanyListComponent;
  let fixture: ComponentFixture<AdMaCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdMaCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdMaCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
