import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminAsidebarComponent } from './admin-asidebar.component';

describe('AdminAsidebarComponent', () => {
  let component: AdminAsidebarComponent;
  let fixture: ComponentFixture<AdminAsidebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminAsidebarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminAsidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
