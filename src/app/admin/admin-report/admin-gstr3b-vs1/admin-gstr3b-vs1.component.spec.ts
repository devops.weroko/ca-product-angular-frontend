import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminGstr3bVs1Component } from './admin-gstr3b-vs1.component';

describe('AdminGstr3bVs1Component', () => {
  let component: AdminGstr3bVs1Component;
  let fixture: ComponentFixture<AdminGstr3bVs1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminGstr3bVs1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminGstr3bVs1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
