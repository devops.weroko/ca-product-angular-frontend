import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdReGstr3bVs1CompanyListComponent } from './ad-re-gstr3b-vs1-company-list.component';

describe('AdReGstr3bVs1CompanyListComponent', () => {
  let component: AdReGstr3bVs1CompanyListComponent;
  let fixture: ComponentFixture<AdReGstr3bVs1CompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdReGstr3bVs1CompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdReGstr3bVs1CompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
