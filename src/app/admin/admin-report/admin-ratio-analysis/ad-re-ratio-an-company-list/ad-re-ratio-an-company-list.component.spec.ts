import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdReRatioAnCompanyListComponent } from './ad-re-ratio-an-company-list.component';

describe('AdReRatioAnCompanyListComponent', () => {
  let component: AdReRatioAnCompanyListComponent;
  let fixture: ComponentFixture<AdReRatioAnCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdReRatioAnCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdReRatioAnCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
