import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminRatioAnalysisComponent } from './admin-ratio-analysis.component';

describe('AdminRatioAnalysisComponent', () => {
  let component: AdminRatioAnalysisComponent;
  let fixture: ComponentFixture<AdminRatioAnalysisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminRatioAnalysisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminRatioAnalysisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
