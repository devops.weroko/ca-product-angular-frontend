import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminBalanceSheetChartComponent } from './admin-balance-sheet-chart.component';

describe('AdminBalanceSheetChartComponent', () => {
  let component: AdminBalanceSheetChartComponent;
  let fixture: ComponentFixture<AdminBalanceSheetChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminBalanceSheetChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminBalanceSheetChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
