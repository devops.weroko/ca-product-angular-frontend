import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdReBaShCompanyListComponent } from './ad-re-ba-sh-company-list.component';

describe('AdReBaShCompanyListComponent', () => {
  let component: AdReBaShCompanyListComponent;
  let fixture: ComponentFixture<AdReBaShCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdReBaShCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdReBaShCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
