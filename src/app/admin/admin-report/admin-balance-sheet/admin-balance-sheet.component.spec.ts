import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminBalanceSheetComponent } from './admin-balance-sheet.component';

describe('AdminBalanceSheetComponent', () => {
  let component: AdminBalanceSheetComponent;
  let fixture: ComponentFixture<AdminBalanceSheetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminBalanceSheetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminBalanceSheetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
