import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminGstr3bVs2aComponent } from './admin-gstr3b-vs2a.component';

describe('AdminGstr3bVs2aComponent', () => {
  let component: AdminGstr3bVs2aComponent;
  let fixture: ComponentFixture<AdminGstr3bVs2aComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminGstr3bVs2aComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminGstr3bVs2aComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
