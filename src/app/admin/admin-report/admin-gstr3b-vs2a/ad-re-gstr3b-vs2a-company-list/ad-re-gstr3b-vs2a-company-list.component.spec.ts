import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdReGstr3bVs2aCompanyListComponent } from './ad-re-gstr3b-vs2a-company-list.component';

describe('AdReGstr3bVs2aCompanyListComponent', () => {
  let component: AdReGstr3bVs2aCompanyListComponent;
  let fixture: ComponentFixture<AdReGstr3bVs2aCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdReGstr3bVs2aCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdReGstr3bVs2aCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
