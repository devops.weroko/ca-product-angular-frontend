import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminProfitLossStatementsComponent } from './admin-profit-loss-statements.component';

describe('AdminProfitLossStatementsComponent', () => {
  let component: AdminProfitLossStatementsComponent;
  let fixture: ComponentFixture<AdminProfitLossStatementsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminProfitLossStatementsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminProfitLossStatementsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
