import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdRePrLoStCompanyListComponent } from './ad-re-pr-lo-st-company-list.component';

describe('AdRePrLoStCompanyListComponent', () => {
  let component: AdRePrLoStCompanyListComponent;
  let fixture: ComponentFixture<AdRePrLoStCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdRePrLoStCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdRePrLoStCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
