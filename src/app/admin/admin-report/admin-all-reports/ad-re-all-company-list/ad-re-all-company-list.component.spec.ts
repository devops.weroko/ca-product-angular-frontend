import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdReAllCompanyListComponent } from './ad-re-all-company-list.component';

describe('AdReAllCompanyListComponent', () => {
  let component: AdReAllCompanyListComponent;
  let fixture: ComponentFixture<AdReAllCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdReAllCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdReAllCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
