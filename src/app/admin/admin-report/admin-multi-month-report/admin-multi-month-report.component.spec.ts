import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminMultiMonthReportComponent } from './admin-multi-month-report.component';

describe('AdminMultiMonthReportComponent', () => {
  let component: AdminMultiMonthReportComponent;
  let fixture: ComponentFixture<AdminMultiMonthReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminMultiMonthReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminMultiMonthReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
