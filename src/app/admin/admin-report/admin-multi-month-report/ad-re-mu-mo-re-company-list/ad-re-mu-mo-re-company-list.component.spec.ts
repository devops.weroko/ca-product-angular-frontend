import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdReMuMoReCompanyListComponent } from './ad-re-mu-mo-re-company-list.component';

describe('AdReMuMoReCompanyListComponent', () => {
  let component: AdReMuMoReCompanyListComponent;
  let fixture: ComponentFixture<AdReMuMoReCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdReMuMoReCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdReMuMoReCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
