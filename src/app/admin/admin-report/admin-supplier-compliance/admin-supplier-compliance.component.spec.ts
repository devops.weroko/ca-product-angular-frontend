import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminSupplierComplianceComponent } from './admin-supplier-compliance.component';

describe('AdminSupplierComplianceComponent', () => {
  let component: AdminSupplierComplianceComponent;
  let fixture: ComponentFixture<AdminSupplierComplianceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminSupplierComplianceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminSupplierComplianceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
