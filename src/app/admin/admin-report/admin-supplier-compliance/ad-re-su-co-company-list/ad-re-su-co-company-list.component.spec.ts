import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdReSuCoCompanyListComponent } from './ad-re-su-co-company-list.component';

describe('AdReSuCoCompanyListComponent', () => {
  let component: AdReSuCoCompanyListComponent;
  let fixture: ComponentFixture<AdReSuCoCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdReSuCoCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdReSuCoCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
