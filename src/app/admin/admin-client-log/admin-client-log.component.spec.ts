import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminClientLogComponent } from './admin-client-log.component';

describe('AdminClientLogComponent', () => {
  let component: AdminClientLogComponent;
  let fixture: ComponentFixture<AdminClientLogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminClientLogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminClientLogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
