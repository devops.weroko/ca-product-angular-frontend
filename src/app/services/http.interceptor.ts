import { HttpInterceptor, HttpHandler, HttpRequest, HttpEvent, HttpResponse, HttpErrorResponse }   from '@angular/common/http';
import { Injectable } from "@angular/core"
import { Observable, of } from "rxjs";
import { tap, catchError } from "rxjs/operators";
import { ToastrService } from 'ngx-toastr';
@Injectable()
export class AppHttpInterceptor implements HttpInterceptor {
    constructor(public toasterService: ToastrService) {}
intercept(
        req: HttpRequest<any>,
        next: HttpHandler
      ): Observable<HttpEvent<any>> {
    
        return next.handle(req).pipe(
            tap(evt => {
                
                if (evt instanceof HttpResponse) {
                    console.log(evt.body)
                   
                    if(evt.body && evt.body.message )
                        this.toasterService.success(evt.body.message, evt.body.status, { positionClass: 'toast-bottom-center' });
                }
            }),
            catchError((err: any) => {
                if(err instanceof HttpErrorResponse) {
                    
                    try {
                        if(err.error.error == "Unauthorized")
                            return next.handle(req);
                        this.toasterService.error(err.error.message, err.error.title, { positionClass: 'toast-bottom-center' });
                    } catch(e) {
                        this.toasterService.error('An error occurred', '', { positionClass: 'toast-bottom-center' });
                    }
                    //log error 
                }
                return of(err);
            }));
    
      }
      
}

@Injectable()
  export class BasicAuthInterceptorService implements HttpInterceptor{
  
    constructor() { }
  
    intercept(req: HttpRequest<any>, next: HttpHandler) {
  
      if (sessionStorage.getItem('username') && sessionStorage.getItem('token')) {
          console.log(sessionStorage.getItem('token'))
        req = req.clone({
          setHeaders: {
            Authorization: sessionStorage.getItem('token')
          }
        })
      }
  
      return next.handle(req);
  
    }
  }
  
  