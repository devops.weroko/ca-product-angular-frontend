import { TestBed } from '@angular/core/testing';

import { CaproductService } from './caproduct.service';

describe('CaproductService', () => {
  let service: CaproductService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CaproductService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
