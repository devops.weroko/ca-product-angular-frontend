import { TestBed } from '@angular/core/testing';

import { ViewListCompanyService } from './view-list-company.service';

describe('ViewListCompanyService', () => {
  let service: ViewListCompanyService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ViewListCompanyService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
