import { Injectable } from '@angular/core';
import { CompanyInfo } from 'src/app/shared/models/CompanyInfo';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GlobalConstants } from 'src/app/common/global-constants';



const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    
  })
  
}

@Injectable({
  providedIn: 'root'
})

export class ViewListCompanyService {

  url:string = `${GlobalConstants.apiURL}/viewlistcompany/listcompany/${sessionStorage.getItem('user_id')}`;

  constructor(private http: HttpClient) { }

  getCompanyList(): Observable<CompanyInfo[]> {
    

    return this.http.get<[CompanyInfo]>(this.url, httpOptions);
    
  }
}
