import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CompanyInfo } from 'src/app/shared/models/CompanyInfo';
import { Observable } from 'rxjs';
import { CompanyAddress } from 'src/app/shared/models/CompanyAddress';
import { CompanySettings } from 'src/app/shared/models/CompanySettings';
import { AuditorDetails } from 'src/app/shared/models/AuditorDetails';
import { SignatoriesDetails } from 'src/app/shared/models/SignatoriesDetails';
import { HoldingDetails } from 'src/app/shared/models/HoldingDetails';
import { SubsidiaryDetails } from 'src/app/shared/models/SubsidiaryDetails';
import { PartnerDetails } from 'src/app/shared/models/PartnerDetails';
import { AssociateDetails } from 'src/app/shared/models/AssociateDetails';
import { GlobalConstants } from 'src/app/common/global-constants';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    
    
  })
  
}

@Injectable({
  providedIn: 'root'
})
export class CreateCompanyService {

  createCompanyUrl:string = `${GlobalConstants.apiURL}/createcompany`;
  user_id=2;
  company_id='';

 // url:string = 'https://jsonplaceholder.typicode.com/todos?_limit=1';

  constructor(private http: HttpClient) {

   }


   //ADDING COMPANY INFO

   addCompanyInfo(companyInfo: CompanyInfo):Observable<CompanyInfo>{

    const url = `${this.createCompanyUrl}/addCompanyInfo/${sessionStorage.getItem('user_id')}`;
    return this.http.post<CompanyInfo>(url, companyInfo, httpOptions)

   }

   //ADDING COMPANY ADDRESS

   addCompanyAddress(companyAddress: CompanyAddress):Observable<CompanyAddress>{
    const url = `${this.createCompanyUrl}/addCompanyAddress`;
    return this.http.post<CompanyAddress>(url, companyAddress, httpOptions)

   }

   //ADDING COMPANY SETTINGS

   addCompanySettings(companySettings: CompanySettings):Observable<CompanySettings>{

    console.log(companySettings);
    const url = `${this.createCompanyUrl}/addCompanySettings/${this.company_id}`;
    return this.http.post<CompanySettings>(this.createCompanyUrl, companySettings, httpOptions)

   }

   //ADDING SIGNATORIES DETAILS

   addSignatoriesDetails(signatoriesDetails: SignatoriesDetails):Observable<SignatoriesDetails>{
    console.log("hi");
    console.log(signatoriesDetails);
    const url = `${this.createCompanyUrl}/addSignatoriesDetails`;
    return this.http.post<SignatoriesDetails>(url, signatoriesDetails, httpOptions)

   }

   //ADDING AUDITOR DETAILS


   addAuditorDetails(auditorDetails: AuditorDetails):Observable<AuditorDetails>{

    console.log(auditorDetails);
    const url = `${this.createCompanyUrl}/addAuditorDetails/${this.company_id}`;
    return this.http.post<AuditorDetails>(url, auditorDetails, httpOptions)

   }

   //ADDING HOLDING DETAILS


   addHoldingDetails(holdingDetails: HoldingDetails):Observable<HoldingDetails>{

    console.log(holdingDetails);
    const url = `${this.createCompanyUrl}/addHoldingDetails/${this.company_id}`;
    return this.http.post<HoldingDetails>(url, holdingDetails, httpOptions)

   }

   //ADD ASSOCIATE DETAILS

   addAssociateDetails(associateDetails: AssociateDetails):Observable<AssociateDetails>{

    console.log(associateDetails);
    const url = `${this.createCompanyUrl}/addAssociateDetails/${this.company_id}`;
    return this.http.post<AssociateDetails>(url, associateDetails, httpOptions)

   }

   //ADDING SUBSIDIARY DETAILS


   addSubsidiaryDetails(subsidiaryDetails: SubsidiaryDetails):Observable<SubsidiaryDetails>{

    console.log(subsidiaryDetails);
    const url = `${this.createCompanyUrl}/addSubsidiaryDetails/${this.company_id}`;
    return this.http.post<SubsidiaryDetails>(url, subsidiaryDetails, httpOptions)

   }

   //ADDING PARTNER DETAILS


   addPartnerDetails(partnerDetails: PartnerDetails):Observable<PartnerDetails>{

    console.log(partnerDetails);
    const url = `${this.createCompanyUrl}/addPartnerDetails/${this.company_id}`;
    return this.http.post<PartnerDetails>(url, partnerDetails, httpOptions)

   }





// //TEST!!!!
//    // Get CompanyInfo
//   getCompanyInfo(): Observable<CompanyInfo[]> {
    

//     return this.http.get<[CompanyInfo]>(this.url);
    
//   }
}


