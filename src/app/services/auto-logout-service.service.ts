import { Injectable } from '@angular/core';
import { AuthenticationService } from './authentication.service';

const store = require('store');

const MINUTES_UNITL_AUTO_LOGOUT = 5 // in mins
const CHECK_INTERVAL = 1000 // in ms
const STORE_KEY =  'lastAction';


@Injectable({
  providedIn: 'root'
})
export class AutoLogoutServiceService {


  authService : any;

  get lastAction() {
    return parseInt(store.get(STORE_KEY));
  }
  set lastAction(value) {
    store.set(STORE_KEY, value);
  }

  constructor( AuthenticationService) { 
    this.authService = AuthenticationService;
    this.check();
    this.initListener();
    this.initInterval();

  }

  initListener() {
    document.body.addEventListener('click', () => this.reset());
  }

  reset() {
    this.lastAction = Date.now();
  }

  initInterval() {
    console.log("checking")
    setInterval(() => {
      this.check();
    }, CHECK_INTERVAL);
  }

  check() {
    const now = Date.now();
    const timeleft = this.lastAction + MINUTES_UNITL_AUTO_LOGOUT * 60 * 1000;
    const diff = timeleft - now;
    const isTimeout = diff < 0;

    if (isTimeout && this.authService.isUserLoggedIn) {
      this.authService.logOut();
    }
  }
}
