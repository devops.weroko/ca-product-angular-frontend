import { Injectable } from '@angular/core';
import { CompanyRegistration } from '../models/CompanyRegistration';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ResponseMsg } from '../models/ResponseMsg';
import { map } from 'rxjs/operators';
import { GlobalConstants } from "../common/global-constants"

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    
    
  })
  
}

@Injectable({
  providedIn: 'root'
})



export class CaproductService {

  constructor(private http: HttpClient) { }



  registerUser(registerInfo :  CompanyRegistration){
    console.log(registerInfo);
    const url = `${GlobalConstants.apiURL}/registerUser`;
    console.log(url);
    return this.http.post(url,registerInfo,httpOptions);
    

  }

  getAllUserDetails(username){

    console.log("getting user info")
    const url = `${GlobalConstants.apiURL}/userinfo/${username}`;
    return this.http.get<any>(url,httpOptions).pipe(map(
      userInfo => {
        console.log(userInfo);
        sessionStorage.setItem('user_id',userInfo.user_id);

        return userInfo;
       }
      )
    );
  }


  


}
