import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { GlobalConstants } from '../common/global-constants';
import { Router } from '@angular/router';


export class JwtResponse{
  constructor(
    public jwttoken:string,
     ) {}
  
}

export class User{
  constructor(
    public status:string,
     ) {}
  
}

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(private httpClient:HttpClient, private router: Router) { }


  authenticate(username, password) {
    return this.httpClient.post<any>(`${GlobalConstants.apiURL}/authenticate`,{username,password}).pipe(
     map(
       userData => {
        sessionStorage.setItem('username',username);
        let tokenStr= 'Bearer '+userData.token;
        sessionStorage.setItem('token', tokenStr);
        return userData;
       }
     )

    );
  }


  isUserLoggedIn() {
    let user = sessionStorage.getItem('username')
    
    return !(user === null)
  }

  logOut() {
    sessionStorage.removeItem('username');
    sessionStorage.clear();
    this.router.navigate(['/login'])
  }


}
