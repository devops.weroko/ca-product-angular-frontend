import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {RegistrationComponent} from './registration/registration.component';
import { ForgotPassComponent } from './forgot-pass/forgot-pass.component';
import { LandingComponent } from './landing/landing.component';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './client/dashboard/dashboard.component';
import { CreateCompanyComponent } from './client/components/create-company/create-company.component';
import { ComVoucherSettingsComponent } from './client/components/create-company/com-voucher-settings/com-voucher-settings.component';

import { ListCompanyComponent } from './client/components/list-company/list-company.component';
import { AuthGuardService } from './services/auth-guard.service';
import { ClientLayoutComponent } from "./layout/client-layout/client-layout.component"
import { Page404notfoundComponent } from './page404notfound/page404notfound.component';

const routes: Routes = [
    {path: 'registration', component:RegistrationComponent},
    {path: 'forgotpass', component:ForgotPassComponent},
    {path:  '', component:LandingComponent },
    {path: 'login', component: LoginComponent },
    //{path: 'client', component: DashboardComponent},
    //{path: 'client/createcompany', component: CreateCompanyComponent},
    // {path: 'client/vouchersettings', component: ComVoucherSettingsComponent},
    // {path: 'client/viewcompany', component: ViewCompanyComponent},
    // {path: 'client/companylist', component: ListCompanyComponent, canActivate:[AuthGuardService]},
    {path: 'test', component: ClientLayoutComponent},

    
     {
      path: 'client',
      component: ClientLayoutComponent,
      canActivate:[AuthGuardService],
      children: [
        {
          path: 'createcompany',
          component: CreateCompanyComponent
        },
        {
          path: 'companylist',
          component: ListCompanyComponent
        }
      ]
    },

   { path: 'err', component: Page404notfoundComponent, pathMatch: 'full' }



];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
