import { Directive, HostBinding, HostListener } from '@angular/core';

@Directive({
  selector: '[appAppDropdown]'
})
export class AppDropdownDirective {

  constructor() { }
  @HostBinding('class.open') isOpen = false;

@HostListener('click') toggleOpen($event){
this.isOpen = !this.isOpen;

  }
}
