import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import { AppHttpInterceptor } from './services/http.interceptor';
import { BasicAuthInterceptorService } from './services/http.interceptor';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LandingComponent } from './landing/landing.component';
import { LoginComponent } from './login/login.component';
import { RegistrationComponent } from './registration/registration.component';
import { ForgotPassComponent } from './forgot-pass/forgot-pass.component';



import { NavbarComponent } from './client/navbar/navbar.component';
import { SidebarComponent } from './client/sidebar/sidebar.component';
import { AsidebarComponent } from './client/asidebar/asidebar.component';
import { FooterComponent } from './footer/footer.component';


import { DashboardComponent } from './client/dashboard/dashboard.component';



import { ComBranchDetailsComponent } from './client/components/create-company/company-address/com-branch-details/com-branch-details.component';
import { ComWarehouseDetailsComponent } from './client/components/create-company/company-address/com-warehouse-details/com-warehouse-details.component';


import { CompanyShortInfoComponent } from './client/list-company/company-short-info/company-short-info.component';


import { LedgerComponent } from './client/ledger/ledger.component';
import { SupplierComponent } from './client/ledger/supplier/supplier.component';
import { CustomerComponent } from './client/ledger/customer/customer.component';
import { ExpenditureComponent } from './client/ledger/expenditure/expenditure.component';
import { IncomeComponent } from './client/ledger/income/income.component';
import { CashComponent } from './client/ledger/cash/cash.component';
import { BankComponent } from './client/ledger/bank/bank.component';
import { FixtAssetsComponent } from './client/ledger/fixt-assets/fixt-assets.component';
import { LoanComponent } from './client/ledger/loan/loan.component';
import { InvestmentComponent } from './client/ledger/investment/investment.component';
import { EmployeeComponent } from './client/ledger/employee/employee.component';
import { MultipleLedgerComponent } from './client/ledger/multiple-ledger/multiple-ledger.component';
import { MultipleStockItemComponent } from './client/ledger/multiple-stock-item/multiple-stock-item.component';
import { BillOfMaterialComponent } from './client/ledger/bill-of-material/bill-of-material.component';
import { SingleStockItemComponent } from './client/ledger/single-stock-item/single-stock-item.component';
import { LedgerAddressComponent } from './client/ledger/ledger-reusable/ledger-address/ledger-address.component';
import { LedgerAttachmentComponent } from './client/ledger/ledger-reusable/ledger-attachment/ledger-attachment.component';
import { LedgerBankInfoComponent } from './client/ledger/ledger-reusable/ledger-bank-info/ledger-bank-info.component';
import { LedgerAdditionalNoteComponent } from './client/ledger/ledger-reusable/ledger-additional-note/ledger-additional-note.component';
import { CustomLedgerComponent } from './client/ledger/custom-ledger/custom-ledger.component';

import { CustomerInfoComponent } from './client/ledger/customer/customer-info/customer-info.component';
import { LeCuAddressComponent } from './client/ledger/customer/le-cu-address/le-cu-address.component';
import { LeCuTaxRegComponent } from './client/ledger/customer/le-cu-tax-reg/le-cu-tax-reg.component';
import { LeCuBankComponent } from './client/ledger/customer/le-cu-bank/le-cu-bank.component';
import { LeCuContactPersonComponent } from './client/ledger/customer/le-cu-contact-person/le-cu-contact-person.component';
import { LeCuAttachmentsComponent } from './client/ledger/customer/le-cu-attachments/le-cu-attachments.component';
import { LeCuAditionalInfoComponent } from './client/ledger/customer/le-cu-aditional-info/le-cu-aditional-info.component';
import { LeCuNotesComponent } from './client/ledger/customer/le-cu-notes/le-cu-notes.component';
import { LeRepeatableComponent } from './client/ledger/le-repeatable/le-repeatable.component';
import { AddressComponentComponent } from './client/ledger/le-repeatable/address-component/address-component.component';
import { AttachmentComponentComponent } from './client/ledger/le-repeatable/attachment-component/attachment-component.component';
import { BankInfoComponentComponent } from './client/ledger/le-repeatable/bank-info-component/bank-info-component.component';
import { AditionalInfoComponentComponent } from './client/ledger/le-repeatable/aditional-info-component/aditional-info-component.component';
import { ContactPersonComponentComponent } from './client/ledger/le-repeatable/contact-person-component/contact-person-component.component';
import { ImpNoteComponentComponent } from './client/ledger/le-repeatable/imp-note-component/imp-note-component.component';
import { FillingComponent } from './client/filling/filling.component';
import { Gspr1Component } from './client/filling/gspr1/gspr1.component';
import { NillReturnComponent } from './client/filling/nill-return/nill-return.component';
import { Gspr3bComponent } from './client/filling/gspr3b/gspr3b.component';
import { Gstr9Component } from './client/filling/gstr9/gstr9.component';
import { InvoicesComponent } from './client/invoices/invoices.component';
import { SalesComponent } from './client/invoices/sales/sales.component';
import { PurchaseComponent } from './client/invoices/purchase/purchase.component';
import { PaymentsComponent } from './client/invoices/payments/payments.component';
import { StockComponent } from './client/invoices/stock/stock.component';
import { ReportComponent } from './client/report/report.component';
import { MultiMonthReportComponent } from './client/report/multi-month-report/multi-month-report.component';
import { Gstr3bVs2aComponent } from './client/report/gstr3b-vs2a/gstr3b-vs2a.component';
import { SupplierComplianceComponent } from './client/report/supplier-compliance/supplier-compliance.component';
import { Gstr3bVs1Component } from './client/report/gstr3b-vs1/gstr3b-vs1.component';
import { AllReportsComponent } from './client/report/all-reports/all-reports.component';
import { ImportComponent } from './client/import/import.component';
import { TallyConnectorComponent } from './client/import/tally-connector/tally-connector.component';
import { BulkImportComponent } from './client/import/bulk-import/bulk-import.component';
import { BalanceSheetComponent } from './client/report/balance-sheet/balance-sheet.component';
import { ProfitLossStatementsComponent } from './client/report/profit-loss-statements/profit-loss-statements.component';
import { RatioAnalysisComponent } from './client/report/ratio-analysis/ratio-analysis.component';
import { BalanceSheetChartComponent } from './client/report/balance-sheet/balance-sheet-chart/balance-sheet-chart.component';
import { ClientLayoutComponent } from './layout/client-layout/client-layout.component';

import { ClientNavbarComponent } from './layout/client-layout/client-navbar/client-navbar.component';
import { ClientSidebarComponent } from './layout/client-layout/client-sidebar/client-sidebar.component';
import { ClientASidebarComponent } from './layout/client-layout/client-asidebar/client-asidebar.component';
import { Page404notfoundComponent } from './page404notfound/page404notfound.component';
import { ClientModule } from './client/client.module';
import { EditorComponent } from './editor/editor.component';
import { EditorAsidebarComponent } from './editor/editor-asidebar/editor-asidebar.component';
import { EditorClientLogComponent } from './editor/editor-client-log/editor-client-log.component';
import { EditorDashboardComponent } from './editor/editor-dashboard/editor-dashboard.component';
import { EditorFillingComponent } from './editor/editor-filling/editor-filling.component';
import { EditorImportComponent } from './editor/editor-import/editor-import.component';
import { EditorInvoicesComponent } from './editor/editor-invoices/editor-invoices.component';
import { EditorLedgerComponent } from './editor/editor-ledger/editor-ledger.component';
import { EditorMasterComponent } from './editor/editor-master/editor-master.component';
import { EditorNavbarComponent } from './editor/editor-navbar/editor-navbar.component';
import { EditorReportComponent } from './editor/editor-report/editor-report.component';
import { EditorSidebarComponent } from './editor/editor-sidebar/editor-sidebar.component';
import { EditorViewCompanyComponent } from './editor/editor-view-company/editor-view-company.component';
import { EditorCreateCompanyComponent } from './editor/editor-create-company/editor-create-company.component';
import { EditorAuditorDetailsComponent } from './editor/editor-create-company/editor-auditor-details/editor-auditor-details.component';
import { EditorComSecuritySettingsComponent } from './editor/editor-create-company/editor-com-security-settings/editor-com-security-settings.component';
import { EditorComVoucherSettingsComponent } from './editor/editor-create-company/editor-com-voucher-settings/editor-com-voucher-settings.component';
import { EditorCompanyAddressComponent } from './editor/editor-create-company/editor-company-address/editor-company-address.component';
import { EditorCompanyInfoComponent } from './editor/editor-create-company/editor-company-info/editor-company-info.component';
import { EditorCompanySettingsComponent } from './editor/editor-create-company/editor-company-settings/editor-company-settings.component';
import { EditorHoldingDetailsComponent } from './editor/editor-create-company/editor-holding-details/editor-holding-details.component';
import { EditorSignatoriesDetailsComponent } from './editor/editor-create-company/editor-signatories-details/editor-signatories-details.component';
import { EditorGspr1Component } from './editor/editor-filling/editor-gspr1/editor-gspr1.component';
import { EditorGspr3bComponent } from './editor/editor-filling/editor-gspr3b/editor-gspr3b.component';
import { EditorGstr9Component } from './editor/editor-filling/editor-gstr9/editor-gstr9.component';
import { EditorNillReturnComponent } from './editor/editor-filling/editor-nill-return/editor-nill-return.component';
import { EditorBulkImportComponent } from './editor/editor-import/editor-bulk-import/editor-bulk-import.component';
import { EditorTallyConnectorComponent } from './editor/editor-import/editor-tally-connector/editor-tally-connector.component';
import { EditorPaymentsComponent } from './editor/editor-invoices/editor-payments/editor-payments.component';
import { EditorPurchaseComponent } from './editor/editor-invoices/editor-purchase/editor-purchase.component';
import { EditorSalesComponent } from './editor/editor-invoices/editor-sales/editor-sales.component';
import { EditorStockComponent } from './editor/editor-invoices/editor-stock/editor-stock.component';
import { EditorCompanyNameListingComponent } from './editor/editor-list-company/editor-company-name-listing/editor-company-name-listing.component';
import { EditorCompanyShortInfoComponent } from './editor/editor-list-company/editor-company-short-info/editor-company-short-info.component';
import { EditorTodoListComponent } from './editor/editor-list-company/editor-todo-list/editor-todo-list.component';
import { EditorAllReportsComponent } from './editor/editor-report/editor-all-reports/editor-all-reports.component';
import { EditorBalanceSheetComponent } from './editor/editor-report/editor-balance-sheet/editor-balance-sheet.component';
import { EditorGstr3bVs1Component } from './editor/editor-report/editor-gstr3b-vs1/editor-gstr3b-vs1.component';
import { EditorGstr3bVs2aComponent } from './editor/editor-report/editor-gstr3b-vs2a/editor-gstr3b-vs2a.component';
import { EditorMultiMonthReportComponent } from './editor/editor-report/editor-multi-month-report/editor-multi-month-report.component';
import { EditorProfitLossStatementsComponent } from './editor/editor-report/editor-profit-loss-statements/editor-profit-loss-statements.component';
import { EditorRatioAnalysisComponent } from './editor/editor-report/editor-ratio-analysis/editor-ratio-analysis.component';
import { EditorSupplierComplianceComponent } from './editor/editor-report/editor-supplier-compliance/editor-supplier-compliance.component';
import { EditorDocumentsComponent } from './editor/editor-view-company/editor-documents/editor-documents.component';
import { EditorViewCompanyAddressComponent } from './editor/editor-view-company/editor-view-company-address/editor-view-company-address.component';
import { EditorViewCompanyAuditorsComponent } from './editor/editor-view-company/editor-view-company-auditors/editor-view-company-auditors.component';
import { EditorViewCompanyHoldingComponent } from './editor/editor-view-company/editor-view-company-holding/editor-view-company-holding.component';
import { EditorViewCompanyInfoComponent } from './editor/editor-view-company/editor-view-company-info/editor-view-company-info.component';
import { EditorViewCompanySequrityComponent } from './editor/editor-view-company/editor-view-company-sequrity/editor-view-company-sequrity.component';
import { EditorViewCompanySettingsComponent } from './editor/editor-view-company/editor-view-company-settings/editor-view-company-settings.component';
import { EditorViewCompanySignetoriesComponent } from './editor/editor-view-company/editor-view-company-signetories/editor-view-company-signetories.component';
import { EditorBankComponent } from './editor/editor-ledger/editor-bank/editor-bank.component';
import { EditorBillOfMaterialComponent } from './editor/editor-ledger/editor-bill-of-material/editor-bill-of-material.component';
import { EditorCashComponent } from './editor/editor-ledger/editor-cash/editor-cash.component';
import { EditorCustomLedgerComponent } from './editor/editor-ledger/editor-custom-ledger/editor-custom-ledger.component';
import { EditorCustomerComponent } from './editor/editor-ledger/editor-customer/editor-customer.component';
import { EditorEmployeeComponent } from './editor/editor-ledger/editor-employee/editor-employee.component';
import { EditorExpenditureComponent } from './editor/editor-ledger/editor-expenditure/editor-expenditure.component';
import { EditorFixtAssetsComponent } from './editor/editor-ledger/editor-fixt-assets/editor-fixt-assets.component';
import { EditorIncomeComponent } from './editor/editor-ledger/editor-income/editor-income.component';
import { EditorInvestmentComponent } from './editor/editor-ledger/editor-investment/editor-investment.component';
import { EditorLeRepeatableComponent } from './editor/editor-ledger/editor-le-repeatable/editor-le-repeatable.component';
import { EditorLedgerReusableComponent } from './editor/editor-ledger/editor-ledger-reusable/editor-ledger-reusable.component';
import { EditorLoanComponent } from './editor/editor-ledger/editor-loan/editor-loan.component';
import { EditorMultipleLedgerComponent } from './editor/editor-ledger/editor-multiple-ledger/editor-multiple-ledger.component';
import { EditorMultipleStockItemComponent } from './editor/editor-ledger/editor-multiple-stock-item/editor-multiple-stock-item.component';
import { EditorSingleStockItemComponent } from './editor/editor-ledger/editor-single-stock-item/editor-single-stock-item.component';
import { EditorSupplierComponent } from './editor/editor-ledger/editor-supplier/editor-supplier.component';
import { EditorListCompanyComponent } from './editor/editor-list-company/editor-list-company.component';
import { EditorCustomerInfoComponent } from './editor/editor-ledger/editor-customer/editor-customer-info/editor-customer-info.component';
import { EditorCustomerAddressComponent } from './editor/editor-ledger/editor-customer/editor-customer-address/editor-customer-address.component';
import { EditorCustomerAdditionalInfoComponent } from './editor/editor-ledger/editor-customer/editor-customer-additional-info/editor-customer-additional-info.component';
import { EditorCustomerAttachmentsComponent } from './editor/editor-ledger/editor-customer/editor-customer-attachments/editor-customer-attachments.component';
import { EditorCustomerBankComponent } from './editor/editor-ledger/editor-customer/editor-customer-bank/editor-customer-bank.component';
import { EditorCustomerContactPersonComponent } from './editor/editor-ledger/editor-customer/editor-customer-contact-person/editor-customer-contact-person.component';
import { EditorCustomerNotesComponent } from './editor/editor-ledger/editor-customer/editor-customer-notes/editor-customer-notes.component';
import { EditorCustomerTaxRegComponent } from './editor/editor-ledger/editor-customer/editor-customer-tax-reg/editor-customer-tax-reg.component';
import { EditorAditionalInfoComponentComponent } from './editor/editor-ledger/editor-le-repeatable/editor-aditional-info-component/editor-aditional-info-component.component';
import { EditorAttachmentComponentComponent } from './editor/editor-ledger/editor-le-repeatable/editor-attachment-component/editor-attachment-component.component';
import { EditorBankInfoComponentComponent } from './editor/editor-ledger/editor-le-repeatable/editor-bank-info-component/editor-bank-info-component.component';
import { EditorContactPersonComponentComponent } from './editor/editor-ledger/editor-le-repeatable/editor-contact-person-component/editor-contact-person-component.component';
import { EditorImpNoteComponentComponent } from './editor/editor-ledger/editor-le-repeatable/editor-imp-note-component/editor-imp-note-component.component';
import { EditorLedgerAdditionalNoteComponent } from './editor/editor-ledger/editor-ledger-reusable/editor-ledger-additional-note/editor-ledger-additional-note.component';
import { EditorLedgerAddressComponent } from './editor/editor-ledger/editor-ledger-reusable/editor-ledger-address/editor-ledger-address.component';
import { EditorLedgerAttachmentComponent } from './editor/editor-ledger/editor-ledger-reusable/editor-ledger-attachment/editor-ledger-attachment.component';
import { EditorLedgerBankInfoComponent } from './editor/editor-ledger/editor-ledger-reusable/editor-ledger-bank-info/editor-ledger-bank-info.component';
import { EditorBalanceSheetChartComponent } from './editor/editor-report/editor-balance-sheet/editor-balance-sheet-chart/editor-balance-sheet-chart.component';
import { EditorViewDocumentComponent } from './editor/editor-view-company/editor-documents/editor-view-document/editor-view-document.component';
import { EditorViewBrunchesComponent } from './editor/editor-view-company/editor-view-company-address/editor-view-brunches/editor-view-brunches.component';
import { EditorViewWirehousesComponent } from './editor/editor-view-company/editor-view-company-address/editor-view-wirehouses/editor-view-wirehouses.component';
import { EditorComBranchDetailsComponent } from './editor/editor-create-company/editor-company-address/editor-com-branch-details/editor-com-branch-details.component';
import { EditorComVoucherSettingsViewComponent } from './editor/editor-create-company/editor-com-voucher-settings/editor-com-voucher-settings-view/editor-com-voucher-settings-view.component';
import { EditorComVoucherSettingsInputComponent } from './editor/editor-create-company/editor-com-voucher-settings/editor-com-voucher-settings-input/editor-com-voucher-settings-input.component';
import { EditorComWarehouseDetailsComponent } from './editor/editor-create-company/editor-company-address/editor-com-warehouse-details/editor-com-warehouse-details.component';
import { SupervisorComponent } from './supervisor/supervisor.component';
import { SupervisorClientLogComponent } from './supervisor/supervisor-client-log/supervisor-client-log.component';
import { SupervisorCreateCompanyComponent } from './supervisor/supervisor-create-company/supervisor-create-company.component';
import { SupervisorAuditorDetailsComponent } from './supervisor/supervisor-create-company/supervisor-auditor-details/supervisor-auditor-details.component';
import { SupervisorComSecuritySettingsComponent } from './supervisor/supervisor-create-company/supervisor-com-security-settings/supervisor-com-security-settings.component';
import { SupervisorComVoucherSettingsComponent } from './supervisor/supervisor-create-company/supervisor-com-voucher-settings/supervisor-com-voucher-settings.component';
import { SupervisorComVoucherSettingsInputComponent } from './supervisor/supervisor-create-company/supervisor-com-voucher-settings/supervisor-com-voucher-settings-input/supervisor-com-voucher-settings-input.component';
import { SupervisorComVoucherSettingsViewComponent } from './supervisor/supervisor-create-company/supervisor-com-voucher-settings/supervisor-com-voucher-settings-view/supervisor-com-voucher-settings-view.component';
import { SupervisorCompanyAddressComponent } from './supervisor/supervisor-create-company/supervisor-company-address/supervisor-company-address.component';
import { SupervisorComBranchDetailsComponent } from './supervisor/supervisor-create-company/supervisor-company-address/supervisor-com-branch-details/supervisor-com-branch-details.component';
import { SupervisorComWarehouseDetailsComponent } from './supervisor/supervisor-create-company/supervisor-company-address/supervisor-com-warehouse-details/supervisor-com-warehouse-details.component';
import { SupervisorCompanyInfoComponent } from './supervisor/supervisor-create-company/supervisor-company-info/supervisor-company-info.component';
import { SupervisorCompanySettingsComponent } from './supervisor/supervisor-create-company/supervisor-company-settings/supervisor-company-settings.component';
import { SupervisorHoldingDetailsComponent } from './supervisor/supervisor-create-company/supervisor-holding-details/supervisor-holding-details.component';
import { SupervisorSignatoriesDetailsComponent } from './supervisor/supervisor-create-company/supervisor-signatories-details/supervisor-signatories-details.component';
import { SupervisorDashboardComponent } from './supervisor/supervisor-dashboard/supervisor-dashboard.component';
import { SupervisorFillingComponent } from './supervisor/supervisor-filling/supervisor-filling.component';
import { SupervisorGspr1Component } from './supervisor/supervisor-filling/supervisor-gspr1/supervisor-gspr1.component';
import { SupervisorGspr3bComponent } from './supervisor/supervisor-filling/supervisor-gspr3b/supervisor-gspr3b.component';
import { SupervisorGstr9Component } from './supervisor/supervisor-filling/supervisor-gstr9/supervisor-gstr9.component';
import { SupervisorNillReturnComponent } from './supervisor/supervisor-filling/supervisor-nill-return/supervisor-nill-return.component';
import { SupervisorImportComponent } from './supervisor/supervisor-import/supervisor-import.component';
import { SupervisorBulkImportComponent } from './supervisor/supervisor-import/supervisor-bulk-import/supervisor-bulk-import.component';
import { SupervisorTallyConnectorComponent } from './supervisor/supervisor-import/supervisor-tally-connector/supervisor-tally-connector.component';
import { SupervisorInvoicesComponent } from './supervisor/supervisor-invoices/supervisor-invoices.component';
import { SupervisorPaymentsComponent } from './supervisor/supervisor-invoices/supervisor-payments/supervisor-payments.component';
import { SupervisorPurchaseComponent } from './supervisor/supervisor-invoices/supervisor-purchase/supervisor-purchase.component';
import { SupervisorSalesComponent } from './supervisor/supervisor-invoices/supervisor-sales/supervisor-sales.component';
import { SupervisorStockComponent } from './supervisor/supervisor-invoices/supervisor-stock/supervisor-stock.component';
import { SupervisorLedgerComponent } from './supervisor/supervisor-ledger/supervisor-ledger.component';
import { SupervisorBankComponent } from './supervisor/supervisor-ledger/supervisor-bank/supervisor-bank.component';
import { SupervisorBillOfMaterialComponent } from './supervisor/supervisor-ledger/supervisor-bill-of-material/supervisor-bill-of-material.component';
import { SupervisorCashComponent } from './supervisor/supervisor-ledger/supervisor-cash/supervisor-cash.component';
import { SupervisorCustomLedgerComponent } from './supervisor/supervisor-ledger/supervisor-custom-ledger/supervisor-custom-ledger.component';
import { SupervisorCustomerComponent } from './supervisor/supervisor-ledger/supervisor-customer/supervisor-customer.component';
import { SupervisorCustomerAdditionalInfoComponent } from './supervisor/supervisor-ledger/supervisor-customer/supervisor-customer-additional-info/supervisor-customer-additional-info.component';
import { SupervisorCustomerAddressComponent } from './supervisor/supervisor-ledger/supervisor-customer/supervisor-customer-address/supervisor-customer-address.component';
import { SupervisorCustomerAttachmentsComponent } from './supervisor/supervisor-ledger/supervisor-customer/supervisor-customer-attachments/supervisor-customer-attachments.component';
import { SupervisorCustomerBankComponent } from './supervisor/supervisor-ledger/supervisor-customer/supervisor-customer-bank/supervisor-customer-bank.component';
import { SupervisorCustomerContactPersonComponent } from './supervisor/supervisor-ledger/supervisor-customer/supervisor-customer-contact-person/supervisor-customer-contact-person.component';
import { SupervisorCustomerInfoComponent } from './supervisor/supervisor-ledger/supervisor-customer/supervisor-customer-info/supervisor-customer-info.component';
import { SupervisorCustomerNotesComponent } from './supervisor/supervisor-ledger/supervisor-customer/supervisor-customer-notes/supervisor-customer-notes.component';
import { SupervisorCustomerTaxRegComponent } from './supervisor/supervisor-ledger/supervisor-customer/supervisor-customer-tax-reg/supervisor-customer-tax-reg.component';
import { SupervisorEmployeeComponent } from './supervisor/supervisor-ledger/supervisor-employee/supervisor-employee.component';
import { SupervisorExpenditureComponent } from './supervisor/supervisor-ledger/supervisor-expenditure/supervisor-expenditure.component';
import { SupervisorFixtAssetsComponent } from './supervisor/supervisor-ledger/supervisor-fixt-assets/supervisor-fixt-assets.component';
import { SupervisorIncomeComponent } from './supervisor/supervisor-ledger/supervisor-income/supervisor-income.component';
import { SupervisorInvestmentComponent } from './supervisor/supervisor-ledger/supervisor-investment/supervisor-investment.component';
import { SupervisorLeRepeatableComponent } from './supervisor/supervisor-ledger/supervisor-le-repeatable/supervisor-le-repeatable.component';
import { SupervisorAddressComponentComponent } from './supervisor/supervisor-ledger/supervisor-le-repeatable/supervisor-address-component/supervisor-address-component.component';
import { SupervisorAdditionalInfoComponentComponent } from './supervisor/supervisor-ledger/supervisor-le-repeatable/supervisor-additional-info-component/supervisor-additional-info-component.component';
import { SupervisorAttachmentComponentComponent } from './supervisor/supervisor-ledger/supervisor-le-repeatable/supervisor-attachment-component/supervisor-attachment-component.component';
import { SupervisorBankInfoComponentComponent } from './supervisor/supervisor-ledger/supervisor-le-repeatable/supervisor-bank-info-component/supervisor-bank-info-component.component';
import { SupervisorContactPersonComponentComponent } from './supervisor/supervisor-ledger/supervisor-le-repeatable/supervisor-contact-person-component/supervisor-contact-person-component.component';
import { SupervisorImpNoteComponentComponent } from './supervisor/supervisor-ledger/supervisor-le-repeatable/supervisor-imp-note-component/supervisor-imp-note-component.component';
import { SupervisorLoanComponent } from './supervisor/supervisor-ledger/supervisor-loan/supervisor-loan.component';
import { SupervisorMultipleLedgerComponent } from './supervisor/supervisor-ledger/supervisor-multiple-ledger/supervisor-multiple-ledger.component';
import { SupervisorMultipleStockItemComponent } from './supervisor/supervisor-ledger/supervisor-multiple-stock-item/supervisor-multiple-stock-item.component';
import { SupervisorSingleStockItemComponent } from './supervisor/supervisor-ledger/supervisor-single-stock-item/supervisor-single-stock-item.component';
import { SupervisorSupplierComponent } from './supervisor/supervisor-ledger/supervisor-supplier/supervisor-supplier.component';
import { SupervisorListCompanyComponent } from './supervisor/supervisor-list-company/supervisor-list-company.component';
import { SupervisorCompanyNameListingComponent } from './supervisor/supervisor-list-company/supervisor-company-name-listing/supervisor-company-name-listing.component';
import { SupervisorCompanyShortInfoComponent } from './supervisor/supervisor-list-company/supervisor-company-short-info/supervisor-company-short-info.component';
import { SupervisorTodoListComponent } from './supervisor/supervisor-list-company/supervisor-todo-list/supervisor-todo-list.component';
import { SupervisorMasterComponent } from './supervisor/supervisor-master/supervisor-master.component';
import { SupervisorNavbarComponent } from './supervisor/supervisor-navbar/supervisor-navbar.component';
import { SupervisorReportComponent } from './supervisor/supervisor-report/supervisor-report.component';
import { SupervisorAllReportsComponent } from './supervisor/supervisor-report/supervisor-all-reports/supervisor-all-reports.component';
import { SupervisorBalanceSheetComponent } from './supervisor/supervisor-report/supervisor-balance-sheet/supervisor-balance-sheet.component';
import { SupervisorBalanceSheetChartComponent } from './supervisor/supervisor-report/supervisor-balance-sheet/supervisor-balance-sheet-chart/supervisor-balance-sheet-chart.component';
import { SupervisorGstr3bVs1Component } from './supervisor/supervisor-report/supervisor-gstr3b-vs1/supervisor-gstr3b-vs1.component';
import { SupervisorGstr3bVs2aComponent } from './supervisor/supervisor-report/supervisor-gstr3b-vs2a/supervisor-gstr3b-vs2a.component';
import { SupervisorMultiMonthReportComponent } from './supervisor/supervisor-report/supervisor-multi-month-report/supervisor-multi-month-report.component';
import { SupervisorProfitLossStatementsComponent } from './supervisor/supervisor-report/supervisor-profit-loss-statements/supervisor-profit-loss-statements.component';
import { SupervisorRatioAnalysisComponent } from './supervisor/supervisor-report/supervisor-ratio-analysis/supervisor-ratio-analysis.component';
import { SupervisorSupplierComplianceComponent } from './supervisor/supervisor-report/supervisor-supplier-compliance/supervisor-supplier-compliance.component';
import { SupervisorSidebarComponent } from './supervisor/supervisor-sidebar/supervisor-sidebar.component';
import { SupervisorViewCompanyComponent } from './supervisor/supervisor-view-company/supervisor-view-company.component';
import { SupervisorDocumentsComponent } from './supervisor/supervisor-view-company/supervisor-documents/supervisor-documents.component';
import { SupervisorViewDocumentComponent } from './supervisor/supervisor-view-company/supervisor-documents/supervisor-view-document/supervisor-view-document.component';
import { SupervisorViewCompanyAddressComponent } from './supervisor/supervisor-view-company/supervisor-view-company-address/supervisor-view-company-address.component';
import { SupervisorViewBrunchesComponent } from './supervisor/supervisor-view-company/supervisor-view-company-address/supervisor-view-brunches/supervisor-view-brunches.component';
import { SupervisorViewWarehousesComponent } from './supervisor/supervisor-view-company/supervisor-view-company-address/supervisor-view-warehouses/supervisor-view-warehouses.component';
import { SupervisorViewCompanyAuditorsComponent } from './supervisor/supervisor-view-company/supervisor-view-company-auditors/supervisor-view-company-auditors.component';
import { SupervisorViewCompanyHoldingComponent } from './supervisor/supervisor-view-company/supervisor-view-company-holding/supervisor-view-company-holding.component';
import { SupervisorViewCompanyInfoComponent } from './supervisor/supervisor-view-company/supervisor-view-company-info/supervisor-view-company-info.component';
import { SupervisorViewCompanySecurityComponent } from './supervisor/supervisor-view-company/supervisor-view-company-security/supervisor-view-company-security.component';
import { SupervisorViewCompanySettingsComponent } from './supervisor/supervisor-view-company/supervisor-view-company-settings/supervisor-view-company-settings.component';
import { SupervisorAsidebarComponent } from './supervisor/supervisor-asidebar/supervisor-asidebar.component';
import { AdminComponent } from './admin/admin.component';
import { AdminAsidebarComponent } from './admin/admin-asidebar/admin-asidebar.component';
import { AdminClientLogComponent } from './admin/admin-client-log/admin-client-log.component';
import { AdminCreateCompanyComponent } from './admin/admin-create-company/admin-create-company.component';
import { AdminAuditorDetailsComponent } from './admin/admin-create-company/admin-auditor-details/admin-auditor-details.component';
import { AdminComSecuritySettingsComponent } from './admin/admin-create-company/admin-com-security-settings/admin-com-security-settings.component';
import { AdminComVoucherSettingsComponent } from './admin/admin-create-company/admin-com-voucher-settings/admin-com-voucher-settings.component';
import { AdminComVoucherSettingsInputComponent } from './admin/admin-create-company/admin-com-voucher-settings/admin-com-voucher-settings-input/admin-com-voucher-settings-input.component';
import { AdminComVoucherSettingsViewComponent } from './admin/admin-create-company/admin-com-voucher-settings/admin-com-voucher-settings-view/admin-com-voucher-settings-view.component';
import { AdminCompanyAddressComponent } from './admin/admin-create-company/admin-company-address/admin-company-address.component';
import { AdminComBranchDetailsComponent } from './admin/admin-create-company/admin-company-address/admin-com-branch-details/admin-com-branch-details.component';
import { AdminComWarehouseDetailsComponent } from './admin/admin-create-company/admin-company-address/admin-com-warehouse-details/admin-com-warehouse-details.component';
import { AdminCompanyInfoComponent } from './admin/admin-create-company/admin-company-info/admin-company-info.component';
import { AdminCompanySettingsComponent } from './admin/admin-create-company/admin-company-settings/admin-company-settings.component';
import { AdminHoldingDetailsComponent } from './admin/admin-create-company/admin-holding-details/admin-holding-details.component';
import { AdminSignatoriesDetailsComponent } from './admin/admin-create-company/admin-signatories-details/admin-signatories-details.component';
import { AdminDashboardComponent } from './admin/admin-dashboard/admin-dashboard.component';
import { AdminFillingComponent } from './admin/admin-filling/admin-filling.component';
import { AdminGspr1Component } from './admin/admin-filling/admin-gspr1/admin-gspr1.component';
import { AdminGspr3bComponent } from './admin/admin-filling/admin-gspr3b/admin-gspr3b.component';
import { AdminGstr9Component } from './admin/admin-filling/admin-gstr9/admin-gstr9.component';
import { AdminNillReturnComponent } from './admin/admin-filling/admin-nill-return/admin-nill-return.component';
import { AdminImportComponent } from './admin/admin-import/admin-import.component';
import { AdminBulkImportComponent } from './admin/admin-import/admin-bulk-import/admin-bulk-import.component';
import { AdminTallyConnectorComponent } from './admin/admin-import/admin-tally-connector/admin-tally-connector.component';
import { AdminInvoicesComponent } from './admin/admin-invoices/admin-invoices.component';
import { AdminPaymentsComponent } from './admin/admin-invoices/admin-payments/admin-payments.component';
import { AdminPurchaseComponent } from './admin/admin-invoices/admin-purchase/admin-purchase.component';
import { AdminSalesComponent } from './admin/admin-invoices/admin-sales/admin-sales.component';
import { AdminStockComponent } from './admin/admin-invoices/admin-stock/admin-stock.component';
import { AdminLedgerComponent } from './admin/admin-ledger/admin-ledger.component';
import { AdminBankComponent } from './admin/admin-ledger/admin-bank/admin-bank.component';
import { AdminBillOfMaterialComponent } from './admin/admin-ledger/admin-bill-of-material/admin-bill-of-material.component';
import { AdminCashComponent } from './admin/admin-ledger/admin-cash/admin-cash.component';
import { AdminCustomLedgerComponent } from './admin/admin-ledger/admin-custom-ledger/admin-custom-ledger.component';
import { AdminCustomerComponent } from './admin/admin-ledger/admin-customer/admin-customer.component';
import { AdminCustomerAdditionalInfoComponent } from './admin/admin-ledger/admin-customer/admin-customer-additional-info/admin-customer-additional-info.component';
import { AdminCustomerAddressComponent } from './admin/admin-ledger/admin-customer/admin-customer-address/admin-customer-address.component';
import { AdminCustomerAttachmentsComponent } from './admin/admin-ledger/admin-customer/admin-customer-attachments/admin-customer-attachments.component';
import { AdminCustomerBankComponent } from './admin/admin-ledger/admin-customer/admin-customer-bank/admin-customer-bank.component';
import { AdminCustomerContactPersonComponent } from './admin/admin-ledger/admin-customer/admin-customer-contact-person/admin-customer-contact-person.component';
import { AdminCustomerInfoComponent } from './admin/admin-ledger/admin-customer/admin-customer-info/admin-customer-info.component';
import { AdminCustomerNotesComponent } from './admin/admin-ledger/admin-customer/admin-customer-notes/admin-customer-notes.component';
import { AdminCustomerTaxRegComponent } from './admin/admin-ledger/admin-customer/admin-customer-tax-reg/admin-customer-tax-reg.component';
import { AdminEmployeeComponent } from './admin/admin-ledger/admin-employee/admin-employee.component';
import { AdminExpenditureComponent } from './admin/admin-ledger/admin-expenditure/admin-expenditure.component';
import { AdminFixtAssetsComponent } from './admin/admin-ledger/admin-fixt-assets/admin-fixt-assets.component';
import { AdminIncomeComponent } from './admin/admin-ledger/admin-income/admin-income.component';
import { AdminInvestmentComponent } from './admin/admin-ledger/admin-investment/admin-investment.component';
import { AdminLeRepeatableComponent } from './admin/admin-ledger/admin-le-repeatable/admin-le-repeatable.component';
import { AdminAdditionalInfoComponentComponent } from './admin/admin-ledger/admin-le-repeatable/admin-additional-info-component/admin-additional-info-component.component';
import { AdminAddressComponentComponent } from './admin/admin-ledger/admin-le-repeatable/admin-address-component/admin-address-component.component';
import { AdminAttachmentComponentComponent } from './admin/admin-ledger/admin-le-repeatable/admin-attachment-component/admin-attachment-component.component';
import { AdminBankInfoComponentComponent } from './admin/admin-ledger/admin-le-repeatable/admin-bank-info-component/admin-bank-info-component.component';
import { AdminContactPersonComponentComponent } from './admin/admin-ledger/admin-le-repeatable/admin-contact-person-component/admin-contact-person-component.component';
import { AdminImpNoteComponentComponent } from './admin/admin-ledger/admin-le-repeatable/admin-imp-note-component/admin-imp-note-component.component';
import { AdminLedgerReusableComponent } from './admin/admin-ledger/admin-ledger-reusable/admin-ledger-reusable.component';
import { AdminLedgerAdditionalNoteComponent } from './admin/admin-ledger/admin-ledger-reusable/admin-ledger-additional-note/admin-ledger-additional-note.component';
import { AdminLedgerAddressComponent } from './admin/admin-ledger/admin-ledger-reusable/admin-ledger-address/admin-ledger-address.component';
import { AdminLedgerAttachmentComponent } from './admin/admin-ledger/admin-ledger-reusable/admin-ledger-attachment/admin-ledger-attachment.component';
import { AdminLedgerBankInfoComponent } from './admin/admin-ledger/admin-ledger-reusable/admin-ledger-bank-info/admin-ledger-bank-info.component';
import { AdminLoanComponent } from './admin/admin-ledger/admin-loan/admin-loan.component';
import { AdminMultipleLedgerComponent } from './admin/admin-ledger/admin-multiple-ledger/admin-multiple-ledger.component';
import { AdminMultipleStockItemComponent } from './admin/admin-ledger/admin-multiple-stock-item/admin-multiple-stock-item.component';
import { AdminSingleStockItemComponent } from './admin/admin-ledger/admin-single-stock-item/admin-single-stock-item.component';
import { AdminSupplierComponent } from './admin/admin-ledger/admin-supplier/admin-supplier.component';
import { AdminListCompanyComponent } from './admin/admin-list-company/admin-list-company.component';
import { AdminCompanyNameListingComponent } from './admin/admin-list-company/admin-company-name-listing/admin-company-name-listing.component';
import { AdminCompanyShortInfoComponent } from './admin/admin-list-company/admin-company-short-info/admin-company-short-info.component';
import { AdminTodoListComponent } from './admin/admin-list-company/admin-todo-list/admin-todo-list.component';
import { AdminMasterComponent } from './admin/admin-master/admin-master.component';
import { AdminNavbarComponent } from './admin/admin-navbar/admin-navbar.component';
import { AdminReportComponent } from './admin/admin-report/admin-report.component';
import { AdminAllReportsComponent } from './admin/admin-report/admin-all-reports/admin-all-reports.component';
import { AdminBalanceSheetComponent } from './admin/admin-report/admin-balance-sheet/admin-balance-sheet.component';
import { AdminBalanceSheetChartComponent } from './admin/admin-report/admin-balance-sheet/admin-balance-sheet-chart/admin-balance-sheet-chart.component';
import { AdminGstr3bVs1Component } from './admin/admin-report/admin-gstr3b-vs1/admin-gstr3b-vs1.component';
import { AdminGstr3bVs2aComponent } from './admin/admin-report/admin-gstr3b-vs2a/admin-gstr3b-vs2a.component';
import { AdminMultiMonthReportComponent } from './admin/admin-report/admin-multi-month-report/admin-multi-month-report.component';
import { AdminProfitLossStatementsComponent } from './admin/admin-report/admin-profit-loss-statements/admin-profit-loss-statements.component';
import { AdminRatioAnalysisComponent } from './admin/admin-report/admin-ratio-analysis/admin-ratio-analysis.component';
import { AdminSupplierComplianceComponent } from './admin/admin-report/admin-supplier-compliance/admin-supplier-compliance.component';
import { AdminSidebarComponent } from './admin/admin-sidebar/admin-sidebar.component';
import { AdminViewCompanyComponent } from './admin/admin-view-company/admin-view-company.component';
import { AdminDocumentsComponent } from './admin/admin-view-company/admin-documents/admin-documents.component';
import { AdminViewDocumentComponent } from './admin/admin-view-company/admin-documents/admin-view-document/admin-view-document.component';
import { AdminViewCompanyAddressComponent } from './admin/admin-view-company/admin-view-company-address/admin-view-company-address.component';
import { AdminViewBrunchesComponent } from './admin/admin-view-company/admin-view-company-address/admin-view-brunches/admin-view-brunches.component';
import { AdminViewWarehousesComponent } from './admin/admin-view-company/admin-view-company-address/admin-view-warehouses/admin-view-warehouses.component';
import { AdminViewCompanyAuditorsComponent } from './admin/admin-view-company/admin-view-company-auditors/admin-view-company-auditors.component';
import { AdminViewCompanyHoldingComponent } from './admin/admin-view-company/admin-view-company-holding/admin-view-company-holding.component';
import { AdminViewCompanyInfoComponent } from './admin/admin-view-company/admin-view-company-info/admin-view-company-info.component';
import { AdminViewCompanySecurityComponent } from './admin/admin-view-company/admin-view-company-security/admin-view-company-security.component';
import { AdminViewCompanySettingsComponent } from './admin/admin-view-company/admin-view-company-settings/admin-view-company-settings.component';
import { AdminViewCompanySignatoriesComponent } from './admin/admin-view-company/admin-view-company-signatories/admin-view-company-signatories.component';
import { SupervisorLedgerReusableComponent } from './supervisor/supervisor-ledger/supervisor-ledger-reusable/supervisor-ledger-reusable.component';
import { SupervisorLedgerAdditionalNoteComponent } from './supervisor/supervisor-ledger/supervisor-ledger-reusable/supervisor-ledger-additional-note/supervisor-ledger-additional-note.component';
import { SupervisorLedgerAttachmentComponent } from './supervisor/supervisor-ledger/supervisor-ledger-reusable/supervisor-ledger-attachment/supervisor-ledger-attachment.component';
import { SupervisorLedgerBankInfoComponent } from './supervisor/supervisor-ledger/supervisor-ledger-reusable/supervisor-ledger-bank-info/supervisor-ledger-bank-info.component';
import { SupervisorViewCompanySignatoriesComponent } from './supervisor/supervisor-view-company/supervisor-view-company-signatories/supervisor-view-company-signatories.component';
import { AppDropdownDirective } from './common/app-dropdown.directive';


@NgModule({
  declarations: [
    AppComponent,
    LandingComponent,
    LoginComponent,
    RegistrationComponent,
    ForgotPassComponent,
    NavbarComponent,
    SidebarComponent,
    AsidebarComponent,
    FooterComponent,
    DashboardComponent,
  

    CompanyShortInfoComponent,
    LedgerComponent,
    SupplierComponent,
    CustomerComponent,
    ExpenditureComponent,
    IncomeComponent,
    CashComponent,
    BankComponent,
    FixtAssetsComponent,
    LoanComponent,
    InvestmentComponent,
    EmployeeComponent,
    MultipleLedgerComponent,
    MultipleStockItemComponent,
    BillOfMaterialComponent,
    SingleStockItemComponent,
    LedgerAddressComponent,
    LedgerAttachmentComponent,
    LedgerBankInfoComponent,
    LedgerAdditionalNoteComponent,
    CustomLedgerComponent,
    
    CustomerInfoComponent,
    LeCuAddressComponent,
    LeCuTaxRegComponent,
    LeCuBankComponent,
    LeCuContactPersonComponent,
    LeCuAttachmentsComponent,
    LeCuAditionalInfoComponent,
    LeCuNotesComponent,
    LeRepeatableComponent,
    AddressComponentComponent,
    AttachmentComponentComponent,
    BankInfoComponentComponent,
    AditionalInfoComponentComponent,
    ContactPersonComponentComponent,
    ImpNoteComponentComponent,
    FillingComponent,
    Gspr1Component,
    NillReturnComponent,
    Gspr3bComponent,
    Gstr9Component,
    InvoicesComponent,
    SalesComponent,
    PurchaseComponent,
    PaymentsComponent,
    StockComponent,
    ReportComponent,
    MultiMonthReportComponent,
    Gstr3bVs2aComponent,
    SupplierComplianceComponent,
    Gstr3bVs1Component,
    AllReportsComponent,
    ImportComponent,
    TallyConnectorComponent,
    BulkImportComponent,
    BalanceSheetComponent,
    ProfitLossStatementsComponent,
    RatioAnalysisComponent,
    BalanceSheetChartComponent,
    ClientLayoutComponent,
    ClientNavbarComponent,
    ClientSidebarComponent,
    ClientASidebarComponent,
    Page404notfoundComponent,
    EditorComponent,
    EditorAsidebarComponent,
    EditorClientLogComponent,
    EditorDashboardComponent,
    EditorFillingComponent,
    EditorImportComponent,
    EditorInvoicesComponent,
    EditorLedgerComponent,
    EditorMasterComponent,
    EditorNavbarComponent,
    EditorReportComponent,
    EditorSidebarComponent,
    EditorViewCompanyComponent,
    EditorCreateCompanyComponent,
    EditorAuditorDetailsComponent,
    EditorComSecuritySettingsComponent,
    EditorComVoucherSettingsComponent,
    EditorCompanyAddressComponent,
    EditorCompanyInfoComponent,
    EditorCompanySettingsComponent,
    EditorHoldingDetailsComponent,
    EditorSignatoriesDetailsComponent,
    EditorGspr1Component,
    EditorGspr3bComponent,
    EditorGstr9Component,
    EditorNillReturnComponent,
    EditorBulkImportComponent,
    EditorTallyConnectorComponent,
    EditorPaymentsComponent,
    EditorPurchaseComponent,
    EditorSalesComponent,
    EditorStockComponent,
    EditorCompanyNameListingComponent,
    EditorCompanyShortInfoComponent,
    EditorTodoListComponent,
    EditorAllReportsComponent,
    EditorBalanceSheetComponent,
    EditorGstr3bVs1Component,
    EditorGstr3bVs2aComponent,
    EditorMultiMonthReportComponent,
    EditorProfitLossStatementsComponent,
    EditorRatioAnalysisComponent,
    EditorSupplierComplianceComponent,
    EditorDocumentsComponent,
    EditorViewCompanyAddressComponent,
    EditorViewCompanyAuditorsComponent,
    EditorViewCompanyHoldingComponent,
    EditorViewCompanyInfoComponent,
    EditorViewCompanySequrityComponent,
    EditorViewCompanySettingsComponent,
    EditorViewCompanySignetoriesComponent,
    EditorBankComponent,
    EditorBillOfMaterialComponent,
    EditorCashComponent,
    EditorCustomLedgerComponent,
    EditorCustomerComponent,
    EditorEmployeeComponent,
    EditorExpenditureComponent,
    EditorFixtAssetsComponent,
    EditorIncomeComponent,
    EditorInvestmentComponent,
    EditorLeRepeatableComponent,
    EditorLedgerReusableComponent,
    EditorLoanComponent,
    EditorMultipleLedgerComponent,
    EditorMultipleStockItemComponent,
    EditorSingleStockItemComponent,
    EditorSupplierComponent,
    EditorListCompanyComponent,
    EditorCustomerInfoComponent,
    EditorCustomerAddressComponent,
    EditorCustomerAdditionalInfoComponent,
    EditorCustomerAttachmentsComponent,
    EditorCustomerBankComponent,
    EditorCustomerContactPersonComponent,
    EditorCustomerNotesComponent,
    EditorCustomerTaxRegComponent,
    EditorAditionalInfoComponentComponent,
    EditorAttachmentComponentComponent,
    EditorBankInfoComponentComponent,
    EditorContactPersonComponentComponent,
    EditorImpNoteComponentComponent,
    EditorLedgerAdditionalNoteComponent,
    EditorLedgerAddressComponent,
    EditorLedgerAttachmentComponent,
    EditorLedgerBankInfoComponent,
    EditorBalanceSheetChartComponent,
    EditorViewDocumentComponent,
    EditorViewBrunchesComponent,
    EditorViewWirehousesComponent,
   EditorComBranchDetailsComponent,
   EditorComVoucherSettingsViewComponent,
   EditorComVoucherSettingsInputComponent,
   EditorComWarehouseDetailsComponent,
   SupervisorComponent,
   SupervisorClientLogComponent,
   SupervisorCreateCompanyComponent,
   SupervisorAuditorDetailsComponent,
   SupervisorComSecuritySettingsComponent,
   SupervisorComVoucherSettingsComponent,
   SupervisorComVoucherSettingsInputComponent,
   SupervisorComVoucherSettingsViewComponent,
   SupervisorCompanyAddressComponent,
   SupervisorComBranchDetailsComponent,
   SupervisorComWarehouseDetailsComponent,
   SupervisorCompanyInfoComponent,
   SupervisorCompanySettingsComponent,
   SupervisorHoldingDetailsComponent,
   SupervisorSignatoriesDetailsComponent,
   SupervisorDashboardComponent,
   SupervisorFillingComponent,
   SupervisorGspr1Component,
   SupervisorGspr3bComponent,
   SupervisorGstr9Component,
   SupervisorNillReturnComponent,
   SupervisorImportComponent,
   SupervisorBulkImportComponent,
   SupervisorTallyConnectorComponent,
   SupervisorInvoicesComponent,
   SupervisorPaymentsComponent,
   SupervisorPurchaseComponent,
   SupervisorSalesComponent,
   SupervisorStockComponent,
   SupervisorLedgerComponent,
   SupervisorBankComponent,
   SupervisorBillOfMaterialComponent,
   SupervisorCashComponent,
   SupervisorCustomLedgerComponent,
   SupervisorCustomerComponent,
   SupervisorCustomerAdditionalInfoComponent,
   SupervisorCustomerAddressComponent,
   SupervisorCustomerAttachmentsComponent,
   SupervisorCustomerBankComponent,
   SupervisorCustomerContactPersonComponent,
   SupervisorCustomerInfoComponent,
   SupervisorCustomerNotesComponent,
   SupervisorCustomerTaxRegComponent,
   SupervisorEmployeeComponent,
   SupervisorExpenditureComponent,
   SupervisorFixtAssetsComponent,
   SupervisorIncomeComponent,
   SupervisorInvestmentComponent,
   SupervisorLeRepeatableComponent,
   SupervisorAddressComponentComponent,
   SupervisorAdditionalInfoComponentComponent,
   SupervisorAttachmentComponentComponent,
   SupervisorBankInfoComponentComponent,
   SupervisorContactPersonComponentComponent,
   SupervisorImpNoteComponentComponent,
   SupervisorLoanComponent,
   SupervisorMultipleLedgerComponent,
   SupervisorMultipleStockItemComponent,
   SupervisorSingleStockItemComponent,
   SupervisorSupplierComponent,
   SupervisorListCompanyComponent,
   SupervisorCompanyNameListingComponent,
   SupervisorCompanyShortInfoComponent,
   SupervisorTodoListComponent,
   SupervisorMasterComponent,
   SupervisorNavbarComponent,
   SupervisorReportComponent,
   SupervisorAllReportsComponent,
   SupervisorBalanceSheetComponent,
   SupervisorBalanceSheetChartComponent,
   SupervisorGstr3bVs1Component,
   SupervisorGstr3bVs2aComponent,
   SupervisorMultiMonthReportComponent,
   SupervisorProfitLossStatementsComponent,
   SupervisorRatioAnalysisComponent,
   SupervisorSupplierComplianceComponent,
   SupervisorSidebarComponent,
   SupervisorViewCompanyComponent,
   SupervisorDocumentsComponent,
   SupervisorViewDocumentComponent,
   SupervisorViewCompanyAddressComponent,
   SupervisorViewBrunchesComponent,
   SupervisorViewWarehousesComponent,
   SupervisorViewCompanyAuditorsComponent,
   SupervisorViewCompanyHoldingComponent,
   SupervisorViewCompanyInfoComponent,
   SupervisorViewCompanySecurityComponent,
   SupervisorViewCompanySettingsComponent,
   
   SupervisorAsidebarComponent,
   AdminComponent,
   AdminAsidebarComponent,
   AdminClientLogComponent,
   AdminCreateCompanyComponent,
   AdminAuditorDetailsComponent,
   AdminComSecuritySettingsComponent,
   AdminComVoucherSettingsComponent,
   AdminComVoucherSettingsInputComponent,
   AdminComVoucherSettingsViewComponent,
   AdminCompanyAddressComponent,
   AdminComBranchDetailsComponent,
   AdminComWarehouseDetailsComponent,
   AdminCompanyInfoComponent,
   AdminCompanySettingsComponent,
   AdminHoldingDetailsComponent,
   AdminSignatoriesDetailsComponent,
   AdminDashboardComponent,
   AdminFillingComponent,
   AdminGspr1Component,
   AdminGspr3bComponent,
   AdminGstr9Component,
   AdminNillReturnComponent,
   AdminImportComponent,
   AdminBulkImportComponent,
   AdminTallyConnectorComponent,
   AdminInvoicesComponent,
   AdminPaymentsComponent,
   AdminPurchaseComponent,
   AdminSalesComponent,
   AdminStockComponent,
   AdminLedgerComponent,
   AdminBankComponent,
   AdminBillOfMaterialComponent,
   AdminCashComponent,
   AdminCustomLedgerComponent,
   AdminCustomerComponent,
   AdminCustomerAdditionalInfoComponent,
   AdminCustomerAddressComponent,
   AdminCustomerAttachmentsComponent,
   AdminCustomerBankComponent,
   AdminCustomerContactPersonComponent,
   AdminCustomerInfoComponent,
   AdminCustomerNotesComponent,
   AdminCustomerTaxRegComponent,
   AdminEmployeeComponent,
   AdminExpenditureComponent,
   AdminFixtAssetsComponent,
   AdminIncomeComponent,
   AdminInvestmentComponent,
   AdminLeRepeatableComponent,
   AdminAdditionalInfoComponentComponent,
   AdminAddressComponentComponent,
   AdminAttachmentComponentComponent,
   AdminBankInfoComponentComponent,
   AdminContactPersonComponentComponent,
   AdminImpNoteComponentComponent,
   AdminLedgerReusableComponent,
   AdminLedgerAdditionalNoteComponent,
   AdminLedgerAddressComponent,
   AdminLedgerAttachmentComponent,
   AdminLedgerBankInfoComponent,
   AdminLoanComponent,
   AdminMultipleLedgerComponent,
   AdminMultipleStockItemComponent,
   AdminSingleStockItemComponent,
   AdminSupplierComponent,
   AdminListCompanyComponent,
   AdminCompanyNameListingComponent,
   AdminCompanyShortInfoComponent,
   AdminTodoListComponent,
   AdminMasterComponent,
   AdminNavbarComponent,
   AdminReportComponent,
   AdminAllReportsComponent,
   AdminBalanceSheetComponent,
   AdminBalanceSheetChartComponent,
   AdminGstr3bVs1Component,
   AdminGstr3bVs2aComponent,
   AdminMultiMonthReportComponent,
   AdminProfitLossStatementsComponent,
   AdminRatioAnalysisComponent,
   AdminSupplierComplianceComponent,
   AdminSidebarComponent,
   AdminViewCompanyComponent,
   AdminDocumentsComponent,
   AdminViewDocumentComponent,
   AdminViewCompanyAddressComponent,
   AdminViewBrunchesComponent,
   AdminViewWarehousesComponent,
   AdminViewCompanyAuditorsComponent,
   AdminViewCompanyHoldingComponent,
   AdminViewCompanyInfoComponent,
   AdminViewCompanySecurityComponent,
   AdminViewCompanySettingsComponent,
   AdminViewCompanySignatoriesComponent,
   SupervisorLedgerReusableComponent,
   SupervisorLedgerAdditionalNoteComponent,
   SupervisorLedgerAttachmentComponent,
   SupervisorLedgerBankInfoComponent,
   SupervisorViewCompanySignatoriesComponent,
   AppDropdownDirective,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule, 
    ToastrModule.forRoot() ,
    ClientModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: AppHttpInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass:BasicAuthInterceptorService, multi: true }

],
  bootstrap: [AppComponent]
})
export class AppModule { }
