import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuMaCompanyListComponent } from './su-ma-company-list.component';

describe('SuMaCompanyListComponent', () => {
  let component: SuMaCompanyListComponent;
  let fixture: ComponentFixture<SuMaCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuMaCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuMaCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
