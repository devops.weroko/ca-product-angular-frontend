import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorMasterComponent } from './supervisor-master.component';

describe('SupervisorMasterComponent', () => {
  let component: SupervisorMasterComponent;
  let fixture: ComponentFixture<SupervisorMasterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorMasterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorMasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
