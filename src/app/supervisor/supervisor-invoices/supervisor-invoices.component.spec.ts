import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorInvoicesComponent } from './supervisor-invoices.component';

describe('SupervisorInvoicesComponent', () => {
  let component: SupervisorInvoicesComponent;
  let fixture: ComponentFixture<SupervisorInvoicesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorInvoicesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorInvoicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
