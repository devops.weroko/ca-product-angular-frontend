import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorSalesComponent } from './supervisor-sales.component';

describe('SupervisorSalesComponent', () => {
  let component: SupervisorSalesComponent;
  let fixture: ComponentFixture<SupervisorSalesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorSalesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorSalesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
