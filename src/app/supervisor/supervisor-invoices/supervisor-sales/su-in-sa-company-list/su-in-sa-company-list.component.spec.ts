import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuInSaCompanyListComponent } from './su-in-sa-company-list.component';

describe('SuInSaCompanyListComponent', () => {
  let component: SuInSaCompanyListComponent;
  let fixture: ComponentFixture<SuInSaCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuInSaCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuInSaCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
