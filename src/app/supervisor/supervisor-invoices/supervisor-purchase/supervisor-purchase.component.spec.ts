import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorPurchaseComponent } from './supervisor-purchase.component';

describe('SupervisorPurchaseComponent', () => {
  let component: SupervisorPurchaseComponent;
  let fixture: ComponentFixture<SupervisorPurchaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorPurchaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorPurchaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
