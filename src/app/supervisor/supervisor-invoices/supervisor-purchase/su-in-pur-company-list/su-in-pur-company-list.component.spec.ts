import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuInPurCompanyListComponent } from './su-in-pur-company-list.component';

describe('SuInPurCompanyListComponent', () => {
  let component: SuInPurCompanyListComponent;
  let fixture: ComponentFixture<SuInPurCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuInPurCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuInPurCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
