import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorPaymentsComponent } from './supervisor-payments.component';

describe('SupervisorPaymentsComponent', () => {
  let component: SupervisorPaymentsComponent;
  let fixture: ComponentFixture<SupervisorPaymentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorPaymentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorPaymentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
