import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuInPayCompanyListComponent } from './su-in-pay-company-list.component';

describe('SuInPayCompanyListComponent', () => {
  let component: SuInPayCompanyListComponent;
  let fixture: ComponentFixture<SuInPayCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuInPayCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuInPayCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
