import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorStockComponent } from './supervisor-stock.component';

describe('SupervisorStockComponent', () => {
  let component: SupervisorStockComponent;
  let fixture: ComponentFixture<SupervisorStockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorStockComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorStockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
