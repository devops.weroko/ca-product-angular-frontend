import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorViewCompanyInfoComponent } from './supervisor-view-company-info.component';

describe('SupervisorViewCompanyInfoComponent', () => {
  let component: SupervisorViewCompanyInfoComponent;
  let fixture: ComponentFixture<SupervisorViewCompanyInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorViewCompanyInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorViewCompanyInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
