import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorViewCompanySignatoriesComponent } from './supervisor-view-company-signatories.component';

describe('SupervisorViewCompanySignatoriesComponent', () => {
  let component: SupervisorViewCompanySignatoriesComponent;
  let fixture: ComponentFixture<SupervisorViewCompanySignatoriesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorViewCompanySignatoriesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorViewCompanySignatoriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
