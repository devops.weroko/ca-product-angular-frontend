import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorViewCompanyHoldingComponent } from './supervisor-view-company-holding.component';

describe('SupervisorViewCompanyHoldingComponent', () => {
  let component: SupervisorViewCompanyHoldingComponent;
  let fixture: ComponentFixture<SupervisorViewCompanyHoldingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorViewCompanyHoldingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorViewCompanyHoldingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
