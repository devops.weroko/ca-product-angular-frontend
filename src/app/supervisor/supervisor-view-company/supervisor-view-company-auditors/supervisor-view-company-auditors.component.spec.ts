import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorViewCompanyAuditorsComponent } from './supervisor-view-company-auditors.component';

describe('SupervisorViewCompanyAuditorsComponent', () => {
  let component: SupervisorViewCompanyAuditorsComponent;
  let fixture: ComponentFixture<SupervisorViewCompanyAuditorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorViewCompanyAuditorsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorViewCompanyAuditorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
