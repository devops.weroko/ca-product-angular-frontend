import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorViewCompanyComponent } from './supervisor-view-company.component';

describe('SupervisorViewCompanyComponent', () => {
  let component: SupervisorViewCompanyComponent;
  let fixture: ComponentFixture<SupervisorViewCompanyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorViewCompanyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorViewCompanyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
