import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorViewDocumentComponent } from './supervisor-view-document.component';

describe('SupervisorViewDocumentComponent', () => {
  let component: SupervisorViewDocumentComponent;
  let fixture: ComponentFixture<SupervisorViewDocumentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorViewDocumentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorViewDocumentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
