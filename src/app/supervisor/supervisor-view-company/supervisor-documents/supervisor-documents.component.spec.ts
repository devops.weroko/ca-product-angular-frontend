import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorDocumentsComponent } from './supervisor-documents.component';

describe('SupervisorDocumentsComponent', () => {
  let component: SupervisorDocumentsComponent;
  let fixture: ComponentFixture<SupervisorDocumentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorDocumentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorDocumentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
