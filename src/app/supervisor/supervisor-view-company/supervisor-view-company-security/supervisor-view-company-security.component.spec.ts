import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorViewCompanySecurityComponent } from './supervisor-view-company-security.component';

describe('SupervisorViewCompanySecurityComponent', () => {
  let component: SupervisorViewCompanySecurityComponent;
  let fixture: ComponentFixture<SupervisorViewCompanySecurityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorViewCompanySecurityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorViewCompanySecurityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
