import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorViewCompanyAddressComponent } from './supervisor-view-company-address.component';

describe('SupervisorViewCompanyAddressComponent', () => {
  let component: SupervisorViewCompanyAddressComponent;
  let fixture: ComponentFixture<SupervisorViewCompanyAddressComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorViewCompanyAddressComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorViewCompanyAddressComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
