import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorViewWarehousesComponent } from './supervisor-view-warehouses.component';

describe('SupervisorViewWarehousesComponent', () => {
  let component: SupervisorViewWarehousesComponent;
  let fixture: ComponentFixture<SupervisorViewWarehousesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorViewWarehousesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorViewWarehousesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
