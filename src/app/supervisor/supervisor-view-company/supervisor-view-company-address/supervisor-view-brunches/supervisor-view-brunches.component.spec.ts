import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorViewBrunchesComponent } from './supervisor-view-brunches.component';

describe('SupervisorViewBrunchesComponent', () => {
  let component: SupervisorViewBrunchesComponent;
  let fixture: ComponentFixture<SupervisorViewBrunchesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorViewBrunchesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorViewBrunchesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
