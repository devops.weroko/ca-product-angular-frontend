import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorViewCompanySettingsComponent } from './supervisor-view-company-settings.component';

describe('SupervisorViewCompanySettingsComponent', () => {
  let component: SupervisorViewCompanySettingsComponent;
  let fixture: ComponentFixture<SupervisorViewCompanySettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorViewCompanySettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorViewCompanySettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
