import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuImBulkCompanyListComponent } from './su-im-bulk-company-list.component';

describe('SuImBulkCompanyListComponent', () => {
  let component: SuImBulkCompanyListComponent;
  let fixture: ComponentFixture<SuImBulkCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuImBulkCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuImBulkCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
