import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorBulkImportComponent } from './supervisor-bulk-import.component';

describe('SupervisorBulkImportComponent', () => {
  let component: SupervisorBulkImportComponent;
  let fixture: ComponentFixture<SupervisorBulkImportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorBulkImportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorBulkImportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
