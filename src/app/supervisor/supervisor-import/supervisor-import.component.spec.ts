import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorImportComponent } from './supervisor-import.component';

describe('SupervisorImportComponent', () => {
  let component: SupervisorImportComponent;
  let fixture: ComponentFixture<SupervisorImportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorImportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorImportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
