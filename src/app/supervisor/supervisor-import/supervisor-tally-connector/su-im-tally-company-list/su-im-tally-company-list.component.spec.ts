import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuImTallyCompanyListComponent } from './su-im-tally-company-list.component';

describe('SuImTallyCompanyListComponent', () => {
  let component: SuImTallyCompanyListComponent;
  let fixture: ComponentFixture<SuImTallyCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuImTallyCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuImTallyCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
