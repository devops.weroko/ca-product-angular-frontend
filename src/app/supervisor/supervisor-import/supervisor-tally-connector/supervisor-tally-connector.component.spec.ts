import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorTallyConnectorComponent } from './supervisor-tally-connector.component';

describe('SupervisorTallyConnectorComponent', () => {
  let component: SupervisorTallyConnectorComponent;
  let fixture: ComponentFixture<SupervisorTallyConnectorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorTallyConnectorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorTallyConnectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
