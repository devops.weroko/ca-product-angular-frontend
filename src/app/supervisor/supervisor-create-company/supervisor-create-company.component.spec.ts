import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorCreateCompanyComponent } from './supervisor-create-company.component';

describe('SupervisorCreateCompanyComponent', () => {
  let component: SupervisorCreateCompanyComponent;
  let fixture: ComponentFixture<SupervisorCreateCompanyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorCreateCompanyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorCreateCompanyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
