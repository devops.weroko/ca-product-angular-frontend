import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorAuditorDetailsComponent } from './supervisor-auditor-details.component';

describe('SupervisorAuditorDetailsComponent', () => {
  let component: SupervisorAuditorDetailsComponent;
  let fixture: ComponentFixture<SupervisorAuditorDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorAuditorDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorAuditorDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
