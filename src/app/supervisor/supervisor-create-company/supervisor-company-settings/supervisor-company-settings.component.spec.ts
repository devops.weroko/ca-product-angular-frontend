import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorCompanySettingsComponent } from './supervisor-company-settings.component';

describe('SupervisorCompanySettingsComponent', () => {
  let component: SupervisorCompanySettingsComponent;
  let fixture: ComponentFixture<SupervisorCompanySettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorCompanySettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorCompanySettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
