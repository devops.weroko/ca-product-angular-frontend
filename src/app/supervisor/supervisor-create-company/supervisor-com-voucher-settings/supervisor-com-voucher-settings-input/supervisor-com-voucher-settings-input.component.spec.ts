import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorComVoucherSettingsInputComponent } from './supervisor-com-voucher-settings-input.component';

describe('SupervisorComVoucherSettingsInputComponent', () => {
  let component: SupervisorComVoucherSettingsInputComponent;
  let fixture: ComponentFixture<SupervisorComVoucherSettingsInputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorComVoucherSettingsInputComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorComVoucherSettingsInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
