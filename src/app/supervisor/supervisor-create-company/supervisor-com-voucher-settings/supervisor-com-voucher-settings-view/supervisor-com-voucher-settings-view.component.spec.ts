import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorComVoucherSettingsViewComponent } from './supervisor-com-voucher-settings-view.component';

describe('SupervisorComVoucherSettingsViewComponent', () => {
  let component: SupervisorComVoucherSettingsViewComponent;
  let fixture: ComponentFixture<SupervisorComVoucherSettingsViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorComVoucherSettingsViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorComVoucherSettingsViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
