import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorComVoucherSettingsComponent } from './supervisor-com-voucher-settings.component';

describe('SupervisorComVoucherSettingsComponent', () => {
  let component: SupervisorComVoucherSettingsComponent;
  let fixture: ComponentFixture<SupervisorComVoucherSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorComVoucherSettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorComVoucherSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
