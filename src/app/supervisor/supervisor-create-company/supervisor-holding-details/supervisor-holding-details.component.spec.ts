import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorHoldingDetailsComponent } from './supervisor-holding-details.component';

describe('SupervisorHoldingDetailsComponent', () => {
  let component: SupervisorHoldingDetailsComponent;
  let fixture: ComponentFixture<SupervisorHoldingDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorHoldingDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorHoldingDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
