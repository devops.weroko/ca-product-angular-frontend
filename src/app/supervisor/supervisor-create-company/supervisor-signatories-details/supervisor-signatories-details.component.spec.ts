import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorSignatoriesDetailsComponent } from './supervisor-signatories-details.component';

describe('SupervisorSignatoriesDetailsComponent', () => {
  let component: SupervisorSignatoriesDetailsComponent;
  let fixture: ComponentFixture<SupervisorSignatoriesDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorSignatoriesDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorSignatoriesDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
