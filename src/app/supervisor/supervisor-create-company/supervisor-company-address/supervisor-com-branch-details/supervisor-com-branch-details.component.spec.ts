import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorComBranchDetailsComponent } from './supervisor-com-branch-details.component';

describe('SupervisorComBranchDetailsComponent', () => {
  let component: SupervisorComBranchDetailsComponent;
  let fixture: ComponentFixture<SupervisorComBranchDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorComBranchDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorComBranchDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
