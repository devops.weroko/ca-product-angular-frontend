import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorCompanyAddressComponent } from './supervisor-company-address.component';

describe('SupervisorCompanyAddressComponent', () => {
  let component: SupervisorCompanyAddressComponent;
  let fixture: ComponentFixture<SupervisorCompanyAddressComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorCompanyAddressComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorCompanyAddressComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
