import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorComWarehouseDetailsComponent } from './supervisor-com-warehouse-details.component';

describe('SupervisorComWarehouseDetailsComponent', () => {
  let component: SupervisorComWarehouseDetailsComponent;
  let fixture: ComponentFixture<SupervisorComWarehouseDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorComWarehouseDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorComWarehouseDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
