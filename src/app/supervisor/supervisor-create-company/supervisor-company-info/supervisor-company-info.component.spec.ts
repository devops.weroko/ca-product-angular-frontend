import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorCompanyInfoComponent } from './supervisor-company-info.component';

describe('SupervisorCompanyInfoComponent', () => {
  let component: SupervisorCompanyInfoComponent;
  let fixture: ComponentFixture<SupervisorCompanyInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorCompanyInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorCompanyInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
