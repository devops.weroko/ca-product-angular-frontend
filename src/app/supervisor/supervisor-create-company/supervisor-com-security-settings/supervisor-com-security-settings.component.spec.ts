import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorComSecuritySettingsComponent } from './supervisor-com-security-settings.component';

describe('SupervisorComSecuritySettingsComponent', () => {
  let component: SupervisorComSecuritySettingsComponent;
  let fixture: ComponentFixture<SupervisorComSecuritySettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorComSecuritySettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorComSecuritySettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
