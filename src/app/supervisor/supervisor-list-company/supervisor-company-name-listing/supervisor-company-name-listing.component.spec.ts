import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorCompanyNameListingComponent } from './supervisor-company-name-listing.component';

describe('SupervisorCompanyNameListingComponent', () => {
  let component: SupervisorCompanyNameListingComponent;
  let fixture: ComponentFixture<SupervisorCompanyNameListingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorCompanyNameListingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorCompanyNameListingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
