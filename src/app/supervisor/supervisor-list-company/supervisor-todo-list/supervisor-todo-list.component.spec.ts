import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorTodoListComponent } from './supervisor-todo-list.component';

describe('SupervisorTodoListComponent', () => {
  let component: SupervisorTodoListComponent;
  let fixture: ComponentFixture<SupervisorTodoListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorTodoListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorTodoListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
