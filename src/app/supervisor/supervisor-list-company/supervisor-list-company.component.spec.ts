import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorListCompanyComponent } from './supervisor-list-company.component';

describe('SupervisorListCompanyComponent', () => {
  let component: SupervisorListCompanyComponent;
  let fixture: ComponentFixture<SupervisorListCompanyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorListCompanyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorListCompanyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
