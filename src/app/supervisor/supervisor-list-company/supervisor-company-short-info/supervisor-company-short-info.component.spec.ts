import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorCompanyShortInfoComponent } from './supervisor-company-short-info.component';

describe('SupervisorCompanyShortInfoComponent', () => {
  let component: SupervisorCompanyShortInfoComponent;
  let fixture: ComponentFixture<SupervisorCompanyShortInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorCompanyShortInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorCompanyShortInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
