import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorGstr3bVs1Component } from './supervisor-gstr3b-vs1.component';

describe('SupervisorGstr3bVs1Component', () => {
  let component: SupervisorGstr3bVs1Component;
  let fixture: ComponentFixture<SupervisorGstr3bVs1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorGstr3bVs1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorGstr3bVs1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
