import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuReGstr3bVs1CompanyListComponent } from './su-re-gstr3b-vs1-company-list.component';

describe('SuReGstr3bVs1CompanyListComponent', () => {
  let component: SuReGstr3bVs1CompanyListComponent;
  let fixture: ComponentFixture<SuReGstr3bVs1CompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuReGstr3bVs1CompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuReGstr3bVs1CompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
