import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorMultiMonthReportComponent } from './supervisor-multi-month-report.component';

describe('SupervisorMultiMonthReportComponent', () => {
  let component: SupervisorMultiMonthReportComponent;
  let fixture: ComponentFixture<SupervisorMultiMonthReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorMultiMonthReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorMultiMonthReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
