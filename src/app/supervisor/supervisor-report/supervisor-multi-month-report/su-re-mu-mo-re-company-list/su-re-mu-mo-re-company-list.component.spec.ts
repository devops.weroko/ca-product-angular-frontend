import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuReMuMoReCompanyListComponent } from './su-re-mu-mo-re-company-list.component';

describe('SuReMuMoReCompanyListComponent', () => {
  let component: SuReMuMoReCompanyListComponent;
  let fixture: ComponentFixture<SuReMuMoReCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuReMuMoReCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuReMuMoReCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
