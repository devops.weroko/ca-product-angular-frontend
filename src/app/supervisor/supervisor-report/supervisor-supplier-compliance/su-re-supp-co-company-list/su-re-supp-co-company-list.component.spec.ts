import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuReSuppCoCompanyListComponent } from './su-re-supp-co-company-list.component';

describe('SuReSuppCoCompanyListComponent', () => {
  let component: SuReSuppCoCompanyListComponent;
  let fixture: ComponentFixture<SuReSuppCoCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuReSuppCoCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuReSuppCoCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
