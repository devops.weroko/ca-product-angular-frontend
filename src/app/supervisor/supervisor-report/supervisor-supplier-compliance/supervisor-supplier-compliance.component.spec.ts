import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorSupplierComplianceComponent } from './supervisor-supplier-compliance.component';

describe('SupervisorSupplierComplianceComponent', () => {
  let component: SupervisorSupplierComplianceComponent;
  let fixture: ComponentFixture<SupervisorSupplierComplianceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorSupplierComplianceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorSupplierComplianceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
