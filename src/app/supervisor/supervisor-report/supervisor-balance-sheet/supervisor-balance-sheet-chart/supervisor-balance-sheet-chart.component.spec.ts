import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorBalanceSheetChartComponent } from './supervisor-balance-sheet-chart.component';

describe('SupervisorBalanceSheetChartComponent', () => {
  let component: SupervisorBalanceSheetChartComponent;
  let fixture: ComponentFixture<SupervisorBalanceSheetChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorBalanceSheetChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorBalanceSheetChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
