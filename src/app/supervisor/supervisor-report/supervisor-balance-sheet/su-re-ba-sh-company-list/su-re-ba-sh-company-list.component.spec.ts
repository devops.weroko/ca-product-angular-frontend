import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuReBaShCompanyListComponent } from './su-re-ba-sh-company-list.component';

describe('SuReBaShCompanyListComponent', () => {
  let component: SuReBaShCompanyListComponent;
  let fixture: ComponentFixture<SuReBaShCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuReBaShCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuReBaShCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
