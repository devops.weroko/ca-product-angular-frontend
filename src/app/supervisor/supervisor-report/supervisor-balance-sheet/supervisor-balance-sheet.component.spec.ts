import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorBalanceSheetComponent } from './supervisor-balance-sheet.component';

describe('SupervisorBalanceSheetComponent', () => {
  let component: SupervisorBalanceSheetComponent;
  let fixture: ComponentFixture<SupervisorBalanceSheetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorBalanceSheetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorBalanceSheetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
