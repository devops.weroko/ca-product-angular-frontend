import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuRePrLoStCompanyListComponent } from './su-re-pr-lo-st-company-list.component';

describe('SuRePrLoStCompanyListComponent', () => {
  let component: SuRePrLoStCompanyListComponent;
  let fixture: ComponentFixture<SuRePrLoStCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuRePrLoStCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuRePrLoStCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
