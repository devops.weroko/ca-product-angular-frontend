import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorProfitLossStatementsComponent } from './supervisor-profit-loss-statements.component';

describe('SupervisorProfitLossStatementsComponent', () => {
  let component: SupervisorProfitLossStatementsComponent;
  let fixture: ComponentFixture<SupervisorProfitLossStatementsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorProfitLossStatementsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorProfitLossStatementsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
