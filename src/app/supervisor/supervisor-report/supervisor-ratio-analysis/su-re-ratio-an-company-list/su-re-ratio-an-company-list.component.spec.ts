import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuReRatioAnCompanyListComponent } from './su-re-ratio-an-company-list.component';

describe('SuReRatioAnCompanyListComponent', () => {
  let component: SuReRatioAnCompanyListComponent;
  let fixture: ComponentFixture<SuReRatioAnCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuReRatioAnCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuReRatioAnCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
