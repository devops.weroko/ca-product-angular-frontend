import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorRatioAnalysisComponent } from './supervisor-ratio-analysis.component';

describe('SupervisorRatioAnalysisComponent', () => {
  let component: SupervisorRatioAnalysisComponent;
  let fixture: ComponentFixture<SupervisorRatioAnalysisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorRatioAnalysisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorRatioAnalysisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
