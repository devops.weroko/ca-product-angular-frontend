import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuReGstr3bVs2aCompanyListComponent } from './su-re-gstr3b-vs2a-company-list.component';

describe('SuReGstr3bVs2aCompanyListComponent', () => {
  let component: SuReGstr3bVs2aCompanyListComponent;
  let fixture: ComponentFixture<SuReGstr3bVs2aCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuReGstr3bVs2aCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuReGstr3bVs2aCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
