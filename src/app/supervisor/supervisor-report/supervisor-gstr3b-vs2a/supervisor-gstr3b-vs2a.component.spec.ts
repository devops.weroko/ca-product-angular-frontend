import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorGstr3bVs2aComponent } from './supervisor-gstr3b-vs2a.component';

describe('SupervisorGstr3bVs2aComponent', () => {
  let component: SupervisorGstr3bVs2aComponent;
  let fixture: ComponentFixture<SupervisorGstr3bVs2aComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorGstr3bVs2aComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorGstr3bVs2aComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
