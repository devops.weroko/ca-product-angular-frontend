import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuReAllCompanyListComponent } from './su-re-all-company-list.component';

describe('SuReAllCompanyListComponent', () => {
  let component: SuReAllCompanyListComponent;
  let fixture: ComponentFixture<SuReAllCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuReAllCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuReAllCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
