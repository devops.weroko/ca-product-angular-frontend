import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorFillingComponent } from './supervisor-filling.component';

describe('SupervisorFillingComponent', () => {
  let component: SupervisorFillingComponent;
  let fixture: ComponentFixture<SupervisorFillingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorFillingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorFillingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
