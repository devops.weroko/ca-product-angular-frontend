import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuFiGspr1CompanyListComponent } from './su-fi-gspr1-company-list.component';

describe('SuFiGspr1CompanyListComponent', () => {
  let component: SuFiGspr1CompanyListComponent;
  let fixture: ComponentFixture<SuFiGspr1CompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuFiGspr1CompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuFiGspr1CompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
