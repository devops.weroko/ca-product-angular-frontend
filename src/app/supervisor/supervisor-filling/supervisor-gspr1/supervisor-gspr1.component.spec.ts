import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorGspr1Component } from './supervisor-gspr1.component';

describe('SupervisorGspr1Component', () => {
  let component: SupervisorGspr1Component;
  let fixture: ComponentFixture<SupervisorGspr1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorGspr1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorGspr1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
