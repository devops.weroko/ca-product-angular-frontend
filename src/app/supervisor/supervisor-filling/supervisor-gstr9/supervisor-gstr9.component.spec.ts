import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorGstr9Component } from './supervisor-gstr9.component';

describe('SupervisorGstr9Component', () => {
  let component: SupervisorGstr9Component;
  let fixture: ComponentFixture<SupervisorGstr9Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorGstr9Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorGstr9Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
