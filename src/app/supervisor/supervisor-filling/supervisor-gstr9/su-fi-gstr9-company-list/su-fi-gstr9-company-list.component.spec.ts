import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuFiGstr9CompanyListComponent } from './su-fi-gstr9-company-list.component';

describe('SuFiGstr9CompanyListComponent', () => {
  let component: SuFiGstr9CompanyListComponent;
  let fixture: ComponentFixture<SuFiGstr9CompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuFiGstr9CompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuFiGstr9CompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
