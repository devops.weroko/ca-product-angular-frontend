import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorGspr3bComponent } from './supervisor-gspr3b.component';

describe('SupervisorGspr3bComponent', () => {
  let component: SupervisorGspr3bComponent;
  let fixture: ComponentFixture<SupervisorGspr3bComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorGspr3bComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorGspr3bComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
