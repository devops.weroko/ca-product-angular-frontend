import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuFiGspr3bCompanyListComponent } from './su-fi-gspr3b-company-list.component';

describe('SuFiGspr3bCompanyListComponent', () => {
  let component: SuFiGspr3bCompanyListComponent;
  let fixture: ComponentFixture<SuFiGspr3bCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuFiGspr3bCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuFiGspr3bCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
