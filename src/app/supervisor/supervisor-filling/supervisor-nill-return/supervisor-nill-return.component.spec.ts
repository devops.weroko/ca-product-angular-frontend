import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorNillReturnComponent } from './supervisor-nill-return.component';

describe('SupervisorNillReturnComponent', () => {
  let component: SupervisorNillReturnComponent;
  let fixture: ComponentFixture<SupervisorNillReturnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorNillReturnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorNillReturnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
