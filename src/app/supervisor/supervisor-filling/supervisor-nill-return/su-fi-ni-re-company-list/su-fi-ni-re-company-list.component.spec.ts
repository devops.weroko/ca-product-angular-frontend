import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuFiNiReCompanyListComponent } from './su-fi-ni-re-company-list.component';

describe('SuFiNiReCompanyListComponent', () => {
  let component: SuFiNiReCompanyListComponent;
  let fixture: ComponentFixture<SuFiNiReCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuFiNiReCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuFiNiReCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
