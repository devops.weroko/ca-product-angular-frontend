import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorClientLogComponent } from './supervisor-client-log.component';

describe('SupervisorClientLogComponent', () => {
  let component: SupervisorClientLogComponent;
  let fixture: ComponentFixture<SupervisorClientLogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorClientLogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorClientLogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
