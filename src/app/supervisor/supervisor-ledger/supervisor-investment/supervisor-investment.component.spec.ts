import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorInvestmentComponent } from './supervisor-investment.component';

describe('SupervisorInvestmentComponent', () => {
  let component: SupervisorInvestmentComponent;
  let fixture: ComponentFixture<SupervisorInvestmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorInvestmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorInvestmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
