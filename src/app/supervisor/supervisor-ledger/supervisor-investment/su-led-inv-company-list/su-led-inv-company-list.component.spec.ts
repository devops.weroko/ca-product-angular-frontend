import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuLedInvCompanyListComponent } from './su-led-inv-company-list.component';

describe('SuLedInvCompanyListComponent', () => {
  let component: SuLedInvCompanyListComponent;
  let fixture: ComponentFixture<SuLedInvCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuLedInvCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuLedInvCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
