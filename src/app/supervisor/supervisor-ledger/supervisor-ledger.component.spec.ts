import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorLedgerComponent } from './supervisor-ledger.component';

describe('SupervisorLedgerComponent', () => {
  let component: SupervisorLedgerComponent;
  let fixture: ComponentFixture<SupervisorLedgerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorLedgerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorLedgerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
