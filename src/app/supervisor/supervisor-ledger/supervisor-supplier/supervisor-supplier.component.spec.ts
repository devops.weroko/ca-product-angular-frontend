import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorSupplierComponent } from './supervisor-supplier.component';

describe('SupervisorSupplierComponent', () => {
  let component: SupervisorSupplierComponent;
  let fixture: ComponentFixture<SupervisorSupplierComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorSupplierComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorSupplierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
