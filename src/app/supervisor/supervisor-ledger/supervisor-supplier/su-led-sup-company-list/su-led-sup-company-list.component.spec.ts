import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuLedSupCompanyListComponent } from './su-led-sup-company-list.component';

describe('SuLedSupCompanyListComponent', () => {
  let component: SuLedSupCompanyListComponent;
  let fixture: ComponentFixture<SuLedSupCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuLedSupCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuLedSupCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
