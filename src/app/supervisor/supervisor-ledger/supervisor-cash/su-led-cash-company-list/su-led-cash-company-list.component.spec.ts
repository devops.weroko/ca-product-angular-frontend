import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuLedCashCompanyListComponent } from './su-led-cash-company-list.component';

describe('SuLedCashCompanyListComponent', () => {
  let component: SuLedCashCompanyListComponent;
  let fixture: ComponentFixture<SuLedCashCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuLedCashCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuLedCashCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
