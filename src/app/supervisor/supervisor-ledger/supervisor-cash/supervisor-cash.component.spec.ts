import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorCashComponent } from './supervisor-cash.component';

describe('SupervisorCashComponent', () => {
  let component: SupervisorCashComponent;
  let fixture: ComponentFixture<SupervisorCashComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorCashComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorCashComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
