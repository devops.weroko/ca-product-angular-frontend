import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuLedExpCompanyListComponent } from './su-led-exp-company-list.component';

describe('SuLedExpCompanyListComponent', () => {
  let component: SuLedExpCompanyListComponent;
  let fixture: ComponentFixture<SuLedExpCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuLedExpCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuLedExpCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
