import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorExpenditureComponent } from './supervisor-expenditure.component';

describe('SupervisorExpenditureComponent', () => {
  let component: SupervisorExpenditureComponent;
  let fixture: ComponentFixture<SupervisorExpenditureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorExpenditureComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorExpenditureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
