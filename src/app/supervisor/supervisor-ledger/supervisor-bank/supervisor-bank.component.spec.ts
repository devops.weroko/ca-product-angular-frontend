import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorBankComponent } from './supervisor-bank.component';

describe('SupervisorBankComponent', () => {
  let component: SupervisorBankComponent;
  let fixture: ComponentFixture<SupervisorBankComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorBankComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorBankComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
