import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuLedBankCompanyListComponent } from './su-led-bank-company-list.component';

describe('SuLedBankCompanyListComponent', () => {
  let component: SuLedBankCompanyListComponent;
  let fixture: ComponentFixture<SuLedBankCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuLedBankCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuLedBankCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
