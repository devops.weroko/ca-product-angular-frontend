import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorCustomerAdditionalInfoComponent } from './supervisor-customer-additional-info.component';

describe('SupervisorCustomerAdditionalInfoComponent', () => {
  let component: SupervisorCustomerAdditionalInfoComponent;
  let fixture: ComponentFixture<SupervisorCustomerAdditionalInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorCustomerAdditionalInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorCustomerAdditionalInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
