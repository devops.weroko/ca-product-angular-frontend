import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorCustomerInfoComponent } from './supervisor-customer-info.component';

describe('SupervisorCustomerInfoComponent', () => {
  let component: SupervisorCustomerInfoComponent;
  let fixture: ComponentFixture<SupervisorCustomerInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorCustomerInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorCustomerInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
