import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorCustomerAddressComponent } from './supervisor-customer-address.component';

describe('SupervisorCustomerAddressComponent', () => {
  let component: SupervisorCustomerAddressComponent;
  let fixture: ComponentFixture<SupervisorCustomerAddressComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorCustomerAddressComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorCustomerAddressComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
