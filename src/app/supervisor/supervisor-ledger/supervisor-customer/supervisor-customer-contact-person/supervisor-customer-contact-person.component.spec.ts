import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorCustomerContactPersonComponent } from './supervisor-customer-contact-person.component';

describe('SupervisorCustomerContactPersonComponent', () => {
  let component: SupervisorCustomerContactPersonComponent;
  let fixture: ComponentFixture<SupervisorCustomerContactPersonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorCustomerContactPersonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorCustomerContactPersonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
