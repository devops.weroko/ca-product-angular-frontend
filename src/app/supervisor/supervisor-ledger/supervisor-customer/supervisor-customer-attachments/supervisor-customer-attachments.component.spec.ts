import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorCustomerAttachmentsComponent } from './supervisor-customer-attachments.component';

describe('SupervisorCustomerAttachmentsComponent', () => {
  let component: SupervisorCustomerAttachmentsComponent;
  let fixture: ComponentFixture<SupervisorCustomerAttachmentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorCustomerAttachmentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorCustomerAttachmentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
