import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorCustomerBankComponent } from './supervisor-customer-bank.component';

describe('SupervisorCustomerBankComponent', () => {
  let component: SupervisorCustomerBankComponent;
  let fixture: ComponentFixture<SupervisorCustomerBankComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorCustomerBankComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorCustomerBankComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
