import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuLedCustomerCompanyListComponent } from './su-led-customer-company-list.component';

describe('SuLedCustomerCompanyListComponent', () => {
  let component: SuLedCustomerCompanyListComponent;
  let fixture: ComponentFixture<SuLedCustomerCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuLedCustomerCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuLedCustomerCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
