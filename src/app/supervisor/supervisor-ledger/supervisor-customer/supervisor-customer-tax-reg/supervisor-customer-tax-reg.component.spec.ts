import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorCustomerTaxRegComponent } from './supervisor-customer-tax-reg.component';

describe('SupervisorCustomerTaxRegComponent', () => {
  let component: SupervisorCustomerTaxRegComponent;
  let fixture: ComponentFixture<SupervisorCustomerTaxRegComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorCustomerTaxRegComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorCustomerTaxRegComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
