import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorCustomerNotesComponent } from './supervisor-customer-notes.component';

describe('SupervisorCustomerNotesComponent', () => {
  let component: SupervisorCustomerNotesComponent;
  let fixture: ComponentFixture<SupervisorCustomerNotesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorCustomerNotesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorCustomerNotesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
