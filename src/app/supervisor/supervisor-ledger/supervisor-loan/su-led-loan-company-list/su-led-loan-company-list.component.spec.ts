import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuLedLoanCompanyListComponent } from './su-led-loan-company-list.component';

describe('SuLedLoanCompanyListComponent', () => {
  let component: SuLedLoanCompanyListComponent;
  let fixture: ComponentFixture<SuLedLoanCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuLedLoanCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuLedLoanCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
