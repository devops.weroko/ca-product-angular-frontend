import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorLoanComponent } from './supervisor-loan.component';

describe('SupervisorLoanComponent', () => {
  let component: SupervisorLoanComponent;
  let fixture: ComponentFixture<SupervisorLoanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorLoanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorLoanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
