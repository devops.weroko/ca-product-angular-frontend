import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuLedMuLedCompanyListComponent } from './su-led-mu-led-company-list.component';

describe('SuLedMuLedCompanyListComponent', () => {
  let component: SuLedMuLedCompanyListComponent;
  let fixture: ComponentFixture<SuLedMuLedCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuLedMuLedCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuLedMuLedCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
