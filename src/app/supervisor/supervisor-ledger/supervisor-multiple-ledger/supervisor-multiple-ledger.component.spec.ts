import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorMultipleLedgerComponent } from './supervisor-multiple-ledger.component';

describe('SupervisorMultipleLedgerComponent', () => {
  let component: SupervisorMultipleLedgerComponent;
  let fixture: ComponentFixture<SupervisorMultipleLedgerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorMultipleLedgerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorMultipleLedgerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
