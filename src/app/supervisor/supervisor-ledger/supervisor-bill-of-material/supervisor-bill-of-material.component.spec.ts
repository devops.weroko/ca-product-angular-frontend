import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorBillOfMaterialComponent } from './supervisor-bill-of-material.component';

describe('SupervisorBillOfMaterialComponent', () => {
  let component: SupervisorBillOfMaterialComponent;
  let fixture: ComponentFixture<SupervisorBillOfMaterialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorBillOfMaterialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorBillOfMaterialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
