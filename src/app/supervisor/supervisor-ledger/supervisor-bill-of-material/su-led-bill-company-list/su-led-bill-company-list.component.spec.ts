import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuLedBillCompanyListComponent } from './su-led-bill-company-list.component';

describe('SuLedBillCompanyListComponent', () => {
  let component: SuLedBillCompanyListComponent;
  let fixture: ComponentFixture<SuLedBillCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuLedBillCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuLedBillCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
