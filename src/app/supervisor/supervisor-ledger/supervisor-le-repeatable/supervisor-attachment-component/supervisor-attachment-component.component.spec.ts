import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorAttachmentComponentComponent } from './supervisor-attachment-component.component';

describe('SupervisorAttachmentComponentComponent', () => {
  let component: SupervisorAttachmentComponentComponent;
  let fixture: ComponentFixture<SupervisorAttachmentComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorAttachmentComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorAttachmentComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
