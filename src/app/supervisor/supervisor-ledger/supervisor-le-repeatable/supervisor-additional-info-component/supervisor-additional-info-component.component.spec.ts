import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorAdditionalInfoComponentComponent } from './supervisor-additional-info-component.component';

describe('SupervisorAdditionalInfoComponentComponent', () => {
  let component: SupervisorAdditionalInfoComponentComponent;
  let fixture: ComponentFixture<SupervisorAdditionalInfoComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorAdditionalInfoComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorAdditionalInfoComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
