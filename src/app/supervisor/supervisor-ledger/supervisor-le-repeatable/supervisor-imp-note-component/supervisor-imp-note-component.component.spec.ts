import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorImpNoteComponentComponent } from './supervisor-imp-note-component.component';

describe('SupervisorImpNoteComponentComponent', () => {
  let component: SupervisorImpNoteComponentComponent;
  let fixture: ComponentFixture<SupervisorImpNoteComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorImpNoteComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorImpNoteComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
