import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorAddressComponentComponent } from './supervisor-address-component.component';

describe('SupervisorAddressComponentComponent', () => {
  let component: SupervisorAddressComponentComponent;
  let fixture: ComponentFixture<SupervisorAddressComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorAddressComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorAddressComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
