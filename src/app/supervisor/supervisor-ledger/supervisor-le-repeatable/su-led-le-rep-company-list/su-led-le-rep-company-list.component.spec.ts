import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuLedLeRepCompanyListComponent } from './su-led-le-rep-company-list.component';

describe('SuLedLeRepCompanyListComponent', () => {
  let component: SuLedLeRepCompanyListComponent;
  let fixture: ComponentFixture<SuLedLeRepCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuLedLeRepCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuLedLeRepCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
