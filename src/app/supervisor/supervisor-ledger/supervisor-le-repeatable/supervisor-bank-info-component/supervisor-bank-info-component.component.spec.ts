import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorBankInfoComponentComponent } from './supervisor-bank-info-component.component';

describe('SupervisorBankInfoComponentComponent', () => {
  let component: SupervisorBankInfoComponentComponent;
  let fixture: ComponentFixture<SupervisorBankInfoComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorBankInfoComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorBankInfoComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
