import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorLeRepeatableComponent } from './supervisor-le-repeatable.component';

describe('SupervisorLeRepeatableComponent', () => {
  let component: SupervisorLeRepeatableComponent;
  let fixture: ComponentFixture<SupervisorLeRepeatableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorLeRepeatableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorLeRepeatableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
