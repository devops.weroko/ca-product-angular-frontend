import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorContactPersonComponentComponent } from './supervisor-contact-person-component.component';

describe('SupervisorContactPersonComponentComponent', () => {
  let component: SupervisorContactPersonComponentComponent;
  let fixture: ComponentFixture<SupervisorContactPersonComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorContactPersonComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorContactPersonComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
