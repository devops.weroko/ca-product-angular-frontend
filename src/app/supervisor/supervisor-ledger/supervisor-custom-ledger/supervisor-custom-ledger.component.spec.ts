import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorCustomLedgerComponent } from './supervisor-custom-ledger.component';

describe('SupervisorCustomLedgerComponent', () => {
  let component: SupervisorCustomLedgerComponent;
  let fixture: ComponentFixture<SupervisorCustomLedgerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorCustomLedgerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorCustomLedgerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
