import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuLedCustomCompanyListComponent } from './su-led-custom-company-list.component';

describe('SuLedCustomCompanyListComponent', () => {
  let component: SuLedCustomCompanyListComponent;
  let fixture: ComponentFixture<SuLedCustomCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuLedCustomCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuLedCustomCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
