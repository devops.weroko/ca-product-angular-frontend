import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuLedEmpCompanyListComponent } from './su-led-emp-company-list.component';

describe('SuLedEmpCompanyListComponent', () => {
  let component: SuLedEmpCompanyListComponent;
  let fixture: ComponentFixture<SuLedEmpCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuLedEmpCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuLedEmpCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
