import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorEmployeeComponent } from './supervisor-employee.component';

describe('SupervisorEmployeeComponent', () => {
  let component: SupervisorEmployeeComponent;
  let fixture: ComponentFixture<SupervisorEmployeeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorEmployeeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorEmployeeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
