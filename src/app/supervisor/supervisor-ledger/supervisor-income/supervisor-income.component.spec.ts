import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorIncomeComponent } from './supervisor-income.component';

describe('SupervisorIncomeComponent', () => {
  let component: SupervisorIncomeComponent;
  let fixture: ComponentFixture<SupervisorIncomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorIncomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorIncomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
