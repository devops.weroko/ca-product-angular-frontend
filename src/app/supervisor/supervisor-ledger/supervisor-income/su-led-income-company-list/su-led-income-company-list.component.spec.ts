import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuLedIncomeCompanyListComponent } from './su-led-income-company-list.component';

describe('SuLedIncomeCompanyListComponent', () => {
  let component: SuLedIncomeCompanyListComponent;
  let fixture: ComponentFixture<SuLedIncomeCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuLedIncomeCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuLedIncomeCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
