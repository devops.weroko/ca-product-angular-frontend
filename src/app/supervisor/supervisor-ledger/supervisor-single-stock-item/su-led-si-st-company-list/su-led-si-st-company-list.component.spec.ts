import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuLedSiStCompanyListComponent } from './su-led-si-st-company-list.component';

describe('SuLedSiStCompanyListComponent', () => {
  let component: SuLedSiStCompanyListComponent;
  let fixture: ComponentFixture<SuLedSiStCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuLedSiStCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuLedSiStCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
