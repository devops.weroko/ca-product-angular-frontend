import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorSingleStockItemComponent } from './supervisor-single-stock-item.component';

describe('SupervisorSingleStockItemComponent', () => {
  let component: SupervisorSingleStockItemComponent;
  let fixture: ComponentFixture<SupervisorSingleStockItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorSingleStockItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorSingleStockItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
