import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuLedLedReuCompanyListComponent } from './su-led-led-reu-company-list.component';

describe('SuLedLedReuCompanyListComponent', () => {
  let component: SuLedLedReuCompanyListComponent;
  let fixture: ComponentFixture<SuLedLedReuCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuLedLedReuCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuLedLedReuCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
