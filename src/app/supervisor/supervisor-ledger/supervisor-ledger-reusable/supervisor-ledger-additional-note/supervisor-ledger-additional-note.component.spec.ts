import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorLedgerAdditionalNoteComponent } from './supervisor-ledger-additional-note.component';

describe('SupervisorLedgerAdditionalNoteComponent', () => {
  let component: SupervisorLedgerAdditionalNoteComponent;
  let fixture: ComponentFixture<SupervisorLedgerAdditionalNoteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorLedgerAdditionalNoteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorLedgerAdditionalNoteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
