import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorLedgerReusableComponent } from './supervisor-ledger-reusable.component';

describe('SupervisorLedgerReusableComponent', () => {
  let component: SupervisorLedgerReusableComponent;
  let fixture: ComponentFixture<SupervisorLedgerReusableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorLedgerReusableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorLedgerReusableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
