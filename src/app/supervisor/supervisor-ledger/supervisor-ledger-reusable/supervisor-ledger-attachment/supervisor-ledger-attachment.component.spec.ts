import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorLedgerAttachmentComponent } from './supervisor-ledger-attachment.component';

describe('SupervisorLedgerAttachmentComponent', () => {
  let component: SupervisorLedgerAttachmentComponent;
  let fixture: ComponentFixture<SupervisorLedgerAttachmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorLedgerAttachmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorLedgerAttachmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
