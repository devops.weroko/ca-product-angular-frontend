import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorLedgerBankInfoComponent } from './supervisor-ledger-bank-info.component';

describe('SupervisorLedgerBankInfoComponent', () => {
  let component: SupervisorLedgerBankInfoComponent;
  let fixture: ComponentFixture<SupervisorLedgerBankInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorLedgerBankInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorLedgerBankInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
