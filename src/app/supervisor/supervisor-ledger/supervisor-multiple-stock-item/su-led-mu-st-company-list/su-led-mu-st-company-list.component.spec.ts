import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuLedMuStCompanyListComponent } from './su-led-mu-st-company-list.component';

describe('SuLedMuStCompanyListComponent', () => {
  let component: SuLedMuStCompanyListComponent;
  let fixture: ComponentFixture<SuLedMuStCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuLedMuStCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuLedMuStCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
