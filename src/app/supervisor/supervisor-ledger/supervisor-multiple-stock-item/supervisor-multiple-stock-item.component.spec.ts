import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorMultipleStockItemComponent } from './supervisor-multiple-stock-item.component';

describe('SupervisorMultipleStockItemComponent', () => {
  let component: SupervisorMultipleStockItemComponent;
  let fixture: ComponentFixture<SupervisorMultipleStockItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorMultipleStockItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorMultipleStockItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
