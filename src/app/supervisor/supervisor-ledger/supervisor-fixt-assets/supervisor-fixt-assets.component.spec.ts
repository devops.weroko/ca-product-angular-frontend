import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorFixtAssetsComponent } from './supervisor-fixt-assets.component';

describe('SupervisorFixtAssetsComponent', () => {
  let component: SupervisorFixtAssetsComponent;
  let fixture: ComponentFixture<SupervisorFixtAssetsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorFixtAssetsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorFixtAssetsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
