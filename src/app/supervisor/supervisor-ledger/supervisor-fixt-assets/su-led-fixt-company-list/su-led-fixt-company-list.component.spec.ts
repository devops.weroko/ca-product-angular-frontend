import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuLedFixtCompanyListComponent } from './su-led-fixt-company-list.component';

describe('SuLedFixtCompanyListComponent', () => {
  let component: SuLedFixtCompanyListComponent;
  let fixture: ComponentFixture<SuLedFixtCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuLedFixtCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuLedFixtCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
