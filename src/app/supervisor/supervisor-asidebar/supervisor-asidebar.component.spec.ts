import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorAsidebarComponent } from './supervisor-asidebar.component';

describe('SupervisorAsidebarComponent', () => {
  let component: SupervisorAsidebarComponent;
  let fixture: ComponentFixture<SupervisorAsidebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorAsidebarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorAsidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
