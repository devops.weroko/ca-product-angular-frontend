import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ClientRoutingModule } from './client-routing.module';
import { CreateCompanyComponent } from './components/create-company/create-company.component';
import { AuditorDetailsComponent } from './components/create-company/auditor-details/auditor-details.component';
import { ComSecuritySettingsComponent } from './components/create-company/com-security-settings/com-security-settings.component';
import { ComVoucherSettingsComponent } from './components/create-company/com-voucher-settings/com-voucher-settings.component';
import { CompanyAddressComponent } from './components/create-company/company-address/company-address.component';
import { CompanyInfoComponent } from './components/create-company/company-info/company-info.component';
import { CompanySettingsComponent } from './components/create-company/company-settings/company-settings.component';
import { HoldingDetailsComponent } from './components/create-company/holding-details/holding-details.component';
import { SignatoriesDetailsComponent } from './components/create-company/signatories-details/signatories-details.component';
import { ListCompanyComponent } from './components/list-company/list-company.component';
import { CompanyNameListingComponent } from './components/list-company/company-name-listing/company-name-listing.component';
import { CompanyShortInfoComponent } from './components/list-company/company-short-info/company-short-info.component';
import { TodoListComponent } from './components/list-company/todo-list/todo-list.component';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { ComWarehouseDetailsComponent } from './components/create-company/company-address/com-warehouse-details/com-warehouse-details.component';
import { ComBranchDetailsComponent } from './components/create-company/company-address/com-branch-details/com-branch-details.component';
import { VoucherSettingsInputComponent } from './components/create-company/com-voucher-settings/voucher-settings-input/voucher-settings-input.component';
import { VoucherSettingsViewComponent } from './components/create-company/com-voucher-settings/voucher-settings-view/voucher-settings-view.component';
import { ViewCompanyComponent } from './components/view-company/view-company.component';
import { ViewCompanySignatoriesComponent } from './components/view-company/view-company-signatories/view-company-signatories.component';
import { ViewCompanySettingsComponent } from './components/view-company/view-company-settings/view-company-settings.component';
import { ViewCompanySecurityComponent } from './components/view-company/view-company-security/view-company-security.component';
import { ViewCompanyInfoComponent } from './components/view-company/view-company-info/view-company-info.component';
import { ViewCompanyHoldingComponent } from './components/view-company/view-company-holding/view-company-holding.component';
import { ViewCompanyAuditorsComponent } from './components/view-company/view-company-auditors/view-company-auditors.component';
import { ViewCompanyAddressComponent } from './components/view-company/view-company-address/view-company-address.component';
import { DocumentsComponent } from './components/view-company/documents/documents.component';
import { ViewDocumentComponent } from './components/view-company/documents/view-document/view-document.component';
import { ViewBranchesComponent } from './components/view-company/view-company-address/view-branches/view-branches.component';
import { ViewWarehousesComponent } from './components/view-company/view-company-address/view-warehouses/view-warehouses.component';


@NgModule({
  // tslint:disable-next-line: max-line-length
  declarations: [
    CreateCompanyComponent,
    AuditorDetailsComponent,
    ComSecuritySettingsComponent,
    ComVoucherSettingsComponent,
    CompanyAddressComponent,
    CompanyInfoComponent,
    CompanySettingsComponent,
    HoldingDetailsComponent,
    SignatoriesDetailsComponent,
    ListCompanyComponent,
    CompanyNameListingComponent,
    CompanyShortInfoComponent,
    TodoListComponent,
    ComWarehouseDetailsComponent,
    ComBranchDetailsComponent,
    VoucherSettingsInputComponent,
    VoucherSettingsViewComponent,
    ViewCompanyComponent,
    ViewCompanySignatoriesComponent,
    ViewCompanySettingsComponent,
    ViewCompanySecurityComponent,
    ViewCompanyInfoComponent,
    ViewCompanyHoldingComponent,
    ViewCompanyAuditorsComponent,
    ViewCompanyAddressComponent,
    DocumentsComponent,
    ViewDocumentComponent,
    ViewBranchesComponent,
    ViewWarehousesComponent],
  imports: [
    CommonModule,
    ClientRoutingModule,
    FormsModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot()
  ],
  exports : [
    // tslint:disable-next-line: max-line-length
    CreateCompanyComponent, AuditorDetailsComponent, ComSecuritySettingsComponent, ComVoucherSettingsComponent, CompanyAddressComponent, CompanyInfoComponent, CompanySettingsComponent, HoldingDetailsComponent, SignatoriesDetailsComponent, ListCompanyComponent, CompanyNameListingComponent, CompanyShortInfoComponent, TodoListComponent
  ]
})
export class ClientModule { }
