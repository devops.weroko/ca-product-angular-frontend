import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewCompanySecurityComponent } from './view-company-security.component';

describe('ViewCompanySecurityComponent', () => {
  let component: ViewCompanySecurityComponent;
  let fixture: ComponentFixture<ViewCompanySecurityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewCompanySecurityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewCompanySecurityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
