import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewCompanyAuditorsComponent } from './view-company-auditors.component';

describe('ViewCompanyAuditorsComponent', () => {
  let component: ViewCompanyAuditorsComponent;
  let fixture: ComponentFixture<ViewCompanyAuditorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewCompanyAuditorsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewCompanyAuditorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
