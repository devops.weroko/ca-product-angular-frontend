import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewCompanyAddressComponent } from './view-company-address.component';

describe('ViewCompanyAddressComponent', () => {
  let component: ViewCompanyAddressComponent;
  let fixture: ComponentFixture<ViewCompanyAddressComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewCompanyAddressComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewCompanyAddressComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
