import { Component, OnInit } from '@angular/core';
import { CreateCompanyService } from 'src/app/services/client/create-company/create-company.service';
import { CompanyInfo } from 'src/app/shared/models/CompanyInfo';

@Component({
  selector: 'app-view-company',
  templateUrl: './view-company.component.html',
  styleUrls: ['./view-company.component.css']
})
export class ViewCompanyComponent implements OnInit {
  companyInfos:CompanyInfo[];

  constructor(private createCompanyService: CreateCompanyService) { }

  ngOnInit(): void {
    
    // this.createCompanyService.getCompanyInfo().subscribe(companyInfos => {
    //   this.companyInfos = companyInfos;
    //   console.log(this.companyInfos);
    // });
    
  }

}
