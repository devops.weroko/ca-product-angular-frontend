import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewCompanySignatoriesComponent } from './view-company-signatories.component';

describe('ViewCompanySignatoriesComponent', () => {
  let component: ViewCompanySignatoriesComponent;
  let fixture: ComponentFixture<ViewCompanySignatoriesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewCompanySignatoriesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewCompanySignatoriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
