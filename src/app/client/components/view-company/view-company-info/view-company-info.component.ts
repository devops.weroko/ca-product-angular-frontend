import { Component, OnInit, Input } from '@angular/core';
import { CompanyInfo } from 'src/app/shared/models/CompanyInfo';

@Component({
  selector: 'app-view-company-info',
  templateUrl: './view-company-info.component.html',
  styleUrls: ['./view-company-info.component.css']
})
export class ViewCompanyInfoComponent implements OnInit {

  @Input() companyInfo: CompanyInfo;
  constructor() { }

  ngOnInit(): void {
  }

}
