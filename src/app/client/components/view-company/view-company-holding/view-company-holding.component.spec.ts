import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewCompanyHoldingComponent } from './view-company-holding.component';

describe('ViewCompanyHoldingComponent', () => {
  let component: ViewCompanyHoldingComponent;
  let fixture: ComponentFixture<ViewCompanyHoldingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewCompanyHoldingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewCompanyHoldingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
