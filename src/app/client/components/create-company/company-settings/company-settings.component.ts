import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-company-settings',
  templateUrl: './company-settings.component.html',
  styleUrls: ['./company-settings.component.css']
})
export class CompanySettingsComponent implements OnInit {

  @Output() addCompanySettings: EventEmitter<any> = new EventEmitter();
  date_format:string;
	
	base_currency:string;
	
	currency_symbol:string;
	
  currency_decimal_place:string;
  
  gst: string;
  ptaxenroll: string;
  ptaxreg: string;
  epf: string;
  esi: string;
  tds: string;
  tcs: string;
  iec: string;
  msme: string;
  otherreg: string;

  constructor() { }

  ngOnInit(): void {
    this.date_format = "DD/MM/YYYY";
    this.gst = 'disable';
    this.ptaxenroll = 'disable';
    this.ptaxreg = 'disable';
    this.epf = 'disable';
    this.esi = 'disable';
    this.tds = 'disable';
    this.tcs = 'disable';
    this.iec = 'disable';
    this.msme = 'disable';
    this.otherreg = 'disable';

  }

  enableOrDisableOptions = [
	  {value : 'enable' , label : 'Enable'},
	  {value : 'disable' , label : 'Disable'}
  ];

  dateOptions = [
    { value :"DD/MM/YYYY", label: "DD/MM/YYYY" },
    { value : "MM/DD/YYYY", label: "MM/DD/YYYY" },
    { value :"YYYY/MM/DD", label:"YYYY/MM/DD" },
    { value :"YYYY/DD/MM", label : "YYYY/DD/MM" }
  ];

  onSubmit(){

    const companySettings = {
      date_format:this.date_format,
	
	base_currency:this.base_currency,
	
	currency_symbol:this.currency_symbol,
	
  currency_decimal_place:this.currency_decimal_place,
  
  gst: this.gst,
  ptaxenroll: this.ptaxenroll,
  ptaxreg: this.ptaxreg,
  epf: this.epf,
  esi: this.esi,
  tds: this.tds,
  tcs: this.tcs,
  iec: this.iec,
  msme: this.msme,
  otherreg: this.otherreg


    }

    this.addCompanySettings.emit(companySettings);
  }


}
