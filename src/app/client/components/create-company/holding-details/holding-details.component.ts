import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { HoldingDetails } from 'src/app/shared/models/HoldingDetails';
import { AssociateDetails } from 'src/app/shared/models/AssociateDetails';
import { SubsidiaryDetails } from 'src/app/shared/models/SubsidiaryDetails';
import { PartnerDetails } from 'src/app/shared/models/PartnerDetails';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-holding-details',
  templateUrl: './holding-details.component.html',
  styleUrls: ['./holding-details.component.css']
})
export class HoldingDetailsComponent implements OnInit {

  HoldingArray : Array<HoldingDetails> = [];
  AssociateArray : Array<AssociateDetails> = [];
  SubsidiaryArray : Array<SubsidiaryDetails> = [];
  PartnerArray : Array<PartnerDetails> = [];

  @Output() addHoldingDetails: EventEmitter<any> = new EventEmitter();
  @Output() addAssociateDetails: EventEmitter<any> = new EventEmitter();
  @Output() addSubsidiaryDetails: EventEmitter<any> = new EventEmitter();
  @Output() addPartnerDetails: EventEmitter<any> = new EventEmitter();

  newHoldingRow: any = {};
  newAssociateRow: any = {};
  newSubsidiaryRow: any = {};
  newPartnerRow: any = {};

  i:number;
  holding: string;
  subsidiary: string;
  associate: string;
  partner: string;
  message : string = "Cannot delete last row!"

  yesOrNoOptions = [
	  {value : 'yes' , label : 'Yes'},
	  {value : 'no' , label : 'No'}
  ];

  constructor(public toasterService : ToastrService) { }

  ngOnInit(): void {

    this.holding = 'no';
    this.associate = 'no';
    this.subsidiary = 'no';
    this.partner = 'no';

    this.newHoldingRow = { 

      holding_sl_no:"",
      holders_name:"",
      holders_cin:"",
      no_share_holders:"",
      ratio_of_share_holders:"",
      date_of_holders:"",
      holding_attachment:"",
      
    
    }

    this.newAssociateRow = { 

      
      associate_sl_no:"",
      name_of_the_associate_company:"",
      cin_of_associate_company:"",
      no_of_shares_associate:"",
      persent_of_shares_associate:"",
      date_of_associate:"",
      associate_attachment:"",
      
    }

    
    this.newSubsidiaryRow = { 

     
      subsidiary_sl_no:"",
      subsidiary_name:"",
      subsidiary_cin:"",
      no_share_subsidiary:"",
      ratio_of_share_subsidiary:"",
      date_of_subsidiary:"",
      subsidiary_attachment:"",
       
    }

    this.newPartnerRow = { 

     
      partner_sl_no:"",
      name_of_the_partner_company:"",
      registration_number_of_partner_ompany:"",
      no_of_shares_holder:"",
      persent_of_shares_hold:"",
      date_of_partnership:"",
      pertner_attachment:""
       
    }

    this.HoldingArray.push(this.newHoldingRow);
    this.SubsidiaryArray.push(this.newAssociateRow);
    this.AssociateArray.push(this.newSubsidiaryRow);
    this.PartnerArray.push(this.newPartnerRow);
  }

  addHoldingRow(index){

    this.newHoldingRow = { 

      
      holding_sl_no:"",
      holders_name:"",
      holders_cin:"",
      no_share_holders:"",
      ratio_of_share_holders:"",
      date_of_holders:"",
      holding_attachment:"",
      

    }

    
 
    this.HoldingArray.push(this.newHoldingRow);
    console.log("Added");
    console.log(this.HoldingArray);
    return true;

  
  }

  addAssociateRow(index){

 
    
    this.newAssociateRow = { 

      
      associate_sl_no:"",
      name_of_the_associate_company:"",
      cin_of_associate_company:"",
      no_of_shares_associate:"",
      persent_of_shares_associate:"",
      date_of_associate:"",
      associate_attachment:"",
      
    }



    this.AssociateArray.push(this.newAssociateRow);
    console.log("Added");
    console.log(this.AssociateArray);
    return true;


  }

  addSubsidiaryRow(index){
 
    this.newSubsidiaryRow = { 

     
      subsidiary_sl_no:"",
      subsidiary_name:"",
      subsidiary_cin:"",
      no_share_subsidiary:"",
      ratio_of_share_subsidiary:"",
      date_of_subsidiary:"",
      subsidiary_attachment:"",
       
    }


    this.SubsidiaryArray.push(this.newSubsidiaryRow);
    console.log("Added");
    console.log(this.SubsidiaryArray);
    return true;



  }

  addPartnerRow(index){


    this.newPartnerRow = { 

     
      partner_sl_no:"",
      name_of_the_partner_company:"",
      registration_number_of_partner_ompany:"",
      no_of_shares_holder:"",
      persent_of_shares_hold:"",
      date_of_partnership:"",
      pertner_attachment:""
       
    }


    this.PartnerArray.push(this.newPartnerRow);
    console.log("Added");
    console.log(this.PartnerArray);
    return true;


  }



  deleteHoldingRow(index){
    if(this.HoldingArray.length==1){
      console.log("Can't Delete");
      this.toasterService.error(this.message, "Error", { positionClass: 'toast-bottom-center' });
      return false;
    }
    else{
      this.HoldingArray.splice(index,1);
      console.log("Deleted successfully");
      return true;
    }

  }

  deleteAssociateRow(index){
 
    if(this.AssociateArray.length==1){
      console.log("Can't Delete");
      this.toasterService.error(this.message, "Error", { positionClass: 'toast-bottom-center' });
      return false;
    }
    else{
      this.AssociateArray.splice(index,1);
      console.log("Deleted successfully");
      return true;
    }


  }
  
  deleteSubsidiaryRow(index){
 

    if(this.SubsidiaryArray.length==1){
      console.log("Can't Delete");
      this.toasterService.error(this.message, "Error", { positionClass: 'toast-bottom-center' });
      return false;
    }
    else{
      this.SubsidiaryArray.splice(index,1);
      console.log("Deleted successfully");
      return true;
    }

  }

  deletePartnerRow(index){

    if(this.PartnerArray.length==1){
      this.toasterService.error(this.message, "Error", { positionClass: 'toast-bottom-center' });
      console.log("Can't Delete");
      return false;
    }
    else{
      this.PartnerArray.splice(index,1);
      console.log("Deleted successfully");
      return true;
    }
  }


  onSubmitHolding(){

    const HoldingArray=this.HoldingArray

    console.log(this.HoldingArray);

    

    this.addHoldingDetails.emit(HoldingArray);
  }

  onSubmitAssociate(){

    const AssociateArray=this.AssociateArray

    console.log(this.AssociateArray);

    

    this.addAssociateDetails.emit(AssociateArray);
  }

  onSubmitSubsidiary(){

    const SubsidiaryArray=this.SubsidiaryArray

    console.log(this.SubsidiaryArray);

    

    this.addSubsidiaryDetails.emit(SubsidiaryArray);
  }

  onSubmitPartner(){

    const PartnerArray=this.PartnerArray

    console.log(this.PartnerArray);

    

    this.addPartnerDetails.emit(PartnerArray);
  }


  


}
