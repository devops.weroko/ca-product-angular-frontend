import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { SignatoriesDetails } from 'src/app/shared/models/SignatoriesDetails';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-signatories-details',
  templateUrl: './signatories-details.component.html',
  styleUrls: ['./signatories-details.component.css']
})
export class SignatoriesDetailsComponent implements OnInit {

  signatoriesArray : Array<SignatoriesDetails> = [];
  newRow: any = {};

  @Output() addSignatoriesDetails: EventEmitter<any> = new EventEmitter();

  i:number;
  constructor(private toasterService : ToastrService) { }

  ngOnInit(): void {
    this.newRow = { 
      name:"",
      father_name:"",
      address:"",
      qualification:"",
      designation:"",
      date_of_birth:"",
      date_of_appointment:"",
      date_of_cessation:"",
      din:"",
      aadhar:"",
      pan:"",
      passport:"",
      aadhar_attachment:"",
      pan_attachment:"",
      passport_attachment:"",
      image_attachment:"",
      authorized_signatories:"",
      share_ratio:""
    }

    this.signatoriesArray.push(this.newRow);
  }

  addRow(index){

    this.newRow = { 
      name:"",
      father_name:"",
      address:"",
      qualification:"",
      designation:"",
      date_of_birth:"",
      date_of_appointment:"",
      date_of_cessation:"",
      din:"",
      aadhar:"",
      pan:"",
      passport:"",
      aadhar_attachment:"",
      pan_attachment:"",
      passport_attachment:"",
      image_attachment:"",
      authorized_signatories:"",
      share_ratio:""
    }

    this.signatoriesArray.push(this.newRow);
    console.log("Added="+index);
    console.log(this.signatoriesArray);
    return true;

    


  }

  deleteRow(index){
    if(this.signatoriesArray.length==1){
      
      this.toasterService.error("Cannot delete last row!", "Error", { positionClass: 'toast-bottom-center' });
      return false;
    }
    else{
      this.signatoriesArray.splice(index,1);
      console.log("Deleted successfully="+index);
      return true;
    }
  }

  onSubmit(){

    const signatoriesArray=this.signatoriesArray

    console.log(this.signatoriesArray);

    

    this.addSignatoriesDetails.emit(signatoriesArray);
  }

}
