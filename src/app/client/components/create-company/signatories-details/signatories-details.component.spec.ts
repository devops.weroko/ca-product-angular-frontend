import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SignatoriesDetailsComponent } from './signatories-details.component';

describe('SignatoriesDetailsComponent', () => {
  let component: SignatoriesDetailsComponent;
  let fixture: ComponentFixture<SignatoriesDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SignatoriesDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SignatoriesDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
