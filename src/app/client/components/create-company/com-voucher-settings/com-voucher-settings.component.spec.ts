import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComVoucherSettingsComponent } from './com-voucher-settings.component';

describe('ComVoucherSettingsComponent', () => {
  let component: ComVoucherSettingsComponent;
  let fixture: ComponentFixture<ComVoucherSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComVoucherSettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComVoucherSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
