import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VoucherSettingsInputComponent } from './voucher-settings-input.component';

describe('VoucherSettingsInputComponent', () => {
  let component: VoucherSettingsInputComponent;
  let fixture: ComponentFixture<VoucherSettingsInputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VoucherSettingsInputComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VoucherSettingsInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
