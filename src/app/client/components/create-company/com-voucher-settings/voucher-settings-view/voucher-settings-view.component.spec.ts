import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VoucherSettingsViewComponent } from './voucher-settings-view.component';

describe('VoucherSettingsViewComponent', () => {
  let component: VoucherSettingsViewComponent;
  let fixture: ComponentFixture<VoucherSettingsViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VoucherSettingsViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VoucherSettingsViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
