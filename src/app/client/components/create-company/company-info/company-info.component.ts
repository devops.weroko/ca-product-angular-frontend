import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-company-info',
  templateUrl: './company-info.component.html',
  styleUrls: ['./company-info.component.css']
})
export class CompanyInfoComponent implements OnInit {

@Input() saved : boolean;

	

  @Output() addCompanyInfo: EventEmitter<any> = new EventEmitter();
  
  name:string;
  trade_name:string;	
	pan:string;
  pan_effective_date:string;
	
	pan_dep_address:string;
	
	range_ward:string;
	
	pan_ph_no:string;
	
	pan_attachment:string;
	
	org_type:string; 
	
	business_type:string;
	
	company_crn_no:string;

	crn_eff_date:string;
	
	crn_address:string;
	
	crn_range_ward:string;
	
	crn_ph_no:string;

	crn_attachment:string;
	
	
	company_llpin_no:string;
	
	llpin_eff_date:string;
	
	llpin_address:string;
	
	llpin_range_ward:string;

	llpin_ph_no:string;

	llpin_attachment:string;
	
	
	company_cin_no:string;
	
	cin_eff_date:string;
	
	cin_address:string;
	
	cin_range_ward:string;
	
	cin_ph_no:string;
	
	cin_attachment:string;
	
	company_logo:string;
	
	phone:string;
	
	mobile:string;
	
	email:string; 
	
	website:string;
	
	date_of_incorporation:string;
	
	financial_year_start:string;
	
	book_commencing:string;
	
	commencement_date:string;



  onSubmit() {
    const companyInfo = {
      name: this.name,
      trade_name: this.trade_name,	
	    pan: this.pan,
      pan_effective_date: this.pan_effective_date,
      pan_dep_address:this.pan_dep_address,
	
      range_ward: this.range_ward,

      pan_ph_no: this.pan_ph_no,
	
	pan_attachment:this.pan_attachment,
	
	org_type:this.org_type,
	
	business_type:this.business_type,
	
	company_crn_no:this.company_crn_no,

	crn_eff_date:this.crn_eff_date,
	
	crn_address:this.crn_address,
	
	crn_range_ward:this.crn_range_ward,
	
	crn_ph_no:this.crn_ph_no,

	crn_attachment:this.crn_attachment,
	
	
	company_llpin_no:this.company_llpin_no,
	
	llpin_eff_date:this.llpin_eff_date,
	
	llpin_address:this.llpin_address,
	
	llpin_range_ward:this.llpin_range_ward,

	llpin_ph_no:this.llpin_ph_no,

	llpin_attachment:this.llpin_attachment,
	
	
	company_cin_no:this.company_cin_no,
	
	cin_eff_date:this.cin_eff_date,
	
	cin_address:this.cin_address,
	
	cin_range_ward:this.cin_range_ward,
	
	cin_ph_no:this.cin_ph_no,
	
	cin_attachment:this.cin_attachment,
	
  company_logo:this.company_logo,
	
	phone:this.phone,
	
	mobile:this.mobile,
	
	email:this.email,
	
	website:this.website,
	
	date_of_incorporation:this.date_of_incorporation,
	
	financial_year_start:this.financial_year_start,
	
	book_commencing:this.book_commencing,
	
	commencement_date:this.commencement_date
    }

  
    this.addCompanyInfo.emit(companyInfo);

  }


  public imagePath;
  imgURL: any;
  public message: string;


  constructor(public toasterService : ToastrService) { }

  ngOnInit(): void {

	this.org_type = '',
	this.business_type = '',
	this.name = "",
	this.trade_name = "",
	this.pan = "",
	this.pan_effective_date = "",
	
	this.pan_dep_address = "",
	
	this.range_ward = "",
	
	this.pan_ph_no = "",
	
	this.pan_attachment = "",
	
	this.org_type = "",
	
	this.business_type = "",
	
	this.company_crn_no = "",

	this.crn_eff_date = "",
	
	this.crn_address = "",
	
	this.crn_range_ward = "",
	
	this.crn_ph_no = "",

	this.crn_attachment = "",
	
	
	this.company_llpin_no = "",
	
	this.llpin_eff_date = "",
	
	this.llpin_address = "",
	
	this.llpin_range_ward = "",

	this.llpin_ph_no = "",

	this.llpin_attachment = "",
	
	
	this.company_cin_no = "",
	
	this.cin_eff_date = "",
	
	this.cin_address = "",
	
	this.cin_range_ward = "",
	
	this.cin_ph_no = "",
	
	this.cin_attachment = "",
	
	this.company_logo  = "",
	
	this.phone = "",
	
	this.mobile = "",
	
	this.email = "", 
	
	this.website = "",
	
	this.date_of_incorporation = "",
	
	this.financial_year_start = "",
	
	this.book_commencing= "",
	
	this.commencement_date = "",
	this.imgURL = ""
	
  }

  
  organisationOptions = [
    { value: '', label: 'Please select' },
    { value: 'Partnership', label: 'Partnership' },
	{ value: 'LLP', label: 'LLP' },
	{ value: 'Company', label: 'Company' },
	{ value: 'Proprietor', label: 'Proprietor' },
	{ value: 'Society', label: 'Society' },
	{ value: 'Trust', label: 'Trust' },
	{ value: 'HUF', label: 'HUF' },
	{ value: 'Artificial Juridicial Person', label: 'Artificial Juridicial Person' },
	{ value: 'Other', label: 'Other' }
  ];

  businessOptions = [
    { value: '', label: 'Please select' },
    { value: 'Partnership', label: 'Manufacturing' },
	{ value: 'Trading', label: 'Trading' },
	{ value: 'Services', label: 'Services' },
	{ value: 'Investment', label: 'Investment' },
	{ value: 'Other', label: 'Other' }
	
  ];

  editCompanyInfo(){
	  this.saved = false;
  }

  preview(files) {
    if (files.length === 0)
      return;
 
    var mimeType = files[0].type;
    if (mimeType.match(/image\/*/) == null) {
	  this.message = "Only images are supported";
	  this.toasterService.error(this.message, "Error", { positionClass: 'toast-bottom-center' });
      return;
    }
 
    var reader = new FileReader();
    this.imagePath = files;
    reader.readAsDataURL(files[0]); 
    reader.onload = (_event) => { 
      this.imgURL = reader.result; 
    }
  }


}
