import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuditorDetailsComponent } from './auditor-details.component';

describe('AuditorDetailsComponent', () => {
  let component: AuditorDetailsComponent;
  let fixture: ComponentFixture<AuditorDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuditorDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuditorDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
