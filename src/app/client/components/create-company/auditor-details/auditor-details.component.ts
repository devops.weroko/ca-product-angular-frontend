import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-auditor-details',
  templateUrl: './auditor-details.component.html',
  styleUrls: ['./auditor-details.component.css']
})
export class AuditorDetailsComponent implements OnInit {

  @Output() addAuditorDetails: EventEmitter<any> = new EventEmitter();
  //Tax Auditor

  tax_name_of_auditor:string;
  tax_auditors_pan:string;
  tax_membership_no:string;
  tax_address:string;
  tax_country:string;
  tax_state:string;
  tax_zip_code:string;
  tax_firm_name:string;
  tax_firm_pan:string;
  tax_frn:string;
  tax_phone:string;
  tax_email:string;
  tax_period_from:string;
  tax_to:string;

  // GST auditor

  gst_name_of_auditor:string;
  gst_auditors_pan:string;
  gst_membership_no:string;
  gst_address:string;
  gst_country:string;
  gst_state:string;
  gst_zip_code:string;
  gst_firm_name:string;
  gst_firm_pan:string;
  gst_frn:string;
  gst_phone:string;
  gst_email:string;
  gst_period_from:string;
  gst_to:string;

  //Satutory Auditor Details

  satutory_name_of_auditor:string;
  satutory_auditors_pan:string;
  satutory_membership_no:string;
  satutory_address:string;
  satutory_country:string;
  satutory_state:string;
  satutory_zip_code:string;
  satutory_firm_name:string;
  satutory_firm_pan:string;
  satutory_frn:string;
  satutory_phone:string;
  satutory_email:string;
  satutory_period_from:string;
  satutory_to:string;

  constructor() { }

  ngOnInit(): void {
  }

  onSubmit(){
    const auditorDetails = {
      //Tax Auditor

  tax_name_of_auditor:this.tax_name_of_auditor,
  tax_auditors_pan:this.tax_auditors_pan,
  tax_membership_no:this.tax_membership_no,
  tax_address:this.tax_address,
  tax_country:this.tax_country,
  tax_state:this.tax_state,
  tax_zip_code:this.tax_zip_code,
  tax_firm_name:this.tax_firm_name,
  tax_firm_pan:this.tax_firm_pan,
  tax_frn:this.tax_frn,
  tax_phone:this.tax_phone,
  tax_email:this.tax_email,
  tax_period_from:this.tax_period_from,
  tax_to:this.tax_to,

  // GST auditor

  gst_name_of_auditor:this.gst_name_of_auditor,
  gst_auditors_pan:this.gst_auditors_pan,
  gst_membership_no:this.gst_membership_no,
  gst_address:this.gst_address,
  gst_country:this.gst_country,
  gst_state:this.gst_state,
  gst_zip_code:this.gst_zip_code,
  gst_firm_name:this.gst_firm_name,
  gst_firm_pan:this.gst_firm_pan,
  gst_frn:this.gst_frn,
  gst_phone:this.gst_phone,
  gst_email:this.gst_email,
  gst_period_from:this.gst_period_from,
  gst_to:this.gst_to,

  //Satutory Auditor Details

  satutory_name_of_auditor:this.satutory_name_of_auditor,
  satutory_auditors_pan:this.satutory_auditors_pan,
  satutory_membership_no:this.satutory_membership_no,
  satutory_address:this.satutory_address,
  satutory_country:this.satutory_country,
  satutory_state:this.satutory_state,
  satutory_zip_code:this.satutory_zip_code,
  satutory_firm_name:this.satutory_firm_name,
  satutory_firm_pan:this.satutory_firm_pan,
  satutory_frn:this.satutory_frn,
  satutory_phone:this.satutory_phone,
  satutory_email:this.satutory_email,
  satutory_period_from:this.satutory_period_from,
  satutory_to:this.satutory_to


    }

    this.addAuditorDetails.emit(auditorDetails);


  }

}
