import { Component, OnInit } from '@angular/core';
import { CompanyInfo } from 'src/app/shared/models/CompanyInfo';
import { CompanyAddress } from 'src/app/shared/models/CompanyAddress';
import { CompanySettings } from 'src/app/shared/models/CompanySettings';
import { SignatoriesDetails } from 'src/app/shared/models/SignatoriesDetails';
import { AuditorDetails } from 'src/app/shared/models/AuditorDetails';
import { HoldingDetails } from 'src/app/shared/models/HoldingDetails';
import { AssociateDetails } from 'src/app/shared/models/AssociateDetails';
import { SubsidiaryDetails } from 'src/app/shared/models/SubsidiaryDetails';
import { PartnerDetails } from 'src/app/shared/models/PartnerDetails';
import { CreateCompanyService } from 'src/app/services/client/create-company/create-company.service';

@Component({
  selector: 'app-create-company',
  templateUrl: './create-company.component.html',
  styleUrls: ['./create-company.component.css']
})
export class CreateCompanyComponent implements OnInit {
  companyInfo:CompanyInfo;
  companyAddress:CompanyAddress;
  companySettings:CompanySettings;
  signatoriesDetails:SignatoriesDetails;
  auditorDetails:AuditorDetails[];
  holdingDetails:HoldingDetails[];
  associateDetails:AssociateDetails[];
  subsidiaryDetails:SubsidiaryDetails[];
  partnerDetails:PartnerDetails[];
 

  constructor(private createCompanyService: CreateCompanyService) { }

  isDisabled = true;
  saved : boolean = false;

  ngOnInit(): void {
  }

  addCompanyInfo(companyInfo: CompanyInfo){


    this.createCompanyService.addCompanyInfo(companyInfo).subscribe(
      companyInfo => {
        this.companyInfo = companyInfo;
        //this.companyAddress.company_id = this.companyInfo.company_id;
        this.saved = true;
      this.isDisabled = false;
      console.log(companyInfo.company_id);
      
      

    })
  }

  

  addCompanyAddress(companyAddress: CompanyAddress){
   
    companyAddress.company_id = this.companyInfo.company_id;
    console.log(companyAddress);
    this.createCompanyService.addCompanyAddress(companyAddress).subscribe( companyAddress => {
      
      this.companyAddress = companyAddress;

    })
  }

  addCompanySettings(companySettings: CompanySettings){
    companySettings.company_id = this.companyInfo.company_id;
    this.createCompanyService.addCompanySettings(companySettings).subscribe( 
      companySettings => {
        this.companySettings = companySettings;
      
    })
  }

  addSignatoriesDetails(signatoriesDetails: SignatoriesDetails[]){
    console.log(signatoriesDetails.length);
    console.log(signatoriesDetails);
     for(let i = 0; i < signatoriesDetails.length ; i++ )
     {
        signatoriesDetails[i].company_id = this.companyInfo.company_id;
     }
    // this.createCompanyService.addSignatoriesDetails(signatoriesDetails).subscribe( signatoriesDetails => {
      
    //   // this.signatoriesDetails = signatoriesDetails;
    // })
  }

  addAuditorDetails(auditorDetails: AuditorDetails){

    this.createCompanyService.addAuditorDetails(auditorDetails).subscribe( auditorDetails => {
      this.auditorDetails.push(auditorDetails);
    })
  }

  addHoldingDetails(holdingDetails: HoldingDetails){

    this.createCompanyService.addHoldingDetails(holdingDetails).subscribe( holdingDetails => {
      this.holdingDetails.push(holdingDetails);
    })
  }

  addAssociateDetails(associateDetails: AssociateDetails){

    this.createCompanyService.addAssociateDetails(associateDetails).subscribe( associateDetails => {
      this.associateDetails.push(associateDetails);
    })
  }
  
  addSubsidiaryDetails(subsidiaryDetails: SubsidiaryDetails){

    this.createCompanyService.addSubsidiaryDetails(subsidiaryDetails).subscribe( subsidiaryDetails => {
      this.subsidiaryDetails.push(subsidiaryDetails);
    })
  }
  addPartnerDetails(partnerDetails: PartnerDetails){

    this.createCompanyService.addPartnerDetails(partnerDetails).subscribe( partnerDetails => {
      this.partnerDetails.push(partnerDetails);
    })
  }


}
