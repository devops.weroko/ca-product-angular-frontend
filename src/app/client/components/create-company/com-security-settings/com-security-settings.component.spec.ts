import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComSecuritySettingsComponent } from './com-security-settings.component';

describe('ComSecuritySettingsComponent', () => {
  let component: ComSecuritySettingsComponent;
  let fixture: ComponentFixture<ComSecuritySettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComSecuritySettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComSecuritySettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
