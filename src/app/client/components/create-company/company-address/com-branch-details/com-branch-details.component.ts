import { Component, OnInit, Input } from '@angular/core';
import { CompanyAddressBranchDetails } from 'src/app/shared/models/CompanyAddressBranchDetails';

@Component({
  selector: 'app-com-branch-details',
  templateUrl: './com-branch-details.component.html',
  styleUrls: ['./com-branch-details.component.css']
})
export class ComBranchDetailsComponent implements OnInit {

  @Input() no_of_branches : number;

  


  branchDetailsArray : Array<CompanyAddressBranchDetails> = [];
  newBlock: any = {};

  

  country: string;
  state: string;

  countryitr : string;

  val : boolean = true;

  lastChange : number;
  
  

  constructor() { 
    
    
  
  }

  ngOnInit() : void {

  }

 

  ngOnChanges(): void {

    this.branchDetailsArray = [];
    this.newBlock = {
      branch_name : "",
      street : "",
      area : "",
      city_town : "",
      district : "",
      police_station : "",
      country : "",
      state : "",
      pin_code : "",
      attachment : "",
      trade_license : "no",
      ptax_enrollment : "no",
      reg_gst :"no",
      tan : "no",
      reg_esi : "no",
      other_reg : "no",
      no_of_registrations : 1,
      warehouse : "no",
      no_of_warehouses : 1
    }
    for (let i = 0; i < this.no_of_branches; i++) {
      this.branchDetailsArray.push(this.newBlock);
      
    }
   
    console.log(this.branchDetailsArray);
  
  }

  yesOrNoOptions = [
	  {value : 'yes' , label : 'Yes'},
	  {value : 'no' , label : 'No'}
  ];

}
