import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComBranchDetailsComponent } from './com-branch-details.component';

describe('ComBranchDetailsComponent', () => {
  let component: ComBranchDetailsComponent;
  let fixture: ComponentFixture<ComBranchDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComBranchDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComBranchDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
