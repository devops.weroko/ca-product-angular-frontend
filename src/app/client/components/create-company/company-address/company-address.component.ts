import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ICountry } from 'country-state-city';

@Component({
  selector: 'app-company-address',
  templateUrl: './company-address.component.html',
  styleUrls: ['./company-address.component.css']
})
export class CompanyAddressComponent implements OnInit {

  @Output() addCompanyAddress: EventEmitter<any> = new EventEmitter();

 

  street: string;
  area: string;

  city_town: string;

  district: string;

  police_station: string;

  state: string;

  country: string;

  zip_code: string;

  no_of_warehouses: number = 0;

  no_of_branches: number = 0;


  list = Array;

  branch: string;
  warehouse: string;
  
  

  nows = Array;

  yesOrNoOptions = [
	  {value : 'yes' , label : 'Yes'},
	  {value : 'no' , label : 'No'}
  ];
  
  constructor( ) {
   }

    

   

  ngOnInit(): void {

    this.branch = 'no',
    this.warehouse = 'no'

    
     
  }



  onSubmit() {
    console.log("hi submitted companyaddress")
    const companyAddress = {

      street: this.street,
      area: this.area,

      city_town: this.city_town,

      district: this.district,

      police_station: this.police_station,

      state: this.state,

      country: this.country,

      zip_code: this.zip_code,

      no_of_warehouses: this.no_of_warehouses,

      no_of_branches: this.no_of_branches


    }
    this.addCompanyAddress.emit(companyAddress);


  }

}
