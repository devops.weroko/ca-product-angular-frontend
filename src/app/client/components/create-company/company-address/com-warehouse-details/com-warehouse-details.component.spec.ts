import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComWarehouseDetailsComponent } from './com-warehouse-details.component';

describe('ComWarehouseDetailsComponent', () => {
  let component: ComWarehouseDetailsComponent;
  let fixture: ComponentFixture<ComWarehouseDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComWarehouseDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComWarehouseDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
