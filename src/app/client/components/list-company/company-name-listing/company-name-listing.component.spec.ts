import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyNameListingComponent } from './company-name-listing.component';

describe('CompanyNameListingComponent', () => {
  let component: CompanyNameListingComponent;
  let fixture: ComponentFixture<CompanyNameListingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompanyNameListingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyNameListingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
