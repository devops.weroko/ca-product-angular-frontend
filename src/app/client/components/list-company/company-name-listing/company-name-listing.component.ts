import { Component, OnInit, Input } from '@angular/core';
import { CompanyInfo } from 'src/app/shared/models/CompanyInfo';

@Component({
  selector: 'app-company-name-listing',
  templateUrl: './company-name-listing.component.html',
  styleUrls: ['./company-name-listing.component.css']
})
export class CompanyNameListingComponent implements OnInit {

  @Input() companyInfo: CompanyInfo;
  constructor() { }

  ngOnInit(): void {
  }

}
