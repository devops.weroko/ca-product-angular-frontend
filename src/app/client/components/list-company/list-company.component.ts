import { Component, OnInit } from '@angular/core';
import { CompanyInfo } from 'src/app/shared/models/CompanyInfo';
import { ViewListCompanyService } from 'src/app/services/client/view-list-company/view-list-company.service';

@Component({
  selector: 'app-list-company',
  templateUrl: './list-company.component.html',
  styleUrls: ['./list-company.component.css']
})
export class ListCompanyComponent implements OnInit {

  companyInfos:CompanyInfo[];

  

  constructor(private viewListCompanyService: ViewListCompanyService) { }

  ngOnInit(): void {

    this.viewListCompanyService.getCompanyList().subscribe(companyInfos => {
      this.companyInfos = companyInfos;
      console.log(this.companyInfos);
    });
  }
}
