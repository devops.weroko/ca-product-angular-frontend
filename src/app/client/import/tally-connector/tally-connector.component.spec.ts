import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TallyConnectorComponent } from './tally-connector.component';

describe('TallyConnectorComponent', () => {
  let component: TallyConnectorComponent;
  let fixture: ComponentFixture<TallyConnectorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TallyConnectorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TallyConnectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
