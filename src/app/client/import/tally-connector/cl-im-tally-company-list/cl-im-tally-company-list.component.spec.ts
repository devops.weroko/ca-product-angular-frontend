import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClImTallyCompanyListComponent } from './cl-im-tally-company-list.component';

describe('ClImTallyCompanyListComponent', () => {
  let component: ClImTallyCompanyListComponent;
  let fixture: ComponentFixture<ClImTallyCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClImTallyCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClImTallyCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
