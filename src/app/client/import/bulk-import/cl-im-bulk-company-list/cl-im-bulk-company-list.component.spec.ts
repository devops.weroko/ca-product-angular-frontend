import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClImBulkCompanyListComponent } from './cl-im-bulk-company-list.component';

describe('ClImBulkCompanyListComponent', () => {
  let component: ClImBulkCompanyListComponent;
  let fixture: ComponentFixture<ClImBulkCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClImBulkCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClImBulkCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
