import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClReSuCoCompanyListComponent } from './cl-re-su-co-company-list.component';

describe('ClReSuCoCompanyListComponent', () => {
  let component: ClReSuCoCompanyListComponent;
  let fixture: ComponentFixture<ClReSuCoCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClReSuCoCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClReSuCoCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
