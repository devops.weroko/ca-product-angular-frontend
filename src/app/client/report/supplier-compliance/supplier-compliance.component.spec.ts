import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupplierComplianceComponent } from './supplier-compliance.component';

describe('SupplierComplianceComponent', () => {
  let component: SupplierComplianceComponent;
  let fixture: ComponentFixture<SupplierComplianceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupplierComplianceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupplierComplianceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
