import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Gstr3bVs1Component } from './gstr3b-vs1.component';

describe('Gstr3bVs1Component', () => {
  let component: Gstr3bVs1Component;
  let fixture: ComponentFixture<Gstr3bVs1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Gstr3bVs1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Gstr3bVs1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
