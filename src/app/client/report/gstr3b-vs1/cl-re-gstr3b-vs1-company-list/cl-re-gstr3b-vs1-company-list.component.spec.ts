import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClReGstr3bVs1CompanyListComponent } from './cl-re-gstr3b-vs1-company-list.component';

describe('ClReGstr3bVs1CompanyListComponent', () => {
  let component: ClReGstr3bVs1CompanyListComponent;
  let fixture: ComponentFixture<ClReGstr3bVs1CompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClReGstr3bVs1CompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClReGstr3bVs1CompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
