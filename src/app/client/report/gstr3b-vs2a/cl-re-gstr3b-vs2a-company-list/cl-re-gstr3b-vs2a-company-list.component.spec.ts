import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClReGstr3bVs2aCompanyListComponent } from './cl-re-gstr3b-vs2a-company-list.component';

describe('ClReGstr3bVs2aCompanyListComponent', () => {
  let component: ClReGstr3bVs2aCompanyListComponent;
  let fixture: ComponentFixture<ClReGstr3bVs2aCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClReGstr3bVs2aCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClReGstr3bVs2aCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
