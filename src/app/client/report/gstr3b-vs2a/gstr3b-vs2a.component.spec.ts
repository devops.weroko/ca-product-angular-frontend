import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Gstr3bVs2aComponent } from './gstr3b-vs2a.component';

describe('Gstr3bVs2aComponent', () => {
  let component: Gstr3bVs2aComponent;
  let fixture: ComponentFixture<Gstr3bVs2aComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Gstr3bVs2aComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Gstr3bVs2aComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
