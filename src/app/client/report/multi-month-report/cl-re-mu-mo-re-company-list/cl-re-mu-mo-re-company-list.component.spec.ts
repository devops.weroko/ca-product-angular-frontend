import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClReMuMoReCompanyListComponent } from './cl-re-mu-mo-re-company-list.component';

describe('ClReMuMoReCompanyListComponent', () => {
  let component: ClReMuMoReCompanyListComponent;
  let fixture: ComponentFixture<ClReMuMoReCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClReMuMoReCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClReMuMoReCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
