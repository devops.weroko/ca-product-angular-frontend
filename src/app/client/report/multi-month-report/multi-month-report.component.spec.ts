import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MultiMonthReportComponent } from './multi-month-report.component';

describe('MultiMonthReportComponent', () => {
  let component: MultiMonthReportComponent;
  let fixture: ComponentFixture<MultiMonthReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MultiMonthReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MultiMonthReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
