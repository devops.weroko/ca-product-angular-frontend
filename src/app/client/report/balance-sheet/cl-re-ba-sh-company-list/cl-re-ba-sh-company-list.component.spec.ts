import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClReBaShCompanyListComponent } from './cl-re-ba-sh-company-list.component';

describe('ClReBaShCompanyListComponent', () => {
  let component: ClReBaShCompanyListComponent;
  let fixture: ComponentFixture<ClReBaShCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClReBaShCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClReBaShCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
