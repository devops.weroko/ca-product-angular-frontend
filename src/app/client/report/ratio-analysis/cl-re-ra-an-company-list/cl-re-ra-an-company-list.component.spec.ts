import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClReRaAnCompanyListComponent } from './cl-re-ra-an-company-list.component';

describe('ClReRaAnCompanyListComponent', () => {
  let component: ClReRaAnCompanyListComponent;
  let fixture: ComponentFixture<ClReRaAnCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClReRaAnCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClReRaAnCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
