import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClReAllCompanyListComponent } from './cl-re-all-company-list.component';

describe('ClReAllCompanyListComponent', () => {
  let component: ClReAllCompanyListComponent;
  let fixture: ComponentFixture<ClReAllCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClReAllCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClReAllCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
