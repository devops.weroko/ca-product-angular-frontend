import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClRePrLoStCompanyListComponent } from './cl-re-pr-lo-st-company-list.component';

describe('ClRePrLoStCompanyListComponent', () => {
  let component: ClRePrLoStCompanyListComponent;
  let fixture: ComponentFixture<ClRePrLoStCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClRePrLoStCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClRePrLoStCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
