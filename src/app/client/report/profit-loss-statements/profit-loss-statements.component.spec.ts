import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfitLossStatementsComponent } from './profit-loss-statements.component';

describe('ProfitLossStatementsComponent', () => {
  let component: ProfitLossStatementsComponent;
  let fixture: ComponentFixture<ProfitLossStatementsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfitLossStatementsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfitLossStatementsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
