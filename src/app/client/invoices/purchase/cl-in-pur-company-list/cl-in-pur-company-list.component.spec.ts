import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClInPurCompanyListComponent } from './cl-in-pur-company-list.component';

describe('ClInPurCompanyListComponent', () => {
  let component: ClInPurCompanyListComponent;
  let fixture: ComponentFixture<ClInPurCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClInPurCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClInPurCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
