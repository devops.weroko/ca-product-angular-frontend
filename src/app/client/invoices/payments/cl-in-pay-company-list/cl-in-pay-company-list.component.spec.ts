import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClInPayCompanyListComponent } from './cl-in-pay-company-list.component';

describe('ClInPayCompanyListComponent', () => {
  let component: ClInPayCompanyListComponent;
  let fixture: ComponentFixture<ClInPayCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClInPayCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClInPayCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
