import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClInStCompanyListComponent } from './cl-in-st-company-list.component';

describe('ClInStCompanyListComponent', () => {
  let component: ClInStCompanyListComponent;
  let fixture: ComponentFixture<ClInStCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClInStCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClInStCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
