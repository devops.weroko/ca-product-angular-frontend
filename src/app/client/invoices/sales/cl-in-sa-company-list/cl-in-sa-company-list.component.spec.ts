import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClInSaCompanyListComponent } from './cl-in-sa-company-list.component';

describe('ClInSaCompanyListComponent', () => {
  let component: ClInSaCompanyListComponent;
  let fixture: ComponentFixture<ClInSaCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClInSaCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClInSaCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
