import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClMaCompanyListComponent } from './cl-ma-company-list.component';

describe('ClMaCompanyListComponent', () => {
  let component: ClMaCompanyListComponent;
  let fixture: ComponentFixture<ClMaCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClMaCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClMaCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
