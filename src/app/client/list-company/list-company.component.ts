import { Component, OnInit } from '@angular/core';
import { ViewListCompanyService } from 'src/app/services/client/view-list-company/view-list-company.service';
import { CompanyInfo } from 'src/app/models/CompanyInfo';
import { HttpClient, HttpHeaders } from '@angular/common/http';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    
    
  })
  
}

@Component({
  selector: 'app-list-company',
  templateUrl: './list-company.component.html',
  styleUrls: ['./list-company.component.css']
})
export class ListCompanyComponent implements OnInit {
  companyInfos:CompanyInfo[];

  

  constructor(private viewListCompanyService: ViewListCompanyService, private http: HttpClient) { }

  ngOnInit(): void {
  

    this.viewListCompanyService.getCompanyList().subscribe(companyInfos => {
      this.companyInfos = companyInfos;
      console.log(this.companyInfos);
    });

    
  }

  testMe(){
    const url = `/testme`;
    console.log(url);
    return this.http.get<any>(url,httpOptions);
  }

}
