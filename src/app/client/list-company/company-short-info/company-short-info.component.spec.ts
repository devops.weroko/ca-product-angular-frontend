import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyShortInfoComponent } from './company-short-info.component';

describe('CompanyShortInfoComponent', () => {
  let component: CompanyShortInfoComponent;
  let fixture: ComponentFixture<CompanyShortInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompanyShortInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyShortInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
