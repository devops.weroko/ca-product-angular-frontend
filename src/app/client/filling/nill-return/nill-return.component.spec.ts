import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NillReturnComponent } from './nill-return.component';

describe('NillReturnComponent', () => {
  let component: NillReturnComponent;
  let fixture: ComponentFixture<NillReturnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NillReturnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NillReturnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
