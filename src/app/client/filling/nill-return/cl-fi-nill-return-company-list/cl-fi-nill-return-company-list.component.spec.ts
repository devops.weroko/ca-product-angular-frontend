import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClFiNillReturnCompanyListComponent } from './cl-fi-nill-return-company-list.component';

describe('ClFiNillReturnCompanyListComponent', () => {
  let component: ClFiNillReturnCompanyListComponent;
  let fixture: ComponentFixture<ClFiNillReturnCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClFiNillReturnCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClFiNillReturnCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
