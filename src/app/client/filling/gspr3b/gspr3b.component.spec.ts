import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Gspr3bComponent } from './gspr3b.component';

describe('Gspr3bComponent', () => {
  let component: Gspr3bComponent;
  let fixture: ComponentFixture<Gspr3bComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Gspr3bComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Gspr3bComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
