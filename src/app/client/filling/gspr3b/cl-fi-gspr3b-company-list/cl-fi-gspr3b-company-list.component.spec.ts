import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClFiGspr3bCompanyListComponent } from './cl-fi-gspr3b-company-list.component';

describe('ClFiGspr3bCompanyListComponent', () => {
  let component: ClFiGspr3bCompanyListComponent;
  let fixture: ComponentFixture<ClFiGspr3bCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClFiGspr3bCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClFiGspr3bCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
