import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClFiGstr9CompanyListComponent } from './cl-fi-gstr9-company-list.component';

describe('ClFiGstr9CompanyListComponent', () => {
  let component: ClFiGstr9CompanyListComponent;
  let fixture: ComponentFixture<ClFiGstr9CompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClFiGstr9CompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClFiGstr9CompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
