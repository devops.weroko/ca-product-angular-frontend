import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClFiGspr1CompanyListComponent } from './cl-fi-gspr1-company-list.component';

describe('ClFiGspr1CompanyListComponent', () => {
  let component: ClFiGspr1CompanyListComponent;
  let fixture: ComponentFixture<ClFiGspr1CompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClFiGspr1CompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClFiGspr1CompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
