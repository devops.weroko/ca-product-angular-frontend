import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Gspr1Component } from './gspr1.component';

describe('Gspr1Component', () => {
  let component: Gspr1Component;
  let fixture: ComponentFixture<Gspr1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Gspr1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Gspr1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
