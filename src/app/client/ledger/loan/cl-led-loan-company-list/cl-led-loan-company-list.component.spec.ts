import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClLedLoanCompanyListComponent } from './cl-led-loan-company-list.component';

describe('ClLedLoanCompanyListComponent', () => {
  let component: ClLedLoanCompanyListComponent;
  let fixture: ComponentFixture<ClLedLoanCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClLedLoanCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClLedLoanCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
