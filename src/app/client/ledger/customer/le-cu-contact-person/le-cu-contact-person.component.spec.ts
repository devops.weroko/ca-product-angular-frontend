import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeCuContactPersonComponent } from './le-cu-contact-person.component';

describe('LeCuContactPersonComponent', () => {
  let component: LeCuContactPersonComponent;
  let fixture: ComponentFixture<LeCuContactPersonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeCuContactPersonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeCuContactPersonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
