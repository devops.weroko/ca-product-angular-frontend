import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeCuAditionalInfoComponent } from './le-cu-aditional-info.component';

describe('LeCuAditionalInfoComponent', () => {
  let component: LeCuAditionalInfoComponent;
  let fixture: ComponentFixture<LeCuAditionalInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeCuAditionalInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeCuAditionalInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
