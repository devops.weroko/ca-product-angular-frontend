import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeCuAttachmentsComponent } from './le-cu-attachments.component';

describe('LeCuAttachmentsComponent', () => {
  let component: LeCuAttachmentsComponent;
  let fixture: ComponentFixture<LeCuAttachmentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeCuAttachmentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeCuAttachmentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
