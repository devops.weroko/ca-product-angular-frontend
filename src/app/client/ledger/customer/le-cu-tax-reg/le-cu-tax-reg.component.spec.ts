import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeCuTaxRegComponent } from './le-cu-tax-reg.component';

describe('LeCuTaxRegComponent', () => {
  let component: LeCuTaxRegComponent;
  let fixture: ComponentFixture<LeCuTaxRegComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeCuTaxRegComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeCuTaxRegComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
