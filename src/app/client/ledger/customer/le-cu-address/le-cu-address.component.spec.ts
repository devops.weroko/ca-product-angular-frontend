import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeCuAddressComponent } from './le-cu-address.component';

describe('LeCuAddressComponent', () => {
  let component: LeCuAddressComponent;
  let fixture: ComponentFixture<LeCuAddressComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeCuAddressComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeCuAddressComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
