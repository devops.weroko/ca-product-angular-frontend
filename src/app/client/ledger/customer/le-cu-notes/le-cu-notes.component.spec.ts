import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeCuNotesComponent } from './le-cu-notes.component';

describe('LeCuNotesComponent', () => {
  let component: LeCuNotesComponent;
  let fixture: ComponentFixture<LeCuNotesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeCuNotesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeCuNotesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
