import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeCuBankComponent } from './le-cu-bank.component';

describe('LeCuBankComponent', () => {
  let component: LeCuBankComponent;
  let fixture: ComponentFixture<LeCuBankComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeCuBankComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeCuBankComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
