import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClLedCustomerCompanyListComponent } from './cl-led-customer-company-list.component';

describe('ClLedCustomerCompanyListComponent', () => {
  let component: ClLedCustomerCompanyListComponent;
  let fixture: ComponentFixture<ClLedCustomerCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClLedCustomerCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClLedCustomerCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
