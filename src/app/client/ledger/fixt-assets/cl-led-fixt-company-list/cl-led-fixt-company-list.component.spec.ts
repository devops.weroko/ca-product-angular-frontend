import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClLedFixtCompanyListComponent } from './cl-led-fixt-company-list.component';

describe('ClLedFixtCompanyListComponent', () => {
  let component: ClLedFixtCompanyListComponent;
  let fixture: ComponentFixture<ClLedFixtCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClLedFixtCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClLedFixtCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
