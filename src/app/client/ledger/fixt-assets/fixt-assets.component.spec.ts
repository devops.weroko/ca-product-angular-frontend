import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FixtAssetsComponent } from './fixt-assets.component';

describe('FixtAssetsComponent', () => {
  let component: FixtAssetsComponent;
  let fixture: ComponentFixture<FixtAssetsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FixtAssetsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FixtAssetsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
