import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClLedCashCompanyListComponent } from './cl-led-cash-company-list.component';

describe('ClLedCashCompanyListComponent', () => {
  let component: ClLedCashCompanyListComponent;
  let fixture: ComponentFixture<ClLedCashCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClLedCashCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClLedCashCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
