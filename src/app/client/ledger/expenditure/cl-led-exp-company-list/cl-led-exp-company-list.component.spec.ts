import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClLedExpCompanyListComponent } from './cl-led-exp-company-list.component';

describe('ClLedExpCompanyListComponent', () => {
  let component: ClLedExpCompanyListComponent;
  let fixture: ComponentFixture<ClLedExpCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClLedExpCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClLedExpCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
