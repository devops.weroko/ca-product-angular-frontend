import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClLedSiStCompanyListComponent } from './cl-led-si-st-company-list.component';

describe('ClLedSiStCompanyListComponent', () => {
  let component: ClLedSiStCompanyListComponent;
  let fixture: ComponentFixture<ClLedSiStCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClLedSiStCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClLedSiStCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
