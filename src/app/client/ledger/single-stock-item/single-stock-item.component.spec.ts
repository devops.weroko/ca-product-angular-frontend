import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SingleStockItemComponent } from './single-stock-item.component';

describe('SingleStockItemComponent', () => {
  let component: SingleStockItemComponent;
  let fixture: ComponentFixture<SingleStockItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SingleStockItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleStockItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
