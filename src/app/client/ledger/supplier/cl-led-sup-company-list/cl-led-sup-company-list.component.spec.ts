import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClLedSupCompanyListComponent } from './cl-led-sup-company-list.component';

describe('ClLedSupCompanyListComponent', () => {
  let component: ClLedSupCompanyListComponent;
  let fixture: ComponentFixture<ClLedSupCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClLedSupCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClLedSupCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
