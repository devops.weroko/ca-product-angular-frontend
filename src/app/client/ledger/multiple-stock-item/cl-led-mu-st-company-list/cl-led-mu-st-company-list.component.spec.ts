import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClLedMuStCompanyListComponent } from './cl-led-mu-st-company-list.component';

describe('ClLedMuStCompanyListComponent', () => {
  let component: ClLedMuStCompanyListComponent;
  let fixture: ComponentFixture<ClLedMuStCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClLedMuStCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClLedMuStCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
