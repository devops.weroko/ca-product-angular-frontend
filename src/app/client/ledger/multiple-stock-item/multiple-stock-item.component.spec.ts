import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MultipleStockItemComponent } from './multiple-stock-item.component';

describe('MultipleStockItemComponent', () => {
  let component: MultipleStockItemComponent;
  let fixture: ComponentFixture<MultipleStockItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MultipleStockItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MultipleStockItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
