import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClLedMuLedCompanyListComponent } from './cl-led-mu-led-company-list.component';

describe('ClLedMuLedCompanyListComponent', () => {
  let component: ClLedMuLedCompanyListComponent;
  let fixture: ComponentFixture<ClLedMuLedCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClLedMuLedCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClLedMuLedCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
