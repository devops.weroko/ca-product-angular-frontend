import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClLedBankCompanyListComponent } from './cl-led-bank-company-list.component';

describe('ClLedBankCompanyListComponent', () => {
  let component: ClLedBankCompanyListComponent;
  let fixture: ComponentFixture<ClLedBankCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClLedBankCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClLedBankCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
