import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClLedInvCompanyListComponent } from './cl-led-inv-company-list.component';

describe('ClLedInvCompanyListComponent', () => {
  let component: ClLedInvCompanyListComponent;
  let fixture: ComponentFixture<ClLedInvCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClLedInvCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClLedInvCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
