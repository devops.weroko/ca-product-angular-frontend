import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LedgerAdditionalNoteComponent } from './ledger-additional-note.component';

describe('LedgerAdditionalNoteComponent', () => {
  let component: LedgerAdditionalNoteComponent;
  let fixture: ComponentFixture<LedgerAdditionalNoteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LedgerAdditionalNoteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LedgerAdditionalNoteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
