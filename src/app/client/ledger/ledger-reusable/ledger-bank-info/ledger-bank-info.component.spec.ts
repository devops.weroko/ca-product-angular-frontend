import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LedgerBankInfoComponent } from './ledger-bank-info.component';

describe('LedgerBankInfoComponent', () => {
  let component: LedgerBankInfoComponent;
  let fixture: ComponentFixture<LedgerBankInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LedgerBankInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LedgerBankInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
