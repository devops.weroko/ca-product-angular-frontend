import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LedgerAddressComponent } from './ledger-address.component';

describe('LedgerAddressComponent', () => {
  let component: LedgerAddressComponent;
  let fixture: ComponentFixture<LedgerAddressComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LedgerAddressComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LedgerAddressComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
