import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LedgerAttachmentComponent } from './ledger-attachment.component';

describe('LedgerAttachmentComponent', () => {
  let component: LedgerAttachmentComponent;
  let fixture: ComponentFixture<LedgerAttachmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LedgerAttachmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LedgerAttachmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
