import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClLedLedReuCompanyListComponent } from './cl-led-led-reu-company-list.component';

describe('ClLedLedReuCompanyListComponent', () => {
  let component: ClLedLedReuCompanyListComponent;
  let fixture: ComponentFixture<ClLedLedReuCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClLedLedReuCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClLedLedReuCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
