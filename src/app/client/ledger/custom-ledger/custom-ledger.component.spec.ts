import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomLedgerComponent } from './custom-ledger.component';

describe('CustomLedgerComponent', () => {
  let component: CustomLedgerComponent;
  let fixture: ComponentFixture<CustomLedgerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomLedgerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomLedgerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
