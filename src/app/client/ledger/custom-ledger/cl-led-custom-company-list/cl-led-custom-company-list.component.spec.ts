import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClLedCustomCompanyListComponent } from './cl-led-custom-company-list.component';

describe('ClLedCustomCompanyListComponent', () => {
  let component: ClLedCustomCompanyListComponent;
  let fixture: ComponentFixture<ClLedCustomCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClLedCustomCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClLedCustomCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
