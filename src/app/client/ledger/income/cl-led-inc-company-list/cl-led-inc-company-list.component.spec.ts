import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClLedIncCompanyListComponent } from './cl-led-inc-company-list.component';

describe('ClLedIncCompanyListComponent', () => {
  let component: ClLedIncCompanyListComponent;
  let fixture: ComponentFixture<ClLedIncCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClLedIncCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClLedIncCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
