import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImpNoteComponentComponent } from './imp-note-component.component';

describe('ImpNoteComponentComponent', () => {
  let component: ImpNoteComponentComponent;
  let fixture: ComponentFixture<ImpNoteComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImpNoteComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImpNoteComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
