import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BankInfoComponentComponent } from './bank-info-component.component';

describe('BankInfoComponentComponent', () => {
  let component: BankInfoComponentComponent;
  let fixture: ComponentFixture<BankInfoComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BankInfoComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BankInfoComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
