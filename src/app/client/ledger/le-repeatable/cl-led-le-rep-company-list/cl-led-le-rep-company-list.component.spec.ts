import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClLedLeRepCompanyListComponent } from './cl-led-le-rep-company-list.component';

describe('ClLedLeRepCompanyListComponent', () => {
  let component: ClLedLeRepCompanyListComponent;
  let fixture: ComponentFixture<ClLedLeRepCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClLedLeRepCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClLedLeRepCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
