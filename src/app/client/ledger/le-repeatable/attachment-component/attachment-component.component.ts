import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { AttachmentComponent } from 'src/app/shared/models/AttachmentComponents';

@Component({
  selector: 'app-attachment-component',
  templateUrl: './attachment-component.component.html',
  styleUrls: ['./attachment-component.component.css']
})
export class AttachmentComponentComponent implements OnInit {
  AttachmentComponentArray : Array<AttachmentComponent> = [];
  newRow: any = {};
  @Output() AttachmentComponent: EventEmitter<any> = new EventEmitter();
i:number;
  constructor() { }

  ngOnInit(): void {

    this.newRow ={
      upload_file:"",
      file_name:""
    }
    this.AttachmentComponentArray.push(this.newRow);

  }

  addRow(index){
    this.newRow ={
      upload_file:"",
      file_name:""
    }
    this.AttachmentComponentArray.push(this.newRow);
    console.log("Added="+index);
    console.log(this.AttachmentComponentArray);
    return true;
  }
  deleteRow(index){
    if(this.AttachmentComponentArray.length==1){
      console.log("Can't Delete");
      return false;
    }
  
    else{
      this.AttachmentComponentArray.splice(index,1);
      console.log("Deleted successfully="+index);
      return true;
    }
}
onSubmit(){

  const signatoriesArray=this.AttachmentComponentArray

  console.log(this.AttachmentComponentArray);

  

  this.AttachmentComponent.emit(signatoriesArray);
}
}