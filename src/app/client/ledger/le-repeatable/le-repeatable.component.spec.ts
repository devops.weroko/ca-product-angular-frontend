import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeRepeatableComponent } from './le-repeatable.component';

describe('LeRepeatableComponent', () => {
  let component: LeRepeatableComponent;
  let fixture: ComponentFixture<LeRepeatableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeRepeatableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeRepeatableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
