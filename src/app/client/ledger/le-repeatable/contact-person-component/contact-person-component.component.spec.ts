import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContactPersonComponentComponent } from './contact-person-component.component';

describe('ContactPersonComponentComponent', () => {
  let component: ContactPersonComponentComponent;
  let fixture: ComponentFixture<ContactPersonComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContactPersonComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactPersonComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
