import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AditionalInfoComponentComponent } from './aditional-info-component.component';

describe('AditionalInfoComponentComponent', () => {
  let component: AditionalInfoComponentComponent;
  let fixture: ComponentFixture<AditionalInfoComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AditionalInfoComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AditionalInfoComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
