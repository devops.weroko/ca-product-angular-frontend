import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClLedBillCompanyListComponent } from './cl-led-bill-company-list.component';

describe('ClLedBillCompanyListComponent', () => {
  let component: ClLedBillCompanyListComponent;
  let fixture: ComponentFixture<ClLedBillCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClLedBillCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClLedBillCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
