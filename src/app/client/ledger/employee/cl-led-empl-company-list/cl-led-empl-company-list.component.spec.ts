import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClLedEmplCompanyListComponent } from './cl-led-empl-company-list.component';

describe('ClLedEmplCompanyListComponent', () => {
  let component: ClLedEmplCompanyListComponent;
  let fixture: ComponentFixture<ClLedEmplCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClLedEmplCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClLedEmplCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
