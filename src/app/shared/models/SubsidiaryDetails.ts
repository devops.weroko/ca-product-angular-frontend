export class SubsidiaryDetails {

    subsidiary_sl_no:number;
    subsidiary_name:string;
    subsidiary_cin:string;
    no_share_subsidiary:string;
    ratio_of_share_subsidiary:string;
    date_of_subsidiary:string;
    subsidiary_attachment:string;

}