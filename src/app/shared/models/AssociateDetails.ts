export class AssociateDetails {
    associate_sl_no:number;
    name_of_the_associate_company:string;
    cin_of_associate_company:string;
    no_of_shares_associate:string;
    persent_of_shares_associate:string;
    date_of_associate:string;
    associate_attachment:string;

}