export class CompanyAddress {

	company_id:number;
	id:number;
    street:string;
    area:string;
	
	city_town:string;
	
	district:string;
	
	police_station:string;
	
	state:string;
	
	country:string;
	
	zip_code:string;
	
	no_of_warehouses:number;
	
    no_of_branches:number;


}