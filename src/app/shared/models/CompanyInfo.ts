export class CompanyInfo {
  company_id:number;
    name:string;
    trade_name:string;	
      pan:string;
    pan_effective_date:string;
      
      pan_dep_address:string;
      
      range_ward:string;
      
      pan_ph_no:string;
      
      pan_attachment:string;
      
      org_type:string; 
      
      business_type:string;
      
      company_crn_no:string;
  
      crn_eff_date:string;
      
      crn_address:string;
      
      crn_range_ward:string;
      
      crn_ph_no:string;
  
      crn_attachment:string;
      
      
      company_llpin_no:string;
      
      llpin_eff_date:string;
      
      llpin_address:string;
      
      llpin_range_ward:string;
  
      llpin_ph_no:string;
  
      llpin_attachment:string;
      
      
      company_cin_no:string;
      
      cin_eff_date:string;
      
      cin_address:string;
      
      cin_range_ward:string;
      
      cin_ph_no:string;
      
      cin_attachment:string;
      
      company_logo:string;
      
      phone:string;
      
      mobile:string;
      
      email:string; 
      
      website:string;
      
      date_of_incorporation:string;
      
      financial_year_start:string;
      
      book_commencing:string;
      
      commencement_date:string;

      title:string;
      

}