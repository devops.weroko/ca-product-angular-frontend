export class AuditorDetails {

    //Tax Auditor

    tax_name_of_auditor:string;
    tax_auditors_pan:string;
    tax_membership_no:string;
    tax_address:string;
    tax_country:string;
    tax_state:string;
    tax_zip_code:string;
    tax_firm_name:string;
    tax_firm_pan:string;
    tax_frn:string;
    tax_phone:string;
    tax_email:string;
    tax_period_from:string;
    tax_to:string;

    // GST auditor

    gst_name_of_auditor:string;
    gst_auditors_pan:string;
    gst_membership_no:string;
    gst_address:string;
    gst_country:string;
    gst_state:string;
    gst_zip_code:string;
    gst_firm_name:string;
    gst_firm_pan:string;
    gst_frn:string;
    gst_phone:string;
    gst_email:string;
    gst_period_from:string;
    gst_to:string;

    //Satutory Auditor Details

    satutory_name_of_auditor:string;
    satutory_auditors_pan:string;
    satutory_membership_no:string;
    satutory_address:string;
    satutory_country:string;
    satutory_state:string;
    satutory_zip_code:string;
    satutory_firm_name:string;
    satutory_firm_pan:string;
    satutory_frn:string;
    satutory_phone:string;
    satutory_email:string;
    satutory_period_from:string;
    satutory_to:string;

}