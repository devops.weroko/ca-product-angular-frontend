export class HoldingDetails {
    holding_sl_no:number;
    holders_name:string;
    holders_cin:string;
    no_share_holders:string;
    ratio_of_share_holders:string;
    date_of_holders:string;
    holding_attachment:string;

}