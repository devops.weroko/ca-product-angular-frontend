export class CompanySettings {

  company_id:number;
  id:number;
  date_format:string;
	
	base_currency:string;
	
	currency_symbol:string;
	
  currency_decimal_place:string;
  
  gst: string;
  ptaxenroll: string;
  ptaxreg: string;
  epf: string;
  esi: string;
  tds: string;
  tcs: string;
  iec: string;
  msme: string;
  otherreg: string;


}