export class SignatoriesDetails {

    company_id:number;
    name:string;
    father_name:string;
    address:string;
    qualification:string;
    designation:string;
    date_of_birth:string;
    date_of_appointment:string;
    date_of_cessation:string;
    din:string;
    aadhar:string;
    pan:string;
    passport:string;
    aadhar_attachment:string;
    pan_attachment:string;
    passport_attachment:string;
    image_attachment:string;
    authorized_signatories:string;
    share_ratio:string;

}