export class CompanyAddressBranchDetails{

    branch_name : string;
    street : string;
    area : string;
    city_town : string;
    district : string;
    police_station : string;
    country : string;
    state : string;
    pin_code : string;
    attachment : string;
    trade_license : string;
    ptax_enrollment : string;
    reg_gst : string;
    tan : string;
    reg_esi : string;
    other_reg : string;
    no_of_registrations : number;
    warehouse : string;
    no_of_warehouses : number;






}