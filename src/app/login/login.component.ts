import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../services/authentication.service';
import { ToastrService } from 'ngx-toastr';
import { CaproductService } from '../services/caproduct.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  username:string;
  password:string;
  invalidLogin = false

  constructor(private router: Router, private loginservice: AuthenticationService, public toasterService: ToastrService, private caproductservice: CaproductService) { }

  ngOnInit(): void {
    if(this.invalidLogin==false){
      this.router.navigate(['/client/companylist'])
    }
  }

  checkLogin() {
    console.log(this.username);

    (this.loginservice.authenticate(this.username, this.password).subscribe(
      data => {

        this.caproductservice.getAllUserDetails(this.username).subscribe(
          userInfo => {
            console.log(userInfo);
          }
        );
        
        setTimeout(()=>{  
        this.router.navigate(['/client/companylist'])
        this.invalidLogin = false;
         }, 3000);

        
        
        
      },
      error => {
        this.invalidLogin = true

      }
    )
    );
  }

}
