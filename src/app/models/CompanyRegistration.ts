export class CompanyRegistration{

    name : string;
    contact_no : string;
    username : string;
    firm_name : string;
    company_type : string;
    user_type : string;
    register_for : string;
    password : string;

}