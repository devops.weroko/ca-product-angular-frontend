import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-client-navbar',
  templateUrl: './client-navbar.component.html',
  styleUrls: ['./client-navbar.component.css']
})
export class ClientNavbarComponent implements OnInit {

  tr :boolean=false;
  ads: boolean=false;
  toto: boolean=false;
  constructor(private authenticationService : AuthenticationService) { }

  @Output() bomb = new EventEmitter<void>();
  @Output() bomb2 = new EventEmitter<void>();

  ngOnInit(): void {
  }
  toggle(){
    this.tr=!this.tr;
    this.bomb2.emit();
  }

  asideMenu(){
    this.toto=!this.toto;
    this.bomb.emit();
    
  }

  logOut(){
    this.authenticationService.logOut();   
  }
}
