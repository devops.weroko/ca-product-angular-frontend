import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientASidebarComponent } from './client-asidebar.component';

describe('ClientASidebarComponent', () => {
  let component: ClientASidebarComponent;
  let fixture: ComponentFixture<ClientASidebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientASidebarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientASidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
