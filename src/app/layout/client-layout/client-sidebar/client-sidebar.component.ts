import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-client-sidebar',
  templateUrl: './client-sidebar.component.html',
  styleUrls: ['./client-sidebar.component.css']
})
export class ClientSidebarComponent implements OnInit {
  toggle : boolean = false;
  to : boolean = true;

  @Output() bomb3 = new EventEmitter<void>();

  team : boolean =true;
  constructor() { }

  ngOnInit(): void {
  }

  dropDownToggle(){
    this.toggle = !this.toggle
  }

  minimizerToggler(){
    this.team=!this.team;
    this.bomb3.emit();
  }

}
